import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DtRegisterFormComponent } from './dt-register-form.component';

describe('DtRegisterFormComponent', () => {
  let component: DtRegisterFormComponent;
  let fixture: ComponentFixture<DtRegisterFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DtRegisterFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DtRegisterFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
