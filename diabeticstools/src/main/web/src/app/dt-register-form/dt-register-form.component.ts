import {Component, OnInit} from '@angular/core';
import {LoginService} from "../service/login-service";
import {ActivatedRoute, Router} from "@angular/router";
import {RegistrationDto} from "../dto/registration-dto";
import {ValidationService} from "../service/validation-service";
import {EmailValidationDto} from "../dto/email-validation-dto";
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-dt-register-form',
  templateUrl: './dt-register-form.component.html',
  styleUrls: ['./dt-register-form.component.scss']
})
export class DtRegisterFormComponent implements OnInit {

  public registerStage: number = 1;
  private registrationDto: RegistrationDto;
  private showPatientSettings: boolean = false;
  public registrationSuccess: boolean = false;
  public registrationFailure: boolean = false;
  public registerFormValidation;
  private arePasswordsSame: boolean = true;

  public emailValidationDto: EmailValidationDto = new EmailValidationDto();

  constructor(private loginService: LoginService,
              private route: ActivatedRoute,
              private router: Router,
              private validationService: ValidationService) {
    this.registrationDto = new RegistrationDto();
    this.registrationDto.patient_settings = this.route.snapshot.data.patient_settings_default;
    this.registrationDto.default_glucose_unit = 'mgPerDL';
  }

  ngOnInit() {
    this.registerFormValidation = new FormGroup({
      email: new FormControl(this.registrationDto.email, [
        Validators.required,
        Validators.email
      ]),
      password: new FormControl(this.registrationDto.password, [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(32)
      ]),
      confirm_password: new FormControl(this.registrationDto.confirm_password, [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(32)
      ]),
      firstname: new FormControl(this.registrationDto.firstname, [
        Validators.required,
      ]),
      lastname: new FormControl(this.registrationDto.lastname, [
        Validators.required,
      ]),
      phone_number: new FormControl(this.registrationDto.phone_number, [
        Validators.minLength(9),
        Validators.maxLength(9),
        Validators.pattern(/[0-9]{9}/)
      ]),
      default_glucose_unit: new FormControl(this.registrationDto.default_glucose_unit, [
        Validators.required
      ]),
      patient_settings: new FormGroup({
        includeBloodPressure: new FormControl(this.registrationDto.patient_settings.includeBloodPressure),
        reportType: new FormControl(this.registrationDto.patient_settings.reportType),
        reportPeriod: new FormControl(this.registrationDto.patient_settings.reportPeriod),
        doctorEmail: new FormControl(this.registrationDto.patient_settings.doctorEmail, [
          Validators.email
        ]),
        timeWindow: new FormControl(this.registrationDto.patient_settings.timeWindow)
      })
    }, {validators: this.passwordMatchValidator});

  }

  passwordMatchValidator(frm: FormGroup) {
    return frm.get('password').value ===
    frm.get('confirm_password').value ? null : {'mismatch': true};
  }

  validateField(fieldName: string, errorName?: string): boolean {
    return errorName===undefined?this.registerFormValidation.get(fieldName).invalid:this.registerFormValidation.get(fieldName).hasError(errorName)&&
    //console.log(this.registerFormValidation.get(fieldName).errors);
    //return this.registerFormValidation.get(fieldName).invalid &&
    (this.registerFormValidation.get(fieldName).dirty || this.registerFormValidation.get(fieldName).touched);
  }

  areFieldsValid(fieldNames: string[]): boolean {
    let result: boolean = true;
     fieldNames.forEach(fieldName => {
       if(this.registerFormValidation.get(fieldName).invalid) {
         result = false;
       }
     });
     return result;
  }

  validateEmail() {
    console.log(this.registerFormValidation.errors);
    this.emailValidationDto.email = this.registerFormValidation.get('email').value;
    if(this.emailValidationDto.email.length === 0) return;
    this.validationService.validateEmail(this.emailValidationDto)
      .subscribe(
        value => {
          this.emailValidationDto = value;
        }
      )
  }

  validatePasswords(control: FormControl) {
    let password = this.registerFormValidation.get('password').value;
    let confirmedPassword = control.value;
    return password === confirmedPassword ? null:{notSame: true};
  }

  registerButtonHandler() {
    this.registrationDto = this.registerFormValidation.value;
    this.loginService.registerNewUser(this.registrationDto)
      .subscribe(
        value => {
          this.registrationSuccess = true;
          setTimeout(() => {
            this.registrationSuccess = false;
          }, 4000);
          this.loginService.login(this.registrationDto.email, this.registrationDto.password, false).subscribe(
            value1 => {
              setTimeout(() => {
                this.router.navigate(['/dashboard']);
              },5000)
            }
          )
        },
        error1 => {
          this.registrationFailure = true;
          setTimeout(() => {
            this.registrationFailure = false;
          }, 5000);
        }
      )
  }

  nextButtonHandler() {
    if(this.registerStage<3) {
      this.registerStage++;
    }
  }
  previousButtonHandler() {
    if(this.registerStage>1) {
      this.registerStage--;
    }
  }
}
