import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {LoginService} from "../service/login-service";
import {ActivatedRoute, Router} from "@angular/router";
import {LocalStorageService, SessionStorageService} from "ngx-webstorage";

@Component({
  selector: 'app-dt-login-page',
  templateUrl: './dt-login-page.component.html',
  styleUrls: ['./dt-login-page.component.scss']
})
export class DtLoginPageComponent implements OnInit {

  constructor(private loginService: LoginService,
              private router: Router,
              private route: ActivatedRoute,
              private $localStorage: LocalStorageService,
              private $sessionStorage: SessionStorageService) {
    if(this.loginService.isLoggedIn()) {
      this.router.navigate(['/dashboard']);
    }
  }

  username: string;
  password: string;
  rememberMe: boolean = false;

  loginSuccess: boolean = false;
  loginFailure: boolean = false;
  status: string;
  loginStatusMessage: string = 'Unexpected error.';



  ngOnInit() {
    this.status = this.route.snapshot.paramMap.get('status');
    setTimeout(() => {this.status = null}, 5000);
  }

  login() {
    this.loginService.login(this.username, this.password, this.rememberMe)
    .subscribe(
      (value) => {
        this.loginSuccess = true;
        setTimeout(() => {this.router.navigate(['dashboard']);}, 1500);
        setTimeout(() => {this.loginSuccess = false}, 1500);
      },
      error1 => {
        if(error1.status === 401) {
          this.loginStatusMessage = 'Niepoprawny login, lub hasło.';
        }
        this.loginFailure = true;
      }
    );
  }
}
