import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DtLoginPageComponent } from './dt-login-page.component';

describe('DtLoginPageComponent', () => {
  let component: DtLoginPageComponent;
  let fixture: ComponentFixture<DtLoginPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DtLoginPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DtLoginPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
