import { Component, OnInit } from '@angular/core';
import {Measurement} from "../../dto/measurement";
import {UserProfileDto} from "../../dto/user-profile-dto";
import {ActivatedRoute} from "@angular/router";
import {PatientService} from "../../service/patient-service";
import {BriefPersonData} from "../../dto/brief-person-data";
import {BloodPressureValue} from "../../dto/blood-pressure-value";
import {UnitValue} from "../../dto/unit-value";

@Component({
  selector: 'app-dt-new-measurement',
  templateUrl: './dt-new-measurement.component.html',
  styleUrls: ['./dt-new-measurement.component.scss']
})
export class DtNewMeasurementComponent implements OnInit {

  public pos_table: number[] = [0,1,2,3,4];

  private glucoseInput: number[] = [0,9,0,0,0];

  private bloodPressureUpper: number[] = [1,2,0,0,0];

  private bloodPressureLower: number[] = [0,8,0,0,0];

  private measurement: Measurement;
  private userProfile: UserProfileDto;
  public userBrief: BriefPersonData;
  public addSuccess: boolean = false;
  public addFailure: boolean = false;
  public buttonPressed: boolean = false;

  constructor(private route: ActivatedRoute,
              private patientService: PatientService) {
    this.measurement = new Measurement();
  }

  ngOnInit() {
    this.userProfile = this.route.snapshot.data.user_profile;
    this.userBrief = this.route.snapshot.data.user_brief;
    this.glucoseInput = this.getDefaultGlucoseValue().default;
    this.writeMeasurementValues();
  }

  getDefaultGlucoseValue(): {unit: string, default: number[]}{
    if(this.userBrief.default_glucose_unit === 'mgPerDL') return {unit: 'mg/dL', default: [0,9,0,0,0]};
    else if(this.userBrief.default_glucose_unit === 'mmolPerL') return {unit: 'mmol/L', default: [0,0,5,0,0]};
    else return null;
  }

  glucoseTooHigh(): boolean {
    return this.measurement.glucose_value>this.userBrief.max_glucose;
  }
  glucoseTooLow(): boolean {
    return this.measurement.glucose_value<this.userBrief.min_glucose;
  }

  handleNewMeasurement() {
    this.writeMeasurementValues();
    if((this.glucoseTooLow() || this.glucoseTooHigh()) && !this.buttonPressed) {
      this.buttonPressed = true;
      return;
    }
    this.patientService.postNewMeasurement(this.measurement)
      .subscribe(
        value => {
          this.addSuccess = true;
          setTimeout(() => {this.addSuccess = false}, 5000);
        },
        error1 => {
          this.addFailure = true;
          setTimeout(() => {this.addFailure = false}, 5000);
        }
      );
    this.buttonPressed = false;
  }

  private writeMeasurementValues() {
    this.measurement.glucose_value = this.parseCompositeInput(this.glucoseInput);
    this.measurement.glucose_unit = this.userProfile.default_glucose_unit;
    if (this.userBrief.patient_settings.includeBloodPressure) {
      this.measurement.blood_pressure_upper = this.parseCompositeInput(this.bloodPressureUpper);
      this.measurement.blood_pressure_lower = this.parseCompositeInput(this.bloodPressureLower);
      this.measurement.blood_pressure_unit = 'mmHg';
    }
  }

  incrementFunction(compositeInput: number[], pos: number) {
    compositeInput[pos] = (Math.abs(compositeInput[pos]+1))%10;
    this.writeMeasurementValues();
  }

  decrementFunction(compositeInput: number[], pos: number) {
    compositeInput[pos] = compositeInput[pos]!==0?(Math.abs(compositeInput[pos]-1))%10:9;
    this.writeMeasurementValues();
  }

  parseCompositeInput(compositeInput: number[]): number {
    let value: number = 0;
      for(let i = 4; i>=0;i--) {
        value = value + compositeInput[i] * Math.pow(10, 2-i);
      }
    return value;
  }
}
