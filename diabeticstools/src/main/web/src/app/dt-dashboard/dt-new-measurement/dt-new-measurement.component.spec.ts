import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DtNewMeasurementComponent } from './dt-new-measurement.component';

describe('DtNewMeasurementComponent', () => {
  let component: DtNewMeasurementComponent;
  let fixture: ComponentFixture<DtNewMeasurementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DtNewMeasurementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DtNewMeasurementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
