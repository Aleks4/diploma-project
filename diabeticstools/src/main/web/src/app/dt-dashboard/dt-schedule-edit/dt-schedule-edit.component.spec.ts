import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DtScheduleEditComponent } from './dt-schedule-edit.component';

describe('DtScheduleEditComponent', () => {
  let component: DtScheduleEditComponent;
  let fixture: ComponentFixture<DtScheduleEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DtScheduleEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DtScheduleEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
