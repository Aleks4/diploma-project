import { Component, OnInit } from '@angular/core';
import {PatientSchedule} from "../../dto/patient-schedule";
import {PatientService} from "../../service/patient-service";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-dt-schedule-edit',
  templateUrl: './dt-schedule-edit.component.html',
  styleUrls: ['./dt-schedule-edit.component.scss']
})
export class DtScheduleEditComponent implements OnInit {

  public patientSchedule: PatientSchedule;
  public editSuccess: boolean = false;
  public editFailure: boolean = false;
  public collidingSchedules: PatientSchedule[] = [];
  public displayColliding: boolean = false;

  constructor(private patientService: PatientService,
              private route: ActivatedRoute) {
    this.patientSchedule = this.route.snapshot.data.patient_schedule;
  }

  ngOnInit() {
  }

  async retrieveCollidingSchedules() {
    this.patientService.retrieveColliding(this.patientSchedule).subscribe(
      value => {
        this.displayColliding = true;
        this.collidingSchedules = value;
      }
    );
  }

  async editScheduleHandler() {
    // if(this.patientSchedule.type === 'MEASUREMENT') {
    //   await this.retrieveCollidingSchedules();
    //   if(this.collidingSchedules.length>0) {
    //     this.displayColliding = true;
    //     return;
    //   }
    // }
    this.patientService.editSchedule(this.patientSchedule)
      .subscribe(
        value => {
          this.editSuccess = true;
          this.displayColliding = false;
          setTimeout(() => {this.editSuccess = false}, 5000);
        },
        error1 => {
          this.editFailure = true;
          this.retrieveCollidingSchedules();
          setTimeout(() => {this.editFailure = false}, 5000);
        }
      );
  }

}
