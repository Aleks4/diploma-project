import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DtMeasurementDetailsComponent } from './dt-measurement-details.component';

describe('DtMeasurementDetailsComponent', () => {
  let component: DtMeasurementDetailsComponent;
  let fixture: ComponentFixture<DtMeasurementDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DtMeasurementDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DtMeasurementDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
