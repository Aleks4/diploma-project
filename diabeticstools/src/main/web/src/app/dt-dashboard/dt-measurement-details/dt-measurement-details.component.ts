import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {Measurement} from "../../dto/measurement";
import {BriefPersonData} from "../../dto/brief-person-data";
import {PatientService} from "../../service/patient-service";

@Component({
  selector: 'app-dt-measurement-details',
  templateUrl: './dt-measurement-details.component.html',
  styleUrls: ['./dt-measurement-details.component.scss']
})
export class DtMeasurementDetailsComponent implements OnInit {

  public measurement: Measurement;
  private userBrief: BriefPersonData;
  public pageFocus: string = 'details';
  public editSuccess: boolean = false;
  public editFailure: boolean = false;

  constructor(private route: ActivatedRoute,
              private patientService: PatientService) {
    this.measurement = this.route.snapshot.data.measurement;
    this.userBrief = this.route.snapshot.data.user_brief;
  }

  ngOnInit() {
  }

  changeFocus(focus: string) {
    this.pageFocus = focus;
  }

  simpleUnitTranslator(unit: string): string {
    if(unit === 'mgPerDL') return 'mg/dL';
    else if(unit === 'mmolPerL') return 'mmol/L';
    else return '';
  }

  handleEdit() {
    this.patientService.editMeasurement(this.measurement)
      .subscribe(
        value => {
          this.measurement = value;
          this.editSuccess = true;
          setTimeout(() => {this.editSuccess = false;}, 3000)
          this.changeFocus('details');
        },
        error1 => {
          this.editFailure = true;
          setTimeout(() => {this.editFailure = false;}, 3000)
        }
      )
  }
}
