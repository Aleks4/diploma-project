import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {PatientSchedule} from "../../dto/patient-schedule";
import {HttpClient} from "@angular/common/http";
import {BriefPersonData} from "../../dto/brief-person-data";
import {PatientService} from "../../service/patient-service";

@Component({
  selector: 'app-dt-patient-schedule',
  templateUrl: './dt-patient-schedule.component.html',
  styleUrls: ['./dt-patient-schedule.component.scss']
})
export class DtPatientScheduleComponent implements OnInit {

  private scheduleList: PatientSchedule[];
  private measurementScheduleList: Array<PatientSchedule> = [];
  private otherScheduleList: Array<PatientSchedule> = [];
  public pageFocus: string;
  private newPatientSchedule: PatientSchedule;
  public addSuccess: boolean = false;
  public addFailure: boolean = false;
  private deleteSuccess: boolean = false;
  private deleteFailure: boolean = false;
  public defaultTimeWindow: number;
  private userBrief: BriefPersonData;

  public displayColliding: boolean = false;
  public collidingSchedules: PatientSchedule[] = [];

  constructor(private route: ActivatedRoute,
              private http: HttpClient,
              private patientService: PatientService) {
    this.scheduleList = this.route.snapshot.data.patient_schedule_list;
    this.userBrief = this.route.snapshot.data.user_brief;
    this.defaultTimeWindow = this.userBrief.patient_settings.timeWindow;
    this.pageFocus = 'list';
    this.newPatientSchedule = new PatientSchedule();
    this.newPatientSchedule.type = 'MEASUREMENT';
    this.newPatientSchedule.time_window = this.defaultTimeWindow;
    this.populateLists();
  }

  ngOnInit() {
  }

  async retrieveCollidingSchedules() {
    this.patientService.retrieveColliding(this.newPatientSchedule).subscribe(
      value => {
        this.displayColliding = true;
        this.collidingSchedules = value;
      }
    );
  }

  populateLists() {
    this.measurementScheduleList = new Array<PatientSchedule>();
    this.otherScheduleList = new Array<PatientSchedule>();
    this.scheduleList.forEach((schedule) => {
      if(schedule.type==='MEASUREMENT') {
        this.measurementScheduleList.push(schedule);
      }
      else if(schedule.type==='MEDICINE') {
        this.otherScheduleList.push(schedule);
      }
    });
  }

  changeFocus(focus: string) {
    this.pageFocus = focus;
  }

  async newScheduleHandler() {
    // if(this.newPatientSchedule.type === 'MEASUREMENT') {
    //   await this.retrieveCollidingSchedules();
    //   if(this.collidingSchedules.length>0) {
    //     this.displayColliding = true;
    //     return;
    //   }
    // }
    this.patientService.postNewSchedule(this.newPatientSchedule)
      .subscribe(
        value => {
          this.pageFocus = 'list';
          this.patientService.getPatientScheduleList().subscribe(value => this.scheduleList = value);

          this.patientService.getPatientScheduleList()
            .subscribe((value) => {
              this.scheduleList = value;
              this.populateLists();
            });
          this.addSuccess = true;
          setTimeout(() => {this.addSuccess = false}, 5000);
        },
        error1 => {
          this.addFailure = true;
          this.retrieveCollidingSchedules();
          setTimeout(() => {this.addFailure = false}, 5000);
        }
      );
  }

  deleteScheduleHandler(scheduleId: number) {
    this.patientService.deletePatientSchedule(scheduleId)
      .subscribe(
        value => {
          this.patientService.getPatientScheduleList().subscribe(value => this.scheduleList = value);
          this.patientService.getPatientScheduleList()
            .subscribe((value) => {
              this.scheduleList = value;
              this.populateLists();
            });
        }
      )
  }
}
