import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DtPatientScheduleComponent } from './dt-patient-schedule.component';

describe('DtPatientScheduleComponent', () => {
  let component: DtPatientScheduleComponent;
  let fixture: ComponentFixture<DtPatientScheduleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DtPatientScheduleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DtPatientScheduleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
