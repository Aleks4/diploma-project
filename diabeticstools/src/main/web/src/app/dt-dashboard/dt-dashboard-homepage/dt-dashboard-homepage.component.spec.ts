import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DtDashboardHomepageComponent } from './dt-dashboard-homepage.component';

describe('DtDashboardHomepageComponent', () => {
  let component: DtDashboardHomepageComponent;
  let fixture: ComponentFixture<DtDashboardHomepageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DtDashboardHomepageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DtDashboardHomepageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
