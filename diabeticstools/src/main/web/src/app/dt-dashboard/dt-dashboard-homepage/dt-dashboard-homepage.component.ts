import {Component, OnDestroy, OnInit} from '@angular/core';
import {PatientService} from "../../service/patient-service";
import {MeasurementRequestDto} from "../../dto/measurement-request-dto";
import {PagingInfoDto} from "../../dto/paging-info-dto";
import {PatientBriefData} from "../../dto/patient-brief-data";

@Component({
  selector: 'app-dt-dashboard-homepage',
  templateUrl: './dt-dashboard-homepage.component.html',
  styleUrls: ['./dt-dashboard-homepage.component.scss']
})
export class DtDashboardHomepageComponent implements OnInit, OnDestroy {

  private requestDto: MeasurementRequestDto;
  public patient_brief: PatientBriefData;
  private timeout;
  private notificationSent: false;
  private permissionStatus: string;

  constructor(private patientService: PatientService) { }

  ngOnInit() {
    // Notification.requestPermission(permission => {
    //   this.permissionStatus = permission;
    // });

    this.requestDto = new MeasurementRequestDto();
    this.requestDto.paging_info = new PagingInfoDto();
    this.requestDto.paging_info.page_number = 0;
    this.requestDto.paging_info.page_size = 10;
    this.patientService.getPatientBriefData(this.requestDto)
      .subscribe(
        value => {this.patient_brief = value}
      );

    this.timeout = setInterval(() => {
      this.patientService.getPatientBriefData(this.requestDto)
        .subscribe(
          value => {this.patient_brief = value}
        );
    }, 60000);
  }
  simpleUnitTranslator(unit: string): string {
    if(unit === 'mgPerDL') return 'mg/dL';
    else if(unit === 'mmolPerL') return 'mmol/L';
    else return '';
  }

  ngOnDestroy(): void {
    clearInterval(this.timeout);
  }

}
