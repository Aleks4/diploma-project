import { Component, OnInit } from '@angular/core';
import {PatientService} from "../../service/patient-service";
import {ActivatedRoute, Router} from "@angular/router";
import {BriefPersonData} from "../../dto/brief-person-data";
import {Measurement} from "../../dto/measurement";
import {MeasurementListDto} from "../../dto/measurement-list-dto";
import {MeasurementRequestDto} from "../../dto/measurement-request-dto";
import {PagingInfoDto} from "../../dto/paging-info-dto";

@Component({
  selector: 'app-dt-measurement-history',
  templateUrl: './dt-measurement-history.component.html',
  styleUrls: ['./dt-measurement-history.component.scss']
})
export class DtMeasurementHistoryComponent implements OnInit {

  public search_mode: string = 'latest';
  private userBrief: BriefPersonData;
  public measurementList: MeasurementListDto;
  public searchbarPageSizes: number[] = [5,10,15,20,25,50,100];
  public requestDto: MeasurementRequestDto;
  public deletionSuccess: boolean = false;
  public deletionFailure: boolean = false;

  constructor(private patientService: PatientService,
              private route: ActivatedRoute,
              private router: Router) {
    this.userBrief = this.route.snapshot.data.user_brief;
    this.measurementList = this.route.snapshot.data.measurement_list;
    this.requestDto = new MeasurementRequestDto();
    this.requestDto.paging_info = new PagingInfoDto();
    this.requestDto.paging_info.page_size = 5;
    this.requestDto.paging_info.page_number = 0;
    this.requestDto.date_scope = 'WEEK';
  }

  ngOnInit() {
    this.requestDto.start_date = this.requestDto.stop_date = this.userBrief.current_date;
  }

  previousButtonHandler() {
    if(this.requestDto.paging_info.page_number>0) {
      this.requestDto.paging_info.page_number--;
      this.populateList();
    }
  }

  nextButtonHandler() {
    if(this.requestDto.paging_info.page_number<this.measurementList.paging_info.total_page_count-1) {
      this.requestDto.paging_info.page_number++;
      this.populateList();
    }
  }

  deleteButtonHandler(itemId: number) {
    this.patientService.deleteMeasurement(itemId)
      .subscribe(
        value => {
          this.deletionSuccess = true;
          this.populateList();
          setTimeout(() => {this.deletionSuccess = false}, 5000);
        },
        error1 => {
          this.deletionFailure = true;
          setTimeout(() => {this.deletionFailure = false}, 5000);
        }
      );
  }

  simpleUnitTranslator(unit: string): string {
    if(unit === 'mgPerDL') return 'mg/dL';
    else if(unit === 'mmolPerL') return 'mmol/L';
    else return '';
  }

  populateList() {
    if(this.search_mode === 'latest') {
      this.patientService.getLatestMeasurements(this.requestDto)
        .subscribe(
          value => {
            this.measurementList = value
          }
        );
    }
    if(this.search_mode === 'date') {
      this.patientService.getMeasurementsInRange(this.requestDto)
        .subscribe(
          value => {
            this.measurementList = value
          }
        );
    }
  }
}
