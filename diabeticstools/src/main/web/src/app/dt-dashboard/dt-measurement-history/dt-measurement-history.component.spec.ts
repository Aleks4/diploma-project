import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DtMeasurementHistoryComponent } from './dt-measurement-history.component';

describe('DtMeasurementHistoryComponent', () => {
  let component: DtMeasurementHistoryComponent;
  let fixture: ComponentFixture<DtMeasurementHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DtMeasurementHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DtMeasurementHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
