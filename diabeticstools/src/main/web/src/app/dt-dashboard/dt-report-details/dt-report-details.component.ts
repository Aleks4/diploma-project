import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {ReportDto} from "../../dto/report-dto";
import {BriefPersonData} from "../../dto/brief-person-data";
import {PatientSchedule} from "../../dto/patient-schedule";
import {ReportTableRow} from "../../dto/report-table-row";
import _ from "lodash";
import {ReportTableData} from "../../dto/report-table-data";
import {PatientService} from "../../service/patient-service";
import {ServerVariables} from "../../dto/server-variables";
import {UserProfileDto} from "../../dto/user-profile-dto";

@Component({
  selector: 'app-dt-report-details',
  templateUrl: './dt-report-details.component.html',
  styleUrls: ['./dt-report-details.component.scss'],
})
export class DtReportDetailsComponent implements OnInit {

  public reportDto: ReportDto;
  public userBrief: BriefPersonData;
  public userProfile: UserProfileDto;
  private componentLoaded = false;
  public reportTableView: [
    {patient_schedule: PatientSchedule;
      table_data: ReportTableRow[][];}
    ];


  view: any[] = [800, 200];
  legend: boolean = true;
  showLabels: boolean = true;
  animations: boolean = true;
  xAxis: boolean = true;
  yAxis: boolean = true;
  showYAxisLabel: boolean = true;
  showXAxisLabel: boolean = true;
  xAxisLabel: string = "Czas";
  yAxisLabel: string = "Glukoza";
  timeline: boolean = true;

  serverVariables: ServerVariables;
  chartData;


  constructor(private route: ActivatedRoute,
              private patientService: PatientService,
              private router: Router) {
    this.reportDto = this.route.snapshot.data.report;
    this.userBrief = this.route.snapshot.data.user_brief;
    this.serverVariables = this.route.snapshot.data.server_variables;
    this.userProfile = this.route.snapshot.data.user_profile;
    console.log(this.serverVariables);
  }

  onSelect(data): void {
    console.log("Item clicked", JSON.parse(JSON.stringify(data)));
  }

  onActivate(data): void {
    console.log("Activate", JSON.parse(JSON.stringify(data)));
  }

  onDeactivate(data): void {
    console.log("Deactivate", JSON.parse(JSON.stringify(data)));
  }

  back() {
    setTimeout(() => {this.router.navigate(['/dashboard/reports'])}, 2000);
  }

  ngOnInit() {
    // this.patientService.getReportById(parseInt(this.route.snapshot.paramMap.get('id')))
    //   .subscribe(
    //     value => this.reportDto = value
    //   );
    console.log('Ladowanie komponentu...');
    this.chartData = _(this.reportDto.report_tables_by_schedule)
      .map((value: ReportTableData) => ({
        name: value.patient_schedule.time,

        series: _(value.table_data.filter((value) => {return value.glucose!=='-'}))
          .map((tableRow: ReportTableRow) => ({
            name: tableRow.date,
            value: parseFloat(tableRow.glucose)
          })).toJSON()

      }))
      .toJSON();
      console.log(this.chartData);
      let dataForTables = JSON.parse(JSON.stringify(this.reportDto.report_tables_by_schedule));
      this.reportTableView = _(dataForTables)
        .map((value: ReportTableData, key) => ({
            patient_schedule: value.patient_schedule,
            table_data: this.splitTable(value.table_data.map((x) => x)),
          })
        ).toJSON();
  }
  formatNumber(num: string, fractionDigits: number) {
    return num!=='-'?parseFloat(num).toFixed(fractionDigits):"-";
  }

  splitTable(tableRows: ReportTableRow[]): ReportTableRow[][] {
    let splicedTable: ReportTableRow[][] = new Array<ReportTableRow[]>();
    while(tableRows.length>0) {
      splicedTable.push(tableRows.splice(0,18));
    }
    return splicedTable;
  }

  simpleUnitTranslator(unit: string): string {
    if(unit === 'mgPerDL') return 'mg/dL';
    else if(unit === 'mmolPerL') return 'mmol/L';
    else return '';
  }

  formatBloodPressureFirst(bp: string): string {
    let values = bp.split('/');
    return values[0]!=="-"?this.formatNumber(values[0], 0).toString():"-";
  }
  formatBloodPressureSecond(bp: string): string {
    let values = bp.split('/');
    return values[1]!=="-"?this.formatNumber(values[1], 0).toString():"-";
  }


  isGlucoseCorrect(glucose: string): boolean {
    return parseFloat(glucose) >= this.serverVariables.min_glucose && parseFloat(glucose) <= this.serverVariables.max_glucose
  }

  isBloodPressureUpperCorrect(pressure: string): boolean {
    let values = pressure.split('/');
    return parseFloat(values[0]) >= this.serverVariables.min_blood_pressure_upper && parseFloat(values[0]) <= this.serverVariables.max_blood_pressure_upper
  }

  isBloodPressureLowerCorrect(pressure: string): boolean {
    let values = pressure.split('/');
    return parseFloat(values[1]) >= this.serverVariables.min_blood_pressure_lower && parseFloat(values[1]) <= this.serverVariables.max_blood_pressure_lower
  }
}
