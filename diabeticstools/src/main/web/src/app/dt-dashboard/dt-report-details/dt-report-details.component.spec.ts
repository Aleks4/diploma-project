import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DtReportDetailsComponent } from './dt-report-details.component';

describe('DtReportDetailsComponent', () => {
  let component: DtReportDetailsComponent;
  let fixture: ComponentFixture<DtReportDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DtReportDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DtReportDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
