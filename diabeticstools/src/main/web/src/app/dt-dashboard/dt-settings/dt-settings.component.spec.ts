import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DtSettingsComponent } from './dt-settings.component';

describe('DtSettingsComponent', () => {
  let component: DtSettingsComponent;
  let fixture: ComponentFixture<DtSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DtSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DtSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
