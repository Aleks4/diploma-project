import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {PatientSettings} from "../../dto/patient-settings";
import {HttpClient} from "@angular/common/http";
import {SERVER_API_URL} from "../../app.constants";
import {BriefPersonData} from "../../dto/brief-person-data";
import {UserProfileDto} from "../../dto/user-profile-dto";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ValidationService} from "../../service/validation-service";
import {EmailValidationDto} from "../../dto/email-validation-dto";

@Component({
  selector: 'app-dt-patient-settings',
  templateUrl: './dt-settings.component.html',
  styleUrls: ['./dt-settings.component.scss']
})
export class DtSettingsComponent implements OnInit {


  public settingsSaveSuccess: boolean = false;
  public settingsSaveFailure: boolean = false;
  public userBrief: BriefPersonData;
  public userProfile: UserProfileDto;
  public userProfileForm: FormGroup;
  private settings: PatientSettings;
  private settingsForm: FormGroup;
  public emailValidationDto: EmailValidationDto = new EmailValidationDto();

  constructor(private route: ActivatedRoute,
              private http: HttpClient,
              private validationService: ValidationService) { }

  ngOnInit() {
    this.userBrief = this.route.snapshot.data.user_brief;
    this.settings = this.userBrief.patient_settings;
    this.userProfile = this.route.snapshot.data.user_profile;

    this.userProfileForm = new FormGroup({
      email: new FormControl(this.userProfile.email, [
        Validators.required,
        Validators.email
      ]),
      firstname: new FormControl(this.userProfile.firstname, [
        Validators.required,
      ]),
      lastname: new FormControl(this.userProfile.lastname, [
        Validators.required,
      ]),
      phoneNumber: new FormControl(this.userProfile.phoneNumber,
        Validators.compose([Validators.minLength(9), Validators.maxLength(9), Validators.pattern(/[0-9]{9}/)])
      ),
      default_glucose_unit: new FormControl(this.userProfile.default_glucose_unit, [Validators.required])
    });
    this.settingsForm = new FormGroup({
      includeBloodPressure: new FormControl(this.settings.includeBloodPressure),
      reportType: new FormControl(this.settings.reportType, [Validators.required]),
      reportPeriod: new FormControl(this.settings.reportPeriod),
      // doctorEmail: new FormControl(this.settings.doctorEmail, [
      //   Validators.email
      // ]),
      timeWindow: new FormControl(this.settings.timeWindow, [
        Validators.min(5),
        Validators.max(30),
      ])
    });
  }

  passwordMatchValidator(frm: FormGroup) {
    return frm.get('password').value ===
    frm.get('confirm_password').value ? null : {'mismatch': true};
  }

  validateProfileField(fieldName: string, errorName?: string): boolean {
    return errorName===undefined?this.userProfileForm.get(fieldName).invalid:this.userProfileForm.get(fieldName).hasError(errorName)&&
      //console.log(this.registerFormValidation.get(fieldName).errors);
      //return this.registerFormValidation.get(fieldName).invalid &&
      (this.userProfileForm.get(fieldName).dirty || this.userProfileForm.get(fieldName).touched);
  }

  validateSettingsField(fieldName: string, errorName?: string): boolean {
    return errorName===undefined?this.settingsForm.get(fieldName).invalid:this.settingsForm.get(fieldName).hasError(errorName)&&
      //console.log(this.registerFormValidation.get(fieldName).errors);
      //return this.registerFormValidation.get(fieldName).invalid &&
      (this.settingsForm.get(fieldName).dirty || this.settingsForm.get(fieldName).touched);
  }

  areProfileFieldsValid(fieldNames: string[]): boolean {
    let result: boolean = true;
    fieldNames.forEach(fieldName => {
      if(this.userProfileForm.get(fieldName).invalid) {
        result = false;
      }
    });
    return result;
  }

  areSettingsFieldsValid(fieldNames: string[]): boolean {
    let result: boolean = true;
    fieldNames.forEach(fieldName => {
      if(this.settingsForm.get(fieldName).invalid) {
        result = false;
      }
    });
    return result;
  }

  validateEmail() {
    console.log(this.userProfileForm.errors);
    this.emailValidationDto.email = this.userProfileForm.get('email').value;
    if(this.emailValidationDto.email.length === 0) return;
    this.validationService.validateEmail(this.emailValidationDto)
      .subscribe(
        value => {
          this.emailValidationDto = value;
        }
      )
  }

  postProfileHandler() {
    this.userProfile = this.userProfileForm.value;
    this.http.put(SERVER_API_URL+'/api/person/profile/change', this.userProfile)
      .subscribe(
        (value) => {
          this.settingsSaveSuccess = true;
          setTimeout(() => {this.settingsSaveSuccess = false}, 5000);
        },
        (error1) => {
          this.settingsSaveFailure = true;
          setTimeout(() => {this.settingsSaveFailure = false}, 5000);
        }
      );
  }

  postSettingsHandler() {
    this.settings = this.settingsForm.value;
    this.http.post(SERVER_API_URL+'/api/patient/settings/change', this.settings)
      .subscribe(
        (value) => {
          this.settingsSaveSuccess = true;
          setTimeout(() => {this.settingsSaveSuccess = false}, 5000);
        },
        (error1) => {
          this.settingsSaveFailure = true;
          setTimeout(() => {this.settingsSaveFailure = false}, 5000);
        }
      )
  }
}
