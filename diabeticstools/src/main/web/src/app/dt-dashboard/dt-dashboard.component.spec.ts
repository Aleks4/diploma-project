import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DtDashboardComponent } from './dt-dashboard.component';

describe('DtDashboardComponent', () => {
  let component: DtDashboardComponent;
  let fixture: ComponentFixture<DtDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DtDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DtDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
