import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {ReportBriefDto} from "../../dto/report-brief-dto";
import {PatientService} from "../../service/patient-service";
import {ReportRequestDto} from "../../dto/report-request-dto";
import {BriefPersonData} from "../../dto/brief-person-data";

@Component({
  selector: 'app-dt-report',
  templateUrl: './dt-report.component.html',
  styleUrls: ['./dt-report.component.scss']
})
export class DtReportComponent implements OnInit {

  public reportList: ReportBriefDto[] = null;
  public requestDto: ReportRequestDto = new ReportRequestDto();
  public generationSuccess = false;
  public generationFailure = false;
  private userBrief: BriefPersonData;

  constructor(private route: ActivatedRoute,
              private patientService: PatientService) {
    this.reportList = this.route.snapshot.data.report_list;
    this.userBrief = this.route.snapshot.data.user_brief;
    this.requestDto.start_date = this.requestDto.end_date = this.userBrief.current_date;
  }

  ngOnInit() {
  }

  generateReport() {
    this.patientService.generateReport(this.requestDto)
      .subscribe(
        value => {
          this.generationSuccess = true;
          this.fetchList();
          setTimeout(() => {this.generationSuccess = false},3000);
        }
      );
  }

  fetchList() {
    this.patientService.getReportList()
      .subscribe(
        value => this.reportList = value
      );
  }

}
