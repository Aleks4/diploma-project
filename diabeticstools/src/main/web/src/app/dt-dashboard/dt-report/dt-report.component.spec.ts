import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DtReportComponent } from './dt-report.component';

describe('DtReportComponent', () => {
  let component: DtReportComponent;
  let fixture: ComponentFixture<DtReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DtReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DtReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
