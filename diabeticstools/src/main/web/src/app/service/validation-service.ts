import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {SERVER_API_URL} from "../app.constants";
import {EmailValidationDto} from "../dto/email-validation-dto";
import {Observable} from "rxjs";

@Injectable()
export class ValidationService {
  constructor(private http: HttpClient) {

  }

  validateEmail(emailDto: EmailValidationDto): Observable<EmailValidationDto> {
    return this.http.post<EmailValidationDto>(SERVER_API_URL+'/api/validation/email', emailDto);
  }
}
