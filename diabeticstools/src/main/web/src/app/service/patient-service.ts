import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {PatientSchedule} from "../dto/patient-schedule";
import {SERVER_API_URL} from "../app.constants";
import {Measurement} from "../dto/measurement";
import {MeasurementRequestDto} from "../dto/measurement-request-dto";
import {MeasurementListDto} from "../dto/measurement-list-dto";
import {PatientSettings} from "../dto/patient-settings";
import {PatientBriefData} from "../dto/patient-brief-data";
import {ReportBriefDto} from "../dto/report-brief-dto";
import {ReportDto} from "../dto/report-dto";
import {ReportRequestDto} from "../dto/report-request-dto";
import {ServerVariables} from "../dto/server-variables";

@Injectable()
export class PatientService {
  constructor(private http: HttpClient) {

  }

  getPatientScheduleList(): Observable<PatientSchedule[]> {
    return this.http.get<PatientSchedule[]>(SERVER_API_URL+'/api/patient/schedule/list');
  }

  getPatientScheduleById(scheduleId: number): Observable<PatientSchedule> {
    return this.http.get<PatientSchedule>(SERVER_API_URL+'/api/patient/schedule/'+scheduleId);
  }

  retrieveColliding(schedule: PatientSchedule): Observable<PatientSchedule[]> {
    return this.http.post<PatientSchedule[]>(SERVER_API_URL+'/api/patient/schedule/colliding', schedule);
  }

  postNewSchedule(newSchedule: PatientSchedule): Observable<any> {
    return this.http.post<Observable<any>>(SERVER_API_URL+'/api/patient/schedule/new', newSchedule);
  }

  editSchedule(schedule: PatientSchedule): Observable<PatientSchedule> {
    return this.http.put<PatientSchedule>(SERVER_API_URL+'/api/patient/schedule/edit', schedule);
  }

  deletePatientSchedule(scheduleId: number):Observable<any> {
    return this.http.delete<Observable<any>>(SERVER_API_URL+'/api/patient/schedule/delete/'+scheduleId);
  }

  postNewMeasurement(newMeasurement: Measurement): Observable<any> {
    return this.http.post(SERVER_API_URL+'/api/measurement/new', newMeasurement);
  }

  getLatestMeasurements(requestDto: MeasurementRequestDto): Observable<MeasurementListDto> {
    return this.http.post<MeasurementListDto>(SERVER_API_URL+'/api/measurement/history/range/latest', requestDto);
  }
  getMeasurementsInRange(requestDto: MeasurementRequestDto): Observable<MeasurementListDto> {
    return this.http.post<MeasurementListDto>(SERVER_API_URL+'/api/measurement/history/range/date', requestDto);
  }

  getMeasurementById(measurementId: number): Observable<Measurement> {
    return this.http.get<Measurement>(SERVER_API_URL+'/api/measurement/'+measurementId);
  }

  editMeasurement(measurement:Measurement): Observable<Measurement> {
    return this.http.put<Measurement>(SERVER_API_URL+'/api/measurement/edit', measurement);
  }

  deleteMeasurement(itemId: number): Observable<any> {
    return this.http.delete<Observable<any>>(SERVER_API_URL+'/api/measurement/delete/'+itemId);
  }

  getDefaultSettings(): Observable<PatientSettings> {
    return this.http.get<PatientSettings>("/api/patient/settings/default");
  }

  getPatientBriefData(requestDto: MeasurementRequestDto): Observable<PatientBriefData> {
    return this.http.post<PatientBriefData>(SERVER_API_URL+'/api/patient/brief', requestDto);
  }

  getReportList(): Observable<ReportBriefDto[]> {
    return this.http.get<ReportBriefDto[]>(SERVER_API_URL+'/api/report/list');
  }

  getReportById(id: number): Observable<ReportDto> {
    return this.http.get<ReportDto>(SERVER_API_URL+'/api/report/'+id);
  }

  generateReport(requestDto: ReportRequestDto): Observable<any> {
    return this.http.post(SERVER_API_URL+'/api/report/new', requestDto);
  }

  getServerVariables(): Observable<ServerVariables> {
    return this.http.get<ServerVariables>(SERVER_API_URL+'/api/patient/variables');
  }
}
