import {Injectable, OnInit} from "@angular/core";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {SERVER_API_URL} from "../app.constants";
import {Observable} from "rxjs";
import {BriefPersonData} from "../dto/brief-person-data";
import {LocalStorageService, SessionStorageService} from "ngx-webstorage";
import {map} from "rxjs/operators";
import {Credentials} from "../dto/credentials";
import {UserProfileDto} from "../dto/user-profile-dto";
import {RegistrationDto} from "../dto/registration-dto";

@Injectable()
export class LoginService implements OnInit {

  private user_brief: BriefPersonData;
  private rememberMe: boolean;

  constructor(private http: HttpClient,
              private $localStorage: LocalStorageService,
              private $sessionStorage: SessionStorageService) {
  }

  ngOnInit() {
    this.user_brief = null;
  }

  logout(): Observable<any> {

    return this.http.post(SERVER_API_URL+'/api/logout', null).pipe();
  }

  clearSessionData() {
    this.$sessionStorage.clear('id_token');
    this.$localStorage.clear('id_token');
  }

  login(username: string, password: string, rememberMe: boolean): Observable<JWTToken> {
    let credentials = new Credentials();
    credentials.email = username;
    credentials.password = password;
    credentials.rememberMe = rememberMe;

    function authSuccessHandler(response: JWTToken) {
      const bearerToken = response.id_token;
      this.storeAuthenticationToken(bearerToken, rememberMe);
      return bearerToken;
    }
    return this.http.post<JWTToken>(SERVER_API_URL+'/api/authenticate', credentials).pipe(map(authSuccessHandler.bind(this)));
  }

  storeAuthenticationToken(jwt: string, rememberMe: boolean) {
    this.rememberMe = rememberMe;
    if (rememberMe) {
      this.$localStorage.store('id_token', jwt);
    } else {
      this.$sessionStorage.store('id_token', jwt);
    }
  }

  getUserBrief(requestHeaders?: HttpHeaders): Observable<BriefPersonData> {
    return this.http.get<BriefPersonData>(SERVER_API_URL+'/api/person/profile/brief', {headers: requestHeaders});
  }

  getUserProfile(): Observable<UserProfileDto> {
    return this.http.get<UserProfileDto>(SERVER_API_URL+'/api/person/profile');
  }

  isLoggedIn(): boolean {
    return this.$localStorage.retrieve('id_token') !== null || this.$sessionStorage.retrieve('id_token') !== null;
  }

  registerNewUser(registrationDto: RegistrationDto): Observable<any> {
    return this.http.post(SERVER_API_URL+'/api/register', registrationDto);
  }
}

export class JWTToken {
  id_token: string;
}
