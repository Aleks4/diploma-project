import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable} from "rxjs";
import {Injectable} from "@angular/core";
import {LocalStorageService, SessionStorageService} from "ngx-webstorage";
import {SERVER_API_URL} from "../app.constants";
import {tap} from "rxjs/operators";
import {Router} from "@angular/router";

@Injectable()
export class AuthHttpInterceptor implements HttpInterceptor{

  constructor(private $sessionStorage: SessionStorageService,
              private $localStorage: LocalStorageService,
              private router: Router) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (!request || !request.url || (request.url.startsWith('http') && !(SERVER_API_URL && request.url.startsWith(SERVER_API_URL)))) {
      return next.handle(request).pipe(
        tap(() => {},
          (err) => {
            if (err instanceof HttpErrorResponse) {
              if (err.status !== 401) {
                return;
              }
              this.router.navigate(['login', {status: '401'}]);
              this.$localStorage.clear('id_token');
              this.$sessionStorage.clear('id_token');
            }
          }
        )
      );
    }
    let token: string = this.$localStorage.retrieve('id_token') || this.$sessionStorage.retrieve('id_token');
    if (token) {
      request = request.clone({
        setHeaders: {
          Authorization: 'Bearer ' + token
        }
      });
    }
    return next.handle(request).pipe(
      tap(() => {},
        (err) => {
          if (err instanceof HttpErrorResponse) {
            if (err.status !== 401) {
              return;
            }
            this.$localStorage.clear('id_token');
            this.$sessionStorage.clear('id_token');
            this.router.navigate(['login', {status: '401'}]);
          }
        }
      )
    );
  }
}
