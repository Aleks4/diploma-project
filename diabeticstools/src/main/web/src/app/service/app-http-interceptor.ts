import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpHeaders,
  HttpInterceptor,
  HttpRequest
} from "@angular/common/http";
import {Observable} from "rxjs";
import {Injectable} from "@angular/core";
import {Router} from "@angular/router";
import {tap} from "rxjs/internal/operators/tap";

@Injectable()
export class AppHttpInterceptor implements HttpInterceptor {

  constructor(private router: Router) {

  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if(!req.url.endsWith('/login') && !req.url.endsWith('/logout')) {
      const contentTypeReq = req.clone({
        setHeaders: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Access-Control-Allow-Headers': 'Content-Type',
        },
        withCredentials: true
      });
      return next.handle(contentTypeReq);
    }
    return next.handle(req);
  }

}
