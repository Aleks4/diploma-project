import {Measurement} from "../dto/measurement";
import {Observable} from "rxjs";
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from "@angular/router";
import {PatientService} from "../service/patient-service";
import {Injectable} from "@angular/core";

@Injectable()
export class MeasurementResolver implements Resolve<Observable<Measurement>> {

  constructor(private patientService: PatientService) {

  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Observable<Measurement>> | Promise<Observable<Measurement>> | Observable<Measurement> {
    return this.patientService.getMeasurementById(parseInt(route.paramMap.get('id')));
  }

}
