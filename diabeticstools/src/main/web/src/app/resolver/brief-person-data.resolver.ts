import {Observable} from "rxjs";
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from "@angular/router";
import {Injectable} from "@angular/core";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {SERVER_API_URL} from "../app.constants";
import {BriefPersonData} from "../dto/brief-person-data";
import {LoginService} from "../service/login-service";

@Injectable()
export class BriefPersonDataResolver implements Resolve<Observable<BriefPersonData>> {

  constructor(private http: HttpClient,
              private loginService: LoginService) {

  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Observable<any>> | Promise<Observable<any>> | Observable<any> {
    //return undefined;
    const requestHeaders = new HttpHeaders();
    requestHeaders.append('Content-Type', 'application/json');
    requestHeaders.append('Accept', 'application/json');
    requestHeaders.append('Access-Control-Allow-Headers', 'Content-Type');
    return this.loginService.getUserBrief(requestHeaders);
  }
}
