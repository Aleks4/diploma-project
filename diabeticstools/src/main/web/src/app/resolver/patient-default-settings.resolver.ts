import {PatientSettings} from "../dto/patient-settings";
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from "@angular/router";
import {Observable} from "rxjs";
import {PatientService} from "../service/patient-service";
import {Injectable} from "@angular/core";

@Injectable()
export class PatientDefaultSettingsResolver implements Resolve<PatientSettings> {

  constructor(private patientService: PatientService) {

  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<PatientSettings> | Promise<PatientSettings> | PatientSettings {
    return this.patientService.getDefaultSettings();
  }

}
