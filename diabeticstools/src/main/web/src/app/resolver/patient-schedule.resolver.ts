import {Observable} from "rxjs";
import {PatientSchedule} from "../dto/patient-schedule";
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from "@angular/router";
import {Injectable} from "@angular/core";
import {PatientService} from "../service/patient-service";

@Injectable()
export class PatientScheduleResolver implements Resolve<Observable<PatientSchedule>> {

  constructor(private patientService: PatientService){

  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Observable<PatientSchedule>> | Promise<Observable<PatientSchedule>> | Observable<PatientSchedule> {
    return this.patientService.getPatientScheduleById(parseInt(route.paramMap.get('id')));
  }

}
