import {Observable} from "rxjs";
import {ReportDto} from "../dto/report-dto";
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from "@angular/router";
import {PatientService} from "../service/patient-service";
import {Injectable} from "@angular/core";

@Injectable()
export class ReportResolver implements Resolve<Observable<ReportDto>> {

  constructor(private patientService: PatientService) {

  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Observable<ReportDto>> | Promise<Observable<ReportDto>> | Observable<ReportDto> {
    return this.patientService.getReportById(parseInt(route.paramMap.get('id')));
  }
}
