import {Observable} from "rxjs";
import {MeasurementListDto} from "../dto/measurement-list-dto";
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from "@angular/router";
import {Injectable} from "@angular/core";
import {MeasurementRequestDto} from "../dto/measurement-request-dto";
import {PatientService} from "../service/patient-service";
import {PagingInfoDto} from "../dto/paging-info-dto";

@Injectable()
export class MeasurementListResolver implements Resolve<Observable<MeasurementListDto>> {
  private readonly requestDto: MeasurementRequestDto;
  constructor(private patientService: PatientService) {
    this.requestDto = new MeasurementRequestDto();
    this.requestDto.date_scope = 'WEEK';
    this.requestDto.paging_info = new PagingInfoDto();
    this.requestDto.paging_info.page_size = 5;
    this.requestDto.paging_info.page_number = 0;
  }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Observable<MeasurementListDto>> | Promise<Observable<MeasurementListDto>> | Observable<MeasurementListDto> {
    return this.patientService.getLatestMeasurements(this.requestDto);
  }

}
