import {UserProfileDto} from "../dto/user-profile-dto";
import {Observable} from "rxjs";
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from "@angular/router";
import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {SERVER_API_URL} from "../app.constants";
import {LoginService} from "../service/login-service";

@Injectable()
export class UserProfileResolver implements Resolve<Observable<UserProfileDto>> {
  constructor(private loginService: LoginService) {

  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Observable<UserProfileDto>> | Promise<Observable<UserProfileDto>> | Observable<UserProfileDto> {
    return this.loginService.getUserProfile();
  }

}
