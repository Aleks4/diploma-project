import {Resolve} from "@angular/router/src/interfaces";
import {ReportBriefDto} from "../dto/report-brief-dto";
import {Observable} from "rxjs";
import {ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {Injectable} from "@angular/core";
import {PatientService} from "../service/patient-service";

@Injectable()
export class ReportListResolver implements Resolve<Observable<ReportBriefDto[]>> {

  constructor(private patientService: PatientService) {

  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Observable<ReportBriefDto[]>> | Promise<Observable<ReportBriefDto[]>> | Observable<ReportBriefDto[]> {
    return this.patientService.getReportList();
  }
}
