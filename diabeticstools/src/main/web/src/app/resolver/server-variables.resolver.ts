import {Observable} from "rxjs";
import {ServerVariables} from "../dto/server-variables";
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from "@angular/router";
import {PatientService} from "../service/patient-service";
import {Injectable} from "@angular/core";

@Injectable()
export class ServerVariablesResolver implements Resolve<Observable<ServerVariables>> {

  constructor(private patientService: PatientService) {

  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Observable<ServerVariables>> | Promise<Observable<ServerVariables>> | Observable<ServerVariables> {
    return this.patientService.getServerVariables();
  }

}
