import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from "@angular/router";
import {PatientBriefData} from "../dto/patient-brief-data";
import {Observable} from "rxjs";
import {PatientService} from "../service/patient-service";
import {Injectable} from "@angular/core";
import {MeasurementRequestDto} from "../dto/measurement-request-dto";
import {PagingInfoDto} from "../dto/paging-info-dto";

@Injectable()
export class PatientBriefDataResolver implements Resolve<Observable<PatientBriefData>> {

  constructor(private patientService: PatientService) {

  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Observable<PatientBriefData>> | Promise<Observable<PatientBriefData>> | Observable<PatientBriefData> {
    let requestDto: MeasurementRequestDto = new MeasurementRequestDto();
    requestDto.paging_info = new PagingInfoDto();
    requestDto.paging_info.page_number = 0;
    requestDto.paging_info.page_size = 10;
    return this.patientService.getPatientBriefData(requestDto);
  }
}
