import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DtUserSidebarComponent } from './dt-user-sidebar.component';

describe('DtUserSidebarComponent', () => {
  let component: DtUserSidebarComponent;
  let fixture: ComponentFixture<DtUserSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DtUserSidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DtUserSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
