import { Component, OnInit } from '@angular/core';
import {LoginService} from "../service/login-service";
import {ActivatedRoute, Router} from "@angular/router";
import {BriefPersonData} from "../dto/brief-person-data";

@Component({
  selector: 'app-dt-user-sidebar',
  templateUrl: './dt-user-sidebar.component.html',
  styleUrls: ['./dt-user-sidebar.component.scss']
})
export class DtUserSidebarComponent implements OnInit {
  public user: BriefPersonData;
  constructor(private loginService: LoginService,
              private router: Router,
              private route: ActivatedRoute) {

  }

  ngOnInit() {
    // this.loginService.getUserBrief().subscribe(
    //   value => {this.user = value},
    //     error1 => {console.log('error tutaj')}
    //     )
    this.user =  this.route.snapshot.data.user_brief;
  }

  logoutHandler() {

    this.loginService.logout().subscribe(

      (value) => {
        this.loginService.clearSessionData();
        this.router.navigate(['/login', {status: 'logout'}]);
      },
      (error1) => {
        this.loginService.clearSessionData();
        this.router.navigate(['/login', {status: 'logout'}]);
      }
    );
  }
}
