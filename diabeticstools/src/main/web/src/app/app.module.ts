import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {DtLoginPageComponent} from './dt-login-page/dt-login-page.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {DtDashboardComponent} from './dt-dashboard/dt-dashboard.component';
import {DtUserSidebarComponent} from './dt-user-sidebar/dt-user-sidebar.component';
import {DtRegisterFormComponent} from './dt-register-form/dt-register-form.component';
import {Route, RouterModule, Routes} from "@angular/router";
import {LoginService} from "./service/login-service";
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule, HttpHandler} from "@angular/common/http";
import {BriefPersonDataResolver} from "./resolver/brief-person-data.resolver";
import {AppHttpInterceptor} from "./service/app-http-interceptor";
import {AuthHttpInterceptor} from "./service/auth-http-interceptor";
import {NgxWebstorageModule} from "ngx-webstorage";
import { DtSettingsComponent } from './dt-dashboard/dt-settings/dt-settings.component';
import { DtNewMeasurementComponent } from './dt-dashboard/dt-new-measurement/dt-new-measurement.component';
import { DtDashboardHomepageComponent } from './dt-dashboard/dt-dashboard-homepage/dt-dashboard-homepage.component';
import {UserProfileResolver} from "./resolver/user-profile.resolver";
import { DtPatientScheduleComponent } from './dt-dashboard/dt-patient-schedule/dt-patient-schedule.component';
import {PatientScheduleListResolver} from "./resolver/patient-schedule-list.resolver";
import {PatientService} from "./service/patient-service";
import { DtMeasurementHistoryComponent } from './dt-dashboard/dt-measurement-history/dt-measurement-history.component';
import {MeasurementListResolver} from "./resolver/measurement-list-resolver";
import {PatientDefaultSettingsResolver} from "./resolver/patient-default-settings.resolver";
import {ValidationService} from "./service/validation-service";
import {PatientBriefDataResolver} from "./resolver/patient-brief-data.resolver";
import { DtReportComponent } from './dt-dashboard/dt-report/dt-report.component';
import {ReportListResolver} from "./resolver/report-list-resolver";
import { DtReportDetailsComponent } from './dt-dashboard/dt-report-details/dt-report-details.component';
import {ReportResolver} from "./resolver/report.resolver";
import {NgxPrintModule} from "ngx-print";
import {NgxChartsModule} from "@swimlane/ngx-charts";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import { DtMeasurementDetailsComponent } from './dt-dashboard/dt-measurement-details/dt-measurement-details.component';
import {MeasurementResolver} from "./resolver/measurement-resolver";
import {ServerVariablesResolver} from "./resolver/server-variables.resolver";
import { DtScheduleEditComponent } from './dt-dashboard/dt-schedule-edit/dt-schedule-edit.component';
import {PatientScheduleResolver} from "./resolver/patient-schedule.resolver";


const routes: Routes = [
  {path: '', redirectTo: 'login', pathMatch: 'full'},
  {
    path: 'login',
    component: DtLoginPageComponent
  },
  {
    path: 'register',
    component: DtRegisterFormComponent,
    resolve: {
      patient_settings_default: PatientDefaultSettingsResolver
    }
  },
  {
    path: 'dashboard',
    component: DtDashboardComponent,
    resolve: {user_brief: BriefPersonDataResolver},
    children: [
      {path: '', redirectTo: 'home', pathMatch: 'full'},
      {
        path: 'home',
        component: DtDashboardHomepageComponent
      },
      {
        path: 'settings',
        component: DtSettingsComponent,
        resolve: {
          user_brief: BriefPersonDataResolver,
          user_profile: UserProfileResolver
        }
        },
      {
        path: 'new-measurement',
        component: DtNewMeasurementComponent,
        resolve: {
          user_brief: BriefPersonDataResolver,
          user_profile: UserProfileResolver
        }
      },
      {
        path: 'measurement/:id',
        component: DtMeasurementDetailsComponent,
        resolve: {
          measurement: MeasurementResolver,
          user_brief: BriefPersonDataResolver
        }
      },
      {
        path: 'schedule',
        component: DtPatientScheduleComponent,
        resolve: {
          patient_schedule_list: PatientScheduleListResolver,
          user_brief: BriefPersonDataResolver
        }
      },
      {
        path: 'schedule/:id',
        component: DtScheduleEditComponent,
        resolve: {
          patient_schedule: PatientScheduleResolver,
          user_brief: BriefPersonDataResolver
        }
      },
      {
        path: 'history',
        component: DtMeasurementHistoryComponent,
        resolve: {
          user_brief: BriefPersonDataResolver,
          measurement_list: MeasurementListResolver
        }
      },
      {
        path: 'reports',
        component: DtReportComponent,
        resolve: {
          report_list: ReportListResolver,
          user_brief: BriefPersonDataResolver
        }
      },
      {
        path: 'report/:id',
        component: DtReportDetailsComponent,
        resolve: {
          report: ReportResolver,
          user_brief: BriefPersonDataResolver,
          server_variables: ServerVariablesResolver,
          user_profile: UserProfileResolver
        }
      }
    ]
  }
];

@NgModule({
  declarations: [
    AppComponent,
    DtLoginPageComponent,
    DtDashboardComponent,
    DtUserSidebarComponent,
    DtRegisterFormComponent,
    DtSettingsComponent,
    DtNewMeasurementComponent,
    DtDashboardHomepageComponent,
    DtPatientScheduleComponent,
    DtMeasurementHistoryComponent,
    DtReportComponent,
    DtReportDetailsComponent,
    DtMeasurementDetailsComponent,
    DtScheduleEditComponent

  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    NgxWebstorageModule.forRoot(),
    NgxPrintModule,
    NgxChartsModule,
    BrowserAnimationsModule
  ],
  providers: [
    LoginService,
    HttpClient,
    BriefPersonDataResolver,
    UserProfileResolver,
    PatientScheduleListResolver,
    PatientService,
    MeasurementListResolver,
    PatientDefaultSettingsResolver,
    ValidationService,
    PatientBriefDataResolver,
    ReportListResolver,
    ReportResolver,
    MeasurementResolver,
    ServerVariablesResolver,
    PatientScheduleResolver,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AppHttpInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthHttpInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
