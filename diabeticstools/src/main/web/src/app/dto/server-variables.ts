export class ServerVariables {
  min_glucose: number;
  max_glucose: number;
  min_blood_pressure_upper: number;
  max_blood_pressure_upper: number;
  min_blood_pressure_lower: number;
  max_blood_pressure_lower: number;
}
