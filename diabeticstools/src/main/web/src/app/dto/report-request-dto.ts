export class ReportRequestDto {
  start_date: string;
  end_date: string;
}
