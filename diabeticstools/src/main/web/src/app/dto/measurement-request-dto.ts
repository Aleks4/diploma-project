import {PagingInfoDto} from "./paging-info-dto";

export class MeasurementRequestDto {
  date_scope: string;
  paging_info: PagingInfoDto;
  start_date: string;
  stop_date: string;
}
