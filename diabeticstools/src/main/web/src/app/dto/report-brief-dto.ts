export class ReportBriefDto {
  id: number;
  start_date: string;
  end_date: string;
  creation_date: string;
  period: string;
  type: string;
}
