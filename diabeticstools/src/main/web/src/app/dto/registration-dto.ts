import {PatientSettings} from "./patient-settings";

export class RegistrationDto {
  email: string;
  password: string;
  confirm_password: string;
  firstname: string;
  lastname: string;
  phone_number: string;
  default_glucose_unit: string;
  patient_settings: PatientSettings;
}
