import {ReportTableData} from "./report-table-data";

export class ReportDto {
  id: number;
  start_date: string;
  end_date: string;
  creation_date: string;
  period: string;
  type: string;
  report_tables_by_schedule: ReportTableData[];
}
