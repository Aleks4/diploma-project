export class PagingInfoDto {
  total_item_count: number;
  total_page_count: number;
  page_number: number;
  page_size: number;
}
