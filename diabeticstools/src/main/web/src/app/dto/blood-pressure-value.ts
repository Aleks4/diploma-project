export class BloodPressureValue {
  upperValue: number;
  lowerValue: number;
  unit: string = 'mmHg';
}
