import {PatientSchedule} from "./patient-schedule";
import {ReportTableRow} from "./report-table-row";

export class ReportTableData {
  patient_schedule: PatientSchedule;
  table_data: ReportTableRow[];
}
