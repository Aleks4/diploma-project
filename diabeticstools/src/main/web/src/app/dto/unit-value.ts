export class UnitValue {
  type: string = "glucose";
  value: number;
  unit: string;
}
