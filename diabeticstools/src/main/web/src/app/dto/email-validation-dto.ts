export class EmailValidationDto {
  email: string;
  isCorrect: boolean = true;
  isFree: boolean = true;
}
