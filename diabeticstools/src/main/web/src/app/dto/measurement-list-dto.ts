import {Measurement} from "./measurement";
import {PagingInfoDto} from "./paging-info-dto";

export class MeasurementListDto {
  measurement_list: Measurement[];
  paging_info: PagingInfoDto;
}
