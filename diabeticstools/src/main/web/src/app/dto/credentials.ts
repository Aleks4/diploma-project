export class Credentials {
  email: string;
  password: string;
  rememberMe: boolean;
}
