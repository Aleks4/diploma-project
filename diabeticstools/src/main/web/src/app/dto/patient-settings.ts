export class PatientSettings {
  includeBloodPressure: boolean;
  reportType: string;
  reportPeriod: string;
  doctorEmail: string;
  timeWindow: number;
}
