export class ReportTableRow {
  date: string;
  glucose: string;
  blood_pressure: string;
}
