import { Component } from '@angular/core';
import {SERVER_API_URL} from "./app.constants";
import {LoginService} from "./service/login-service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'dt-app';

  constructor(private loginService: LoginService,
              private router: Router
              ) {

  }

}
