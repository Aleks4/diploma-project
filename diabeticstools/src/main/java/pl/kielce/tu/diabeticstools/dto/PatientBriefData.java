package pl.kielce.tu.diabeticstools.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PatientBriefData {
    @JsonProperty("active_schedules")
    private List<PatientScheduleDto> activeSchedules;
    @JsonProperty("next_schedule")
    private PatientScheduleDto nextSchedule;
    @JsonProperty("measurements_today")
    private MeasurementListDto measurementsToday;
}
