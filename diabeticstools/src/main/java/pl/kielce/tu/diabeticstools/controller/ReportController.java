package pl.kielce.tu.diabeticstools.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.kielce.tu.diabeticstools.dto.ReportBriefDto;
import pl.kielce.tu.diabeticstools.dto.ReportDto;
import pl.kielce.tu.diabeticstools.dto.ReportRequestDto;
import pl.kielce.tu.diabeticstools.model.Report;
import pl.kielce.tu.diabeticstools.security.SecurityUtils;
import pl.kielce.tu.diabeticstools.service.ReportService;

import java.util.List;

@RestController
@RequestMapping("/api/report")
public class ReportController {

    private final SecurityUtils securityUtils;

    private final ReportService reportService;

    @Autowired
    public ReportController(SecurityUtils securityUtils, ReportService reportService) {
        this.securityUtils = securityUtils;
        this.reportService = reportService;
    }

//    @GetMapping("/test")
//    public ResponseEntity<Report> getReportForPatient() throws Exception{
//        return ResponseEntity.ok(reportService.generatePeriodicReportForPatient(securityUtils.getCurrentPatient()));
//    }

    @GetMapping("/{reportId}")
    public ResponseEntity<ReportDto> getReportById(@PathVariable Long reportId) throws Exception {
        return ResponseEntity.ok(reportService.getReportById(securityUtils.getCurrentPatient(), reportId));
    }

    @GetMapping("/list")
    public ResponseEntity<List<ReportBriefDto>> getReportBriefList() throws Exception{
        return ResponseEntity.ok(reportService.getReportBriefList(securityUtils.getCurrentPatient()));
    }

    @PostMapping("/new")
    public ResponseEntity<ReportDto> generateManualReport(@RequestBody ReportRequestDto requestDto) throws Exception{
        reportService.generateManualReportForPatient(securityUtils.getCurrentPatient(), requestDto);
        return ResponseEntity.ok().build();
    }
}
