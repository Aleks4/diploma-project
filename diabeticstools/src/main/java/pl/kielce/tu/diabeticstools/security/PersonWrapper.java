package pl.kielce.tu.diabeticstools.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import pl.kielce.tu.diabeticstools.model.Person;
import pl.kielce.tu.diabeticstools.model.Role;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;

public class PersonWrapper implements UserDetails {

    private Person person;

    public PersonWrapper(Person person) {
        this.person = person;
    }

    @Override

    public Collection<? extends GrantedAuthority> getAuthorities() {
        SimpleGrantedAuthority y = new SimpleGrantedAuthority(person.getRole().toString());
        //logger.info(y.getAuthority()+" <- FROM PERSON WRAPPER");

        return List.of(y);
    }

    @Override
    public String getPassword() {
        return person.getPassword();
    }

    @Override
    public String getUsername() {
        return person.getEmail();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return !person.getLocked();
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return person.getActive();
    }
}
