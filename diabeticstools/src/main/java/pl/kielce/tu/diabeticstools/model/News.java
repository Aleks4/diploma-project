package pl.kielce.tu.diabeticstools.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@Data
@Entity
public class News implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String subject;

    @ManyToOne(optional = false)
    private Person author;

    private Timestamp date;

    @Column(length = 4096)
    private String content;
}
