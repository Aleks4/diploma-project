package pl.kielce.tu.diabeticstools.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MeasurementListDto {
    @JsonProperty("measurement_list")
    private List<MeasurementDto> measurementList;

    @JsonProperty("paging_info")
    private PagingInfo pagingInfo;
}
