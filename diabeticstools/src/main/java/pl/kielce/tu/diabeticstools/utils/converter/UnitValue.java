package pl.kielce.tu.diabeticstools.utils.converter;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.NoArgsConstructor;
import pl.kielce.tu.diabeticstools.model.embeddable.GlucoseValue;

import java.math.BigDecimal;

@NoArgsConstructor
public abstract class UnitValue {
    protected String type;

    abstract public BigDecimal getValue();

    abstract public Unit getUnit();

    abstract public void setValue(BigDecimal value);

    abstract public void setUnit(Unit unit) throws IllegalArgumentException;

    UnitValue initialValue(BigDecimal value) {
        this.setValue(value);
        return this;
    }

    UnitValue initialUnit(Unit unit) {
        this.setUnit(unit);
        return this;
    }
}
