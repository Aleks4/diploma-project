package pl.kielce.tu.diabeticstools.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import pl.kielce.tu.diabeticstools.dto.RegistrationDto;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class Patient extends Person implements Serializable {

    @OneToOne
    private PatientSettings settings;

    private Timestamp nextReportDate;

    public Patient(RegistrationDto registrationDto) {
        this.email = registrationDto.getEmail();
        this.password = registrationDto.getPassword();
        this.firstname = registrationDto.getFirstname();
        this.lastname = registrationDto.getLastname();
        this.phoneNumber = registrationDto.getPhoneNumber();
        this.defaultGlucoseUnit = registrationDto.getDefaultGlucoseUnit();
        this.settings = registrationDto.getPatientSettings()!=null?registrationDto.getPatientSettings():new PatientSettings();
    }
}
