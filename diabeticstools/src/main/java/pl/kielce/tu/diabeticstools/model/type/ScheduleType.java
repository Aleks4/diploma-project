package pl.kielce.tu.diabeticstools.model.type;

public enum ScheduleType {
    MEASUREMENT,
    MEDICINE
}
