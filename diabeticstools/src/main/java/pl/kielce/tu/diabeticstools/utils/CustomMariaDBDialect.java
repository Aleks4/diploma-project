package pl.kielce.tu.diabeticstools.utils;

import org.hibernate.dialect.MariaDB10Dialect;

public class CustomMariaDBDialect extends MariaDB10Dialect {
    @Override
    public String getTableTypeString() {
        return super.getTableTypeString() + " ROW_FORMAT=DYNAMIC";
    }
}
