package pl.kielce.tu.diabeticstools.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.kielce.tu.diabeticstools.dto.MeasurementDto;
import pl.kielce.tu.diabeticstools.model.embeddable.BloodPressureValue;
import pl.kielce.tu.diabeticstools.model.embeddable.GlucoseValue;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Measurement implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private Patient patient;

    @Embedded
    private GlucoseValue glucose;

    @Embedded
    private BloodPressureValue bloodPressure;

    private Boolean editable = true;

    private Timestamp insertionTimestamp;

    @ManyToOne
    private PatientSchedule patientSchedule;

    private String patientComment;

    public Measurement(MeasurementDto measurementDto) {
        this.glucose = new GlucoseValue(measurementDto.getGlucose_value(), measurementDto.getGlucose_unit());
        this.bloodPressure = new BloodPressureValue(measurementDto.getBlood_pressure_upper(), measurementDto.getBlood_pressure_lower(), measurementDto.getBlood_pressure_unit());
        this.setInsertionTimestamp(measurementDto.getInsertionTimestamp());
        this.patientComment = measurementDto.getPatientComment();
    }
}
