package pl.kielce.tu.diabeticstools.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.kielce.tu.diabeticstools.dto.validation.EmailValidationDto;
import pl.kielce.tu.diabeticstools.service.ValidationService;

@RestController
@RequestMapping("/api/validation")
public class ValidationController {

    private final ValidationService validationService;

    @Autowired
    public ValidationController(ValidationService validationService) {
        this.validationService = validationService;
    }

    @PostMapping("/email")
    public ResponseEntity<EmailValidationDto> isEmailAlreadyTaken(@RequestBody EmailValidationDto emailValidationDto) throws Exception{
        return ResponseEntity.ok(validationService.validateEmail(emailValidationDto));
    }
}
