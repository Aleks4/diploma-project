package pl.kielce.tu.diabeticstools.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.kielce.tu.diabeticstools.model.Measurement;
import pl.kielce.tu.diabeticstools.model.Report;
import pl.kielce.tu.diabeticstools.model.type.ReportPeriod;
import pl.kielce.tu.diabeticstools.model.type.ReportType;

import java.io.Serializable;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.List;

@Data
@NoArgsConstructor
public class ReportDto implements Serializable {

    private Long id;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @JsonProperty("start_date")
    private LocalDate startDate;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @JsonProperty("end_date")
    private LocalDate endDate;

    @JsonFormat(pattern = "yyyy-MM-dd, HH:mm:ss")
    @JsonProperty("creation_date")
    private Timestamp creationDate;

    @JsonProperty("report_tables_by_schedule")
    private List<ReportTableData> reportTablesBySchedule;

    private ReportPeriod period;

    private ReportType type;

    public ReportDto(Report report) {
        this.id = report.getId();
        this.startDate = report.getStartDate().toLocalDateTime().toLocalDate();
        this.endDate = report.getEndDate().toLocalDateTime().toLocalDate();
        this.creationDate = report.getCreationDate();
        this.period = report.getPeriod();
        this.type = report.getType();
    }
}
