package pl.kielce.tu.diabeticstools.exception;

public class TimeWindowCollisionException extends Exception {

    public TimeWindowCollisionException(String message) {
        super(message);
    }
}
