package pl.kielce.tu.diabeticstools.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import pl.kielce.tu.diabeticstools.dto.PatientScheduleDto;
import pl.kielce.tu.diabeticstools.model.type.ScheduleType;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalTime;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class PatientSchedule implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private Patient patient;

    private LocalTime time;

    private String name;

    @Enumerated(EnumType.STRING)
    private ScheduleType type;

    private Integer timeWindow;

    private Boolean active;

    public PatientSchedule(PatientScheduleDto patientScheduleDto) {
        this.time = patientScheduleDto.getTime();
        this.name = patientScheduleDto.getName();
        this.type = patientScheduleDto.getType();
        this.timeWindow = patientScheduleDto.getTimeWindow();
        this.active = true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        PatientSchedule that = (PatientSchedule) o;

        return new EqualsBuilder()
                .append(id, that.id)
                .append(patient, that.patient)
                .append(time, that.time)
                .append(name, that.name)
                .append(type, that.type)
                .append(timeWindow, that.timeWindow)
                .append(active, that.active)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(patient)
                .append(time)
                .append(name)
                .append(type)
                .append(timeWindow)
                .append(active)
                .toHashCode();
    }
}