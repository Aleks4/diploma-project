package pl.kielce.tu.diabeticstools.exception;

public class UnprocessableEntityException extends Exception {

    public UnprocessableEntityException(String message) {
        super(message);
    }
}
