package pl.kielce.tu.diabeticstools.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.kielce.tu.diabeticstools.dto.DateScope;
import pl.kielce.tu.diabeticstools.dto.MeasurementDto;
import pl.kielce.tu.diabeticstools.dto.MeasurementListDto;
import pl.kielce.tu.diabeticstools.dto.MeasurementRequestDto;
import pl.kielce.tu.diabeticstools.model.Measurement;
import pl.kielce.tu.diabeticstools.security.SecurityUtils;
import pl.kielce.tu.diabeticstools.service.MeasurementHistoryService;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/api/measurement/history")
public class MeasurementHistoryController {

    private final MeasurementHistoryService measurementHistoryService;
    private final SecurityUtils utils;

    @Autowired
    public MeasurementHistoryController(MeasurementHistoryService measurementHistoryService,
                                        SecurityUtils utils) {
        this.measurementHistoryService = measurementHistoryService;
        this.utils = utils;
    }

    @PostMapping("/range/date")
    public ResponseEntity<MeasurementListDto> getMeasurementsInDateRange(@RequestBody MeasurementRequestDto requestDto) throws Exception {
        return ResponseEntity.ok(measurementHistoryService
                .getMeasurementsInRange(utils.getCurrentPatient(), requestDto)
        );
    }
    @PostMapping("/range/latest")
    public ResponseEntity<MeasurementListDto> getLatestMeasurements(@RequestBody MeasurementRequestDto requestDto) throws Exception{
        return ResponseEntity.ok(measurementHistoryService.getLatestMeasurements(utils.getCurrentPatient(), requestDto));
    }
}
