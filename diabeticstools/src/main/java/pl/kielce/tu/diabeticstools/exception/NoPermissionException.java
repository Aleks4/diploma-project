package pl.kielce.tu.diabeticstools.exception;

public class NoPermissionException extends Exception{

    public NoPermissionException(String message) {
        super(message);
    }
}
