package pl.kielce.tu.diabeticstools;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.kielce.tu.diabeticstools.config.ServerConfig;

@SpringBootApplication
@EnableScheduling
@EnableConfigurationProperties({ServerConfig.class})
public class DiabeticstoolsApplication {

	public static void main(String[] args) {
		SpringApplication.run(DiabeticstoolsApplication.class, args);
	}

}
