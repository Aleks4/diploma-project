package pl.kielce.tu.diabeticstools.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.kielce.tu.diabeticstools.config.ServerConfig;
import pl.kielce.tu.diabeticstools.dto.BriefPersonData;
import pl.kielce.tu.diabeticstools.dto.PatientSettingsDto;
import pl.kielce.tu.diabeticstools.dto.RegistrationDto;
import pl.kielce.tu.diabeticstools.dto.UserProfileDto;
import pl.kielce.tu.diabeticstools.exception.UnprocessableEntityException;
import pl.kielce.tu.diabeticstools.model.Patient;
import pl.kielce.tu.diabeticstools.model.Person;
import pl.kielce.tu.diabeticstools.model.Role;
import pl.kielce.tu.diabeticstools.model.embeddable.GlucoseValue;
import pl.kielce.tu.diabeticstools.model.units.GlucoseUnit;
import pl.kielce.tu.diabeticstools.repository.PatientRepository;
import pl.kielce.tu.diabeticstools.repository.PatientSettingsRepository;
import pl.kielce.tu.diabeticstools.repository.PersonRepository;
import pl.kielce.tu.diabeticstools.repository.TokenBlacklistRepository;
import pl.kielce.tu.diabeticstools.security.SecurityUtils;
import pl.kielce.tu.diabeticstools.security.jwt.JWTTokenProvider;
import pl.kielce.tu.diabeticstools.utils.ApplicationUtils;
import pl.kielce.tu.diabeticstools.utils.converter.UnitUtils;

import javax.persistence.EntityNotFoundException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import java.util.Optional;

@Service
public class PersonService {
    private final PersonRepository personRepository;

    private final PatientRepository patientRepository;

    private final PatientSettingsRepository patientSettingsRepository;

    private final SecurityUtils utils;

    private final JWTTokenProvider tokenProvider;

    private final ApplicationUtils appUtils;

    private final ServerConfig serverConfig;

    private final UnitUtils unitUtils;

    @Autowired
    public PersonService(PersonRepository personRepository,
                         PatientRepository patientRepository,
                         SecurityUtils utils,
                         JWTTokenProvider tokenProvider,
                         PatientSettingsRepository patientSettingsRepository,
                         ApplicationUtils appUtils,
                         ServerConfig serverConfig,
                         UnitUtils unitUtils) {
        this.personRepository = personRepository;
        this.patientRepository = patientRepository;
        this.utils = utils;
        this.tokenProvider = tokenProvider;
        this.patientSettingsRepository = patientSettingsRepository;
        this.appUtils = appUtils;
        this.serverConfig = serverConfig;
        this.unitUtils = unitUtils;
    }

    public UserProfileDto getUserProfile(Person person) {
        return new UserProfileDto(person);
    }

    @Transactional
    public void changeUserProfile(Person person, UserProfileDto userProfileDto) throws Exception{
        if(!person.getEmail().equals(userProfileDto.getEmail())) {
            Optional<String> token = utils.getCurrentUserJWT();
            token.ifPresent(tokenProvider::blacklistToken);
        }
        person.setEmail(userProfileDto.getEmail());
        person.setFirstname(userProfileDto.getFirstname());
        person.setLastname(userProfileDto.getLastname());
        person.setDefaultGlucoseUnit(userProfileDto.getDefaultGlucoseUnit());
        person.setPhoneNumber(userProfileDto.getPhoneNumber());
        person.setModificationDate(Timestamp.from(Instant.now()));
        personRepository.save(person);
    }

    public BriefPersonData getPersonBrief() throws Exception{
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Optional<Person> personOptional = personRepository.findByEmail(authentication.getName());
        if(personOptional.isEmpty()) {
            throw new EntityNotFoundException("Person not found.");
        }
        Person person = personOptional.get();
        BriefPersonData briefPersonData = new BriefPersonData();
        briefPersonData.setFirstname(person.getFirstname());
        briefPersonData.setLastname(person.getLastname());
        briefPersonData.setRole(person.getRole().toString());
        briefPersonData.setDefaultGlucoseUnit(person.getDefaultGlucoseUnit());
        briefPersonData.setCurrentDate(LocalDate.now());
        briefPersonData.setMinGlucose(unitUtils.convert(new GlucoseValue(BigDecimal.valueOf(serverConfig.getMin_glucose()), GlucoseUnit.mgPerDL), person.getDefaultGlucoseUnit()).getValue());
        briefPersonData.setMaxGlucose(unitUtils.convert(new GlucoseValue(BigDecimal.valueOf(serverConfig.getMax_glucose()), GlucoseUnit.mgPerDL), person.getDefaultGlucoseUnit()).getValue());
        if(person.getRole().equals(Role.PATIENT)) {
            Optional<Patient> patient = patientRepository.findById(person.getId());
            briefPersonData.setPatientSettings(patient
                    .map(Patient::getSettings)
                    .map(PatientSettingsDto::new)
                    .orElse(null)
            );
        }
        return briefPersonData;
    }

    @Transactional
    public void registerNewPatient(RegistrationDto registrationDto) throws Exception{
        if(!registrationDto.getPassword().equals(registrationDto.getConfirmPassword())) {
            throw new UnprocessableEntityException("Passwords do not match.");
        }
        Patient patient = new Patient(registrationDto);
        if(patientRepository.findByEmail(patient.getEmail()).isPresent()) {
            throw new UnprocessableEntityException("Email is already taken");
        }
        patient.setPassword(new BCryptPasswordEncoder(11).encode(registrationDto.getPassword()));
        patientSettingsRepository.save(patient.getSettings());
        patient.setLocked(false);
        patient.setActive(true);
        patient.setRole(Role.PATIENT);
        patient.setRegistrationDate(Timestamp.from(Instant.now()));
        patient.setNextReportDate(appUtils.currentTimePlus(patient.getSettings().getReportPeriod()));
        patientRepository.save(patient);
    }
}
