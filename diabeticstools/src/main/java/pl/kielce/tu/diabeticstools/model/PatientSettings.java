package pl.kielce.tu.diabeticstools.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.kielce.tu.diabeticstools.model.type.ReportPeriod;
import pl.kielce.tu.diabeticstools.model.type.ReportType;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.io.Serializable;
import java.sql.Timestamp;
import java.time.Instant;

@Entity
@Data
@NoArgsConstructor
public class PatientSettings implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Long id;

    private Boolean includeBloodPressure = false;

    @Enumerated(EnumType.STRING)
    private ReportType reportType = ReportType.MANUAL_REPORT;

    @Enumerated(EnumType.STRING)
    private ReportPeriod reportPeriod = ReportPeriod.MONTHLY;

    private Integer timeWindow = 5;

    @Email
    private String doctorEmail;

    private Timestamp creationDate = Timestamp.from(Instant.now());

    private Timestamp modificationDate = Timestamp.from(Instant.now());
}
