package pl.kielce.tu.diabeticstools.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.kielce.tu.diabeticstools.config.ServerConfig;
import pl.kielce.tu.diabeticstools.dto.*;
import pl.kielce.tu.diabeticstools.exception.ConversionException;
import pl.kielce.tu.diabeticstools.model.Measurement;
import pl.kielce.tu.diabeticstools.model.Patient;
import pl.kielce.tu.diabeticstools.model.PatientSchedule;
import pl.kielce.tu.diabeticstools.model.PatientSettings;
import pl.kielce.tu.diabeticstools.model.embeddable.GlucoseValue;
import pl.kielce.tu.diabeticstools.model.type.ReportType;
import pl.kielce.tu.diabeticstools.model.units.GlucoseUnit;
import pl.kielce.tu.diabeticstools.repository.MeasurementRepository;
import pl.kielce.tu.diabeticstools.repository.PatientRepository;
import pl.kielce.tu.diabeticstools.repository.PatientSettingsRepository;
import pl.kielce.tu.diabeticstools.utils.ApplicationUtils;
import pl.kielce.tu.diabeticstools.utils.converter.UnitUtils;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.stream.Collectors;

@Service
public class PatientService {

    private final PatientSettingsRepository patientSettingsRepository;

    private final MeasurementRepository measurementRepository;

    private final PatientScheduleService patientScheduleService;

    private final MeasurementHistoryService measurementHistoryService;

    private final PatientRepository patientRepository;

    private final ApplicationUtils appUtils;

    private final UnitUtils unitUtils;

    private final ServerConfig serverConfig;

    @Autowired
    public PatientService(PatientSettingsRepository patientSettingsRepository,
                          MeasurementRepository measurementRepository,
                          PatientScheduleService patientScheduleService,
                          MeasurementHistoryService measurementHistoryService,
                          PatientRepository patientRepository,
                          ApplicationUtils appUtils,
                          UnitUtils unitUtils,
                          ServerConfig serverConfig) {
        this.patientSettingsRepository = patientSettingsRepository;
        this.measurementRepository = measurementRepository;
        this.patientScheduleService = patientScheduleService;
        this.measurementHistoryService = measurementHistoryService;
        this.patientRepository = patientRepository;
        this.appUtils = appUtils;
        this.unitUtils = unitUtils;
        this.serverConfig = serverConfig;
    }

    @Transactional
    public void changePatientSettings(Patient patient, PatientSettingsDto newSettings) throws Exception {
        PatientSettings settingsToChange = patient.getSettings();
        if(newSettings
                .getReportType()
                .equals(ReportType.PERIODIC_REPORT)
                &&
                !settingsToChange
                        .getReportPeriod()
                        .equals(newSettings.getReportPeriod())) {
            patient.setNextReportDate(appUtils.currentTimePlus(newSettings.getReportPeriod()));
        }
        settingsToChange.setIncludeBloodPressure(newSettings.getIncludeBloodPressure());
        settingsToChange.setReportType(newSettings.getReportType());
        settingsToChange.setDoctorEmail(newSettings.getDoctorEmail());
        settingsToChange.setReportPeriod(newSettings.getReportPeriod());
        settingsToChange.setTimeWindow(newSettings.getTimeWindow());
        settingsToChange.setModificationDate(Timestamp.from(Instant.now()));
        patientRepository.save(patient);
        patientSettingsRepository.save(settingsToChange);
    }
    @SuppressWarnings("Duplicates")
    public PatientBriefData getPatientBriefData(Patient patient, MeasurementRequestDto requestDto) throws Exception{
        PatientBriefData patientBriefData = new PatientBriefData();
        patientBriefData.setActiveSchedules(patientScheduleService
                .getCurrentlyOpenedSchedulesForPatient(patient)
                .stream()
                .map(PatientScheduleDto::new)
                .collect(Collectors.toList())
        );
        PatientSchedule nextSchedule = patientScheduleService.getNextSchedule(patient);
        patientBriefData.setNextSchedule(nextSchedule != null?new PatientScheduleDto(nextSchedule):null);
        Page<Measurement> measurementPage = measurementRepository.findByPatientIdAndInsertionTimestampBetween(
                patient.getId(),
                Timestamp.valueOf(LocalDateTime.of(LocalDate.now(), LocalTime.MIDNIGHT)),
                Timestamp.valueOf(LocalDateTime.of(LocalDate.now().plus(1, ChronoUnit.DAYS), LocalTime.MIDNIGHT)),
                PageRequest.of(requestDto.getPagingInfo().getPageNumber(), requestDto.getPagingInfo().getPageSize())
        );
        MeasurementListDto listDto = measurementHistoryService.extractMeasurementList(patient, measurementPage, requestDto);
        patientBriefData.setMeasurementsToday(listDto);
        return patientBriefData;
    }

    public ServerVariablesDto getServerVariables(Patient patient) throws Exception{
        ServerVariablesDto serverVars = new ServerVariablesDto();
        serverVars.setMinGlucose(unitUtils.convert(new GlucoseValue(BigDecimal.valueOf(serverConfig.getMin_glucose()), GlucoseUnit.mgPerDL), patient.getDefaultGlucoseUnit()).getValue());
        serverVars.setMaxGlucose(unitUtils.convert(new GlucoseValue(BigDecimal.valueOf(serverConfig.getMax_glucose()), GlucoseUnit.mgPerDL), patient.getDefaultGlucoseUnit()).getValue());
        serverVars.setMinBloodPressureUpper(BigDecimal.valueOf(90));
        serverVars.setMaxBloodPressureUpper(BigDecimal.valueOf(140));
        serverVars.setMinBloodPressureLower(BigDecimal.valueOf(50));
        serverVars.setMaxBloodPressureLower(BigDecimal.valueOf(90));
        return serverVars;
    }
}
