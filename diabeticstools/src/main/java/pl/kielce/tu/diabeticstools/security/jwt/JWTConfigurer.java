package pl.kielce.tu.diabeticstools.security.jwt;

import org.springframework.security.config.annotation.SecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.DefaultSecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import pl.kielce.tu.diabeticstools.security.SecurityUtils;

public class JWTConfigurer extends SecurityConfigurerAdapter<DefaultSecurityFilterChain, HttpSecurity> {

    private JWTTokenProvider tokenProvider;

    private SecurityUtils utils;

    public JWTConfigurer(JWTTokenProvider tokenProvider, SecurityUtils utils) {
        this.tokenProvider = tokenProvider;
        this.utils = utils;
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        JWTTokenFilter customFilter = new JWTTokenFilter(tokenProvider, utils);
        http.addFilterBefore(customFilter, UsernamePasswordAuthenticationFilter.class);
    }
}
