package pl.kielce.tu.diabeticstools.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.kielce.tu.diabeticstools.model.PatientSettings;

@Repository
public interface PatientSettingsRepository extends JpaRepository<PatientSettings, Long> {

}
