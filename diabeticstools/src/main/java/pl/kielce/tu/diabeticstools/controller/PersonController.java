package pl.kielce.tu.diabeticstools.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.kielce.tu.diabeticstools.dto.BriefPersonData;
import pl.kielce.tu.diabeticstools.dto.UserProfileDto;
import pl.kielce.tu.diabeticstools.dto.validation.EmailValidationDto;
import pl.kielce.tu.diabeticstools.security.SecurityUtils;
import pl.kielce.tu.diabeticstools.service.PersonService;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/person")
public class PersonController {

    private final PersonService personService;

    private final SecurityUtils utils;

    @Autowired
    public PersonController(PersonService personService,
                            SecurityUtils utils) {
        this.personService = personService;
        this.utils = utils;
    }

    @GetMapping(value = "/profile")
    public ResponseEntity<?> getUserProfile() throws Exception{
        return ResponseEntity.ok(personService.getUserProfile(utils.getCurrentPerson()));
    }

    @PutMapping(value = "/profile/change")
    public ResponseEntity<?> changeUserProfile(@Valid @RequestBody UserProfileDto userProfileDto) throws Exception {
        personService.changeUserProfile(utils.getCurrentPerson(), userProfileDto);
        return ResponseEntity.ok().build();
    }

    @GetMapping(value = "/profile/brief")
    public ResponseEntity<BriefPersonData> getUserBriefData() throws Exception{
        return ResponseEntity.ok(personService.getPersonBrief());
    }
}
