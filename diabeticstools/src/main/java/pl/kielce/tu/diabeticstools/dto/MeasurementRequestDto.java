package pl.kielce.tu.diabeticstools.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MeasurementRequestDto {
    @JsonProperty("date_scope")
    private DateScope dateScope;

    @JsonProperty("paging_info")
    private PagingInfo pagingInfo;

    @JsonProperty("start_date")
    private LocalDate startDate;

    @JsonProperty("stop_date")
    private LocalDate stopDate;
}
