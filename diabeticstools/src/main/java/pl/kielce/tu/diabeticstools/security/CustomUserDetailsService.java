package pl.kielce.tu.diabeticstools.security;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import pl.kielce.tu.diabeticstools.config.ServerConfig;
import pl.kielce.tu.diabeticstools.model.Patient;
import pl.kielce.tu.diabeticstools.model.PatientSettings;
import pl.kielce.tu.diabeticstools.model.Person;
import pl.kielce.tu.diabeticstools.model.Role;
import pl.kielce.tu.diabeticstools.repository.PatientRepository;
import pl.kielce.tu.diabeticstools.repository.PatientSettingsRepository;
import pl.kielce.tu.diabeticstools.repository.PersonRepository;

import javax.annotation.PostConstruct;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Date;
import java.util.Optional;
import java.util.logging.Logger;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    private static Logger logger = Logger.getLogger(CustomUserDetailsService.class.getName());

    private final PersonRepository personRepository;

    private final ServerConfig serverConfig;

    @Autowired
    public CustomUserDetailsService(PersonRepository personRepository,
                                    ServerConfig serverConfig) {
        this.personRepository = personRepository;
        this.serverConfig = serverConfig;

    }

    @PostConstruct
    public void firstLaunchInitRoot() {
        if (personRepository.count() == 0) {
            if(serverConfig.getDefault_password() == null || serverConfig.getDefault_password().equals("")) {
                serverConfig.setDefault_password(RandomStringUtils.random(8, true, true));
            }

            logger.info("First launch detected\n" +
                    " - creating new user with credentials: \n" +
                    "Email: "+serverConfig.getDefault_login()+"\n" +
                    "Password: "+serverConfig.getDefault_password()+"\n" +
                    "Highly recommended to log in and change those!");
            Person person = new Person();
            person.setPassword(
                    new BCryptPasswordEncoder(11)
                    .encode(serverConfig.getDefault_password())
            );
            person.setEmail(serverConfig.getDefault_login());
            person.setLocked(false);
            person.setActive(true);
            person.setRegistrationDate(new Timestamp(new Date().getTime()));
            person.setRole(Role.ADMINISTRATOR);
            person.setFirstname("Firstname");
            person.setLastname("Lastname");
            personRepository.save(person);

        }
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        Optional<Person> person = personRepository.findByEmail(s);
        if (person.isEmpty()) {
            throw new UsernameNotFoundException(s);
        }
        return new PersonWrapper(person.get());
    }
}
