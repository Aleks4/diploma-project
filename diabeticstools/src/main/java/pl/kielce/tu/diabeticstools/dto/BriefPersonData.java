package pl.kielce.tu.diabeticstools.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import pl.kielce.tu.diabeticstools.model.PatientSettings;
import pl.kielce.tu.diabeticstools.model.units.GlucoseUnit;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
public class BriefPersonData {
    private String firstname;
    private String lastname;
    private String role;
    @JsonProperty("patient_settings")
    private PatientSettingsDto patientSettings;
    @JsonProperty("default_glucose_unit")
    private GlucoseUnit defaultGlucoseUnit;
    @JsonProperty("current_date")
    private LocalDate currentDate;
    @JsonProperty("min_glucose")
    private BigDecimal minGlucose;
    @JsonProperty("max_glucose")
    private BigDecimal maxGlucose;
}
