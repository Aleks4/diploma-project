package pl.kielce.tu.diabeticstools.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import pl.kielce.tu.diabeticstools.model.Patient;
import pl.kielce.tu.diabeticstools.model.Person;
import pl.kielce.tu.diabeticstools.model.Role;
import pl.kielce.tu.diabeticstools.repository.PatientRepository;
import pl.kielce.tu.diabeticstools.repository.PersonRepository;

import javax.naming.AuthenticationException;
import javax.servlet.http.HttpServletRequest;

import java.util.Optional;
import java.util.stream.Stream;

import static pl.kielce.tu.diabeticstools.security.jwt.JWTTokenFilter.AUTHORIZATION_HEADER;

@Service
public class SecurityUtils {

    private final PersonRepository personRepository;

    private final PatientRepository patientRepository;

    @Autowired
    public SecurityUtils(PersonRepository personRepository,
                         PatientRepository patientRepository) {
        this.personRepository = personRepository;
        this.patientRepository = patientRepository;
    }

    public String resolveToken(HttpServletRequest request){
        String bearerToken = request.getHeader(AUTHORIZATION_HEADER);
        if (StringUtils.hasText(bearerToken) && bearerToken.startsWith("Bearer ")) {
            return bearerToken.substring(7);
        }
        return null;
    }

    public Optional<String> getCurrentUserLogin() {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        return Optional.ofNullable(securityContext.getAuthentication())
                .map(authentication -> {
                    if (authentication.getPrincipal() instanceof UserDetails) {
                        UserDetails springSecurityUser = (UserDetails) authentication.getPrincipal();
                        return springSecurityUser.getUsername();
                    } else if (authentication.getPrincipal() instanceof String) {
                        return (String) authentication.getPrincipal();
                    }
                    return null;
                });
    }

    public Person getCurrentPerson() throws Exception{
        Optional<String> currentPersonEmail = getCurrentUserLogin();
        if(currentPersonEmail.isEmpty()) {
            throw new AuthenticationException("User is not logged in.");
        }

        Optional<Person> personOptional = personRepository.findByEmail(currentPersonEmail.get());

        return personOptional.orElseThrow( () -> new AuthenticationException("User is not logged in."));
    }

    public Patient getCurrentPatient() throws Exception{
        Optional<String> patientLogin = getCurrentUserLogin();
        if(patientLogin.isEmpty()) {
            throw new AuthenticationException("User is not logged in.");
        }
        Optional<Patient> patientOptional = patientRepository.findByEmail(patientLogin.get());

        return patientOptional.orElseThrow(() -> new AuthenticationException("User is not logged in."));
    }

    /**
     * Get the JWT of the current user.
     *
     * @return the JWT of the current user.
     */
    public Optional<String> getCurrentUserJWT() {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        return Optional.ofNullable(securityContext.getAuthentication())
                .filter(authentication -> authentication.getCredentials() instanceof String)
                .map(authentication -> (String) authentication.getCredentials());
    }
}
