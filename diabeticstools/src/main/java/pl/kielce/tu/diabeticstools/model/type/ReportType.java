package pl.kielce.tu.diabeticstools.model.type;

public enum ReportType {
    PERIODIC_REPORT,
    MANUAL_REPORT
}
