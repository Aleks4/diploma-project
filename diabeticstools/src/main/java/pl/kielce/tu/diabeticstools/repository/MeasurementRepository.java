package pl.kielce.tu.diabeticstools.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import pl.kielce.tu.diabeticstools.model.Measurement;
import pl.kielce.tu.diabeticstools.model.Patient;
import pl.kielce.tu.diabeticstools.model.PatientSchedule;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

@Repository
public interface MeasurementRepository extends PagingAndSortingRepository<Measurement, Long> {
    List<Measurement> findByPatientSchedule(PatientSchedule patientSchedule);

    List<Measurement> findByPatientId(Long id);

    Page<Measurement> findByPatientIdAndInsertionTimestampBetween(Long patientId, Timestamp before, Timestamp after, Pageable pageable);

    Page<Measurement> findByPatientIdAndInsertionTimestampAfterOrderByInsertionTimestampDesc(Long patientId, Timestamp after, Pageable page);

    Page<Measurement> findByPatientIdAndInsertionTimestampBetweenOrderByInsertionTimestampDesc(Long patientId, Timestamp start, Timestamp end, Pageable page);

    List<Measurement> findByPatientAndPatientSchedule(Patient patient, PatientSchedule patientSchedule);

    Optional<Measurement> findByIdAndPatientId(Long id, Long patientId);
}
