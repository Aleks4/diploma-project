package pl.kielce.tu.diabeticstools.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.kielce.tu.diabeticstools.model.Report;

@Repository
public interface PeriodicReportRepository extends JpaRepository<Report, Long> {
}
