package pl.kielce.tu.diabeticstools.utils;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import pl.kielce.tu.diabeticstools.model.type.ReportPeriod;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@Component
public class ApplicationUtils {

    private final Environment environment;

    private final Map<ReportPeriod, TimeValue> periodMapper = new HashMap<>();

    {
        this.periodMapper.put(ReportPeriod.WEEKLY, new TimeValue(1, ChronoUnit.WEEKS));
        this.periodMapper.put(ReportPeriod.MONTHLY, new TimeValue(1, ChronoUnit.MONTHS));
        this.periodMapper.put(ReportPeriod.QUARTERLY, new TimeValue(3, ChronoUnit.MONTHS));
        this.periodMapper.put(ReportPeriod.ANNUAL, new TimeValue(1, ChronoUnit.YEARS));
    }
    @Autowired
    public ApplicationUtils(Environment environment) {
        this.environment = environment;
        this.periodMapper.put(ReportPeriod.WEEKLY, new TimeValue(1, ChronoUnit.WEEKS));
        this.periodMapper.put(ReportPeriod.MONTHLY, new TimeValue(1, ChronoUnit.MONTHS));
        this.periodMapper.put(ReportPeriod.QUARTERLY, new TimeValue(3, ChronoUnit.MONTHS));
        this.periodMapper.put(ReportPeriod.ANNUAL, new TimeValue(1, ChronoUnit.YEARS));
    }

    public boolean isDevelopmentMode() {
        return Arrays
                .asList(environment.getActiveProfiles())
                .contains("dev");
    }

    public TimeValue mapPeriodToUnit(ReportPeriod period) {
        return periodMapper.get(period);
    }

    public Timestamp currentTimePlus(ReportPeriod period) {
        TimeValue timeValue = periodMapper.get(period);
        return Timestamp.valueOf(LocalDateTime.now().plus(timeValue.getValue(), timeValue.getUnit()));
    }

//    public Timestamp currentTimeMinus(ReportPeriod period) {
//        TimeValue timeValue = periodMapper.get(period);
//        return Timestamp.valueOf(LocalDateTime.now().minus(timeValue.getValue(), timeValue.getUnit()));
//    }
}
