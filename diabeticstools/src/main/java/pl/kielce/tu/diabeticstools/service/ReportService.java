package pl.kielce.tu.diabeticstools.service;

import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import pl.kielce.tu.diabeticstools.dto.*;
import pl.kielce.tu.diabeticstools.exception.ConversionException;
import pl.kielce.tu.diabeticstools.exception.NotFoundException;
import pl.kielce.tu.diabeticstools.exception.UnprocessableEntityException;
import pl.kielce.tu.diabeticstools.model.Measurement;
import pl.kielce.tu.diabeticstools.model.Patient;
import pl.kielce.tu.diabeticstools.model.PatientSchedule;
import pl.kielce.tu.diabeticstools.model.Report;
import pl.kielce.tu.diabeticstools.model.embeddable.GlucoseValue;
import pl.kielce.tu.diabeticstools.model.type.ReportType;
import pl.kielce.tu.diabeticstools.model.type.ScheduleType;
import pl.kielce.tu.diabeticstools.repository.MeasurementRepository;
import pl.kielce.tu.diabeticstools.repository.PatientRepository;
import pl.kielce.tu.diabeticstools.repository.PatientScheduleRepository;
import pl.kielce.tu.diabeticstools.repository.ReportRepository;
import pl.kielce.tu.diabeticstools.utils.ApplicationUtils;
import pl.kielce.tu.diabeticstools.utils.TimeValue;
import pl.kielce.tu.diabeticstools.utils.converter.UnitUtils;
import pl.kielce.tu.diabeticstools.dto.ReportTableRow;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Service
public class ReportService {
    private final Logger logger = Logger.getLogger(ReportService.class.getName());
    private final MeasurementRepository measurementRepository;
    private final PatientRepository patientRepository;
    private final PatientScheduleRepository patientScheduleRepository;
    private final ReportRepository reportRepository;
    private final ApplicationUtils applicationUtils;
    private final UnitUtils unitUtils;

    public ReportService(MeasurementRepository measurementRepository,
                         PatientRepository patientRepository,
                         PatientScheduleRepository patientScheduleRepository,
                         ReportRepository reportRepository,
                         ApplicationUtils applicationUtils,
                         UnitUtils unitUtils) {
        this.measurementRepository = measurementRepository;
        this.patientRepository = patientRepository;
        this.patientScheduleRepository = patientScheduleRepository;
        this.reportRepository = reportRepository;
        this.applicationUtils = applicationUtils;
        this.unitUtils = unitUtils;
    }

    public ReportDto getReportById(Patient patient, Long id) throws Exception{

        if(id == null || patient == null) {
            throw new UnprocessableEntityException("patient and report ID cannot be null");
        }
        Optional<Report> reportOptional = reportRepository.findById(id);
        if(reportOptional.isEmpty()) throw new NotFoundException();
        ReportDto reportDto = new ReportDto(reportOptional.get());

        reportDto.setReportTablesBySchedule(
                fetchReportTableDataFromMeasurements(reportOptional.get(), getActivePatientSchedules(patient))
        );
        return reportDto;
    }

    public List<ReportBriefDto> getReportBriefList(Patient patient) {
        return reportRepository.findByPatient(patient)
                .stream()
                .map(ReportBriefDto::new)
                .collect(Collectors.toList());
    }


    public void generateReports() {
        List<Patient> patientToServe = patientRepository.findBySettings_ReportType(ReportType.PERIODIC_REPORT)
                .stream()
                .filter(patient -> patient.getNextReportDate() != null)
                .filter(patient -> patient.getNextReportDate().toLocalDateTime().isBefore(LocalDateTime.now()))
                .collect(Collectors.toList());
        patientToServe.forEach(this::generatePeriodicReportForPatient);
    }

    public void generateManualReportForPatient(Patient patient, ReportRequestDto requestDto) {
        generateReport(
                patient,
                Timestamp.valueOf(LocalDateTime.of(requestDto.getStartDate(), LocalTime.MIDNIGHT)),
                Timestamp.valueOf(LocalDateTime.of(requestDto.getEndDate(), LocalTime.MAX)),
                ReportType.MANUAL_REPORT
        );
    }
    public void generatePeriodicReportForPatient(Patient patient) {

        TimeValue timeValue = applicationUtils.mapPeriodToUnit(patient.getSettings().getReportPeriod());
        generateReport(patient,
                Timestamp.valueOf(
                        LocalDateTime.of(LocalDate.now().minus(timeValue.getValue(), timeValue.getUnit()), LocalTime.MIDNIGHT)
                ),
                Timestamp.valueOf(
                        LocalDateTime.of(LocalDate.now().minus(1, ChronoUnit.DAYS), LocalTime.MAX)
                ),
                ReportType.PERIODIC_REPORT
        );

    }
    public void generateReport(Patient patient, Timestamp start, Timestamp end, ReportType type) {
        Report report = new Report();
        report.setPatient(patient);
        report.setStartDate(start);
        report.setEndDate(end);
        report.setType(type);
        List<Measurement> measurements = measurementRepository.findByPatientIdAndInsertionTimestampBetweenOrderByInsertionTimestampDesc(
                patient.getId(),
                report.getStartDate(),
                report.getEndDate(),
                Pageable.unpaged()
        )
                .stream()
                .filter(measurement -> measurement.getPatientSchedule()!=null)
                .collect(Collectors.toList());

        report.setMeasurements(measurements);
        report.setPeriod(patient.getSettings().getReportPeriod());

        reportRepository.save(report);
        measurements.forEach(measurement -> measurement.setEditable(false));
        measurementRepository.saveAll(measurements);
    }



    private List<ReportTableData> fetchReportTableDataFromMeasurements(Report report, List<PatientSchedule> activeSchedules) {
        List<ReportTableData> tableDataForSchedules = new ArrayList<>();

        activeSchedules.forEach(schedule -> {
                    tableDataForSchedules.add(new ReportTableData(
                                    new PatientScheduleDto(schedule),
                                    toTableData(report.getMeasurements()
                                            .stream()
                                            .filter(measurement -> measurement.getPatientSchedule()!=null)
                                            .filter(measurement -> measurement.getPatientSchedule().getId().equals(schedule.getId()))
                                                    .peek(measurement -> {
                                                        try {
                                                            measurement.setGlucose(
                                                                    (GlucoseValue)unitUtils.convert(
                                                                            measurement.getGlucose(),
                                                                            report.getPatient().getDefaultGlucoseUnit()
                                                                    )
                                                            );
                                                        } catch (ConversionException e) {
                                                            e.printStackTrace();
                                                        }
                                                    })
                                                    .collect(Collectors.toList()),
                                            report.getStartDate(), report.getEndDate())
                            )
                    );

                }
        );
        return tableDataForSchedules;
    }

    private List<ReportTableRow> toTableData(List<Measurement> measurements, Timestamp start, Timestamp end) {
        List<ReportTableRow> tableRows = new ArrayList<>();

        for(Timestamp dateIterator = Timestamp.valueOf(start.toLocalDateTime());
            dateIterator.before(end) || dateIterator.equals(end);
            dateIterator = Timestamp.valueOf(dateIterator.toLocalDateTime().plus(1, ChronoUnit.DAYS))) {
            Measurement measurement = getMeasurementForDate(measurements, dateIterator.toLocalDateTime().toLocalDate());
            if(measurement != null) {
                tableRows.add(toTableRow(measurement));
            }
            else {
                tableRows.add(new ReportTableRow(dateIterator, "-", "-/-"));
            }
            //tableRows.add(measurement!=null?toTableRow(measurement):new ReportTableRow(dateIterator, "-", "-/-"));
        }


        return tableRows;
    }

    private List<PatientSchedule> getActivePatientSchedules(Patient patient) {
        return patientScheduleRepository
                    .findByPatient(patient)
                    .stream()
                    .filter(PatientSchedule::getActive)
                    .filter(patientSchedule -> ScheduleType.MEASUREMENT.equals(patientSchedule.getType()))
                    .collect(Collectors.toList());
    }

    private ReportTableRow toTableRow(Measurement measurement) {
        return ReportTableRow.of(measurement.getInsertionTimestamp(), measurement.getGlucose(), measurement.getBloodPressure());
    }

    private Measurement getMeasurementForDate(List<Measurement> measurements, LocalDate date) {
        for (Measurement searchable: measurements
        ) {
//            if(searchable.getId() == 164 || searchable.getInsertionTimestamp().toLocalDateTime().toLocalDate().isEqual(LocalDate.of(2020, 1, 4))) {
//                logger.info("DEBUG MSG FOR 165");
//            }
            if(searchable.getInsertionTimestamp().toLocalDateTime().toLocalDate().isEqual(date)) return searchable;
        }
        return null;

    }
}
