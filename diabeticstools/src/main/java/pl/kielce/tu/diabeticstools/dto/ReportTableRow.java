package pl.kielce.tu.diabeticstools.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.kielce.tu.diabeticstools.model.embeddable.BloodPressureValue;
import pl.kielce.tu.diabeticstools.model.embeddable.GlucoseValue;

import java.sql.Timestamp;
import java.time.format.DateTimeFormatter;

@Data
@NoArgsConstructor
public class ReportTableRow {
    private String date;
    private String glucose;
    @JsonProperty("blood_pressure")
    private String bloodPressure;

    public ReportTableRow(Timestamp timestamp, GlucoseValue glucoseValue, BloodPressureValue bloodPressureValue) {
        this.date = timestamp.toLocalDateTime().toLocalDate().format(DateTimeFormatter.ofPattern("MM.dd"));
        this.glucose = glucoseValue.getValue().toString();
        this.bloodPressure = bloodPressureValue!=null? bloodPressureValue.getUpperValue()+"/"+bloodPressureValue.getLowerValue().toString():null;
    }

    public ReportTableRow(Timestamp timestamp, GlucoseValue glucoseValue) {
        this.date = timestamp.toLocalDateTime().toLocalDate().format(DateTimeFormatter.ofPattern("MM.dd"));
        this.glucose = glucoseValue.getValue().toString();
    }

    public ReportTableRow(Timestamp timestamp, String glucose, String bloodPressure) {
        this.date = timestamp.toLocalDateTime().toLocalDate().format(DateTimeFormatter.ofPattern("MM.dd"));
        this.glucose = glucose;
        this.bloodPressure = bloodPressure;
    }

    public static ReportTableRow of(Timestamp timestamp, GlucoseValue glucoseValue, BloodPressureValue bloodPressureValue) {
        return new ReportTableRow(timestamp, glucoseValue, bloodPressureValue);
    }

    public static ReportTableRow of(Timestamp timestamp, GlucoseValue glucoseValue) {
        return new ReportTableRow(timestamp, glucoseValue);
    }
}
