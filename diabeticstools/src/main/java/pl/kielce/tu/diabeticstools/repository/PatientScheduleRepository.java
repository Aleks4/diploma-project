package pl.kielce.tu.diabeticstools.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.kielce.tu.diabeticstools.model.Patient;
import pl.kielce.tu.diabeticstools.model.PatientSchedule;

import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface PatientScheduleRepository extends JpaRepository<PatientSchedule, Long> {
    List<PatientSchedule> findByPatient(Patient patient);

    List<PatientSchedule> findByPatientAndTimeAfter(Patient patient, LocalTime time);

    Optional<PatientSchedule> findByPatientAndId(Patient patient, Long id);
}
