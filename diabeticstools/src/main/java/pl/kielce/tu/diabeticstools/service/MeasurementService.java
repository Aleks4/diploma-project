package pl.kielce.tu.diabeticstools.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.kielce.tu.diabeticstools.dto.BulkMeasurementDto;
import pl.kielce.tu.diabeticstools.dto.MeasurementDto;
import pl.kielce.tu.diabeticstools.exception.MissingBloodPressureException;
import pl.kielce.tu.diabeticstools.exception.NotFoundException;
import pl.kielce.tu.diabeticstools.exception.UnprocessableEntityException;
import pl.kielce.tu.diabeticstools.model.Measurement;
import pl.kielce.tu.diabeticstools.model.Patient;
import pl.kielce.tu.diabeticstools.model.embeddable.BloodPressureValue;
import pl.kielce.tu.diabeticstools.model.embeddable.GlucoseValue;
import pl.kielce.tu.diabeticstools.repository.MeasurementRepository;
import pl.kielce.tu.diabeticstools.utils.converter.UnitUtils;

import javax.persistence.EntityNotFoundException;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MeasurementService {
    private final MeasurementRepository measurementRepository;

    private final PatientScheduleService patientScheduleService;

    private final UnitUtils unitUtils;

    @Autowired
    public MeasurementService(MeasurementRepository measurementRepository,
                              PatientScheduleService patientScheduleService,
                              UnitUtils unitUtils) {
        this.measurementRepository = measurementRepository;
        this.patientScheduleService = patientScheduleService;
        this.unitUtils = unitUtils;
    }

    @Transactional
    public void addNewMeasurement(Patient patient, Measurement measurement) throws Exception {
        if(patient == null || measurement == null) {
            throw new EntityNotFoundException("Invalid request");
        }
        if(patient.getSettings().getIncludeBloodPressure() && measurement.getBloodPressure() == null) {
            throw new MissingBloodPressureException();
        }
        measurement.setPatient(patient);
        measurement.setInsertionTimestamp(Timestamp.from(Instant.now()));
        measurement.setPatientSchedule(patientScheduleService.getMeasurementSchedule(measurement));
        measurementRepository.save(measurement);
    }

    @Transactional
    public void addBulkMeasurements(Patient patient, BulkMeasurementDto bulkMeasurementDto) throws Exception{
        if(bulkMeasurementDto == null || bulkMeasurementDto.getMeasurements() == null) {
            throw new UnprocessableEntityException("Cannot post empty body");
        }

        for (MeasurementDto item: bulkMeasurementDto.getMeasurements()) {
            if(
                item.getDate() == null || item.getTime() == null ||
                item.getGlucose_unit() == null || item.getGlucose_value() == null
            ) {
                throw new UnprocessableEntityException("Measurement body not complete");
            }
        }

        List<Measurement> measurementsToAdd = bulkMeasurementDto.getMeasurements()
                .stream()
                .peek(measurement ->
                        measurement.setInsertionTimestamp(
                                Timestamp.valueOf(
                                        LocalDateTime.of(measurement.getDate(), measurement.getTime())
                                )
                        )
                )
                .map(Measurement::new)
                .peek(measurement -> measurement.setPatient(patient))
                .sorted(Comparator.comparing(Measurement::getInsertionTimestamp))
                .collect(Collectors.toList());
        for (Measurement measurement:measurementsToAdd) {
            addMeasurementFromBulk(measurement);
        }
    }

    private void addMeasurementFromBulk(Measurement measurement) {
        if(measurement.getInsertionTimestamp() == null) {
            measurement.setInsertionTimestamp(Timestamp.from(Instant.now()));
        }
        measurement.setPatientSchedule(patientScheduleService.getMeasurementSchedule(measurement));
        measurementRepository.save(measurement);
    }

    @Transactional
    public MeasurementDto editMeasurement(Patient patient, MeasurementDto measurementDto) throws Exception{
        Optional<Measurement> measurementOptional = measurementRepository.findByIdAndPatientId(measurementDto.getId(), patient.getId());
        if(measurementOptional.isEmpty()) {
            throw new NotFoundException();
        }
        Measurement measurement = measurementOptional.get();
        if(!measurement.getEditable()) {
            throw new UnprocessableEntityException("Measurement is not editable!");
        }
        if(measurementDto.getBlood_pressure_upper() != null && measurementDto.getBlood_pressure_lower() != null && patient.getSettings().getIncludeBloodPressure()) {
            measurement.setBloodPressure(new BloodPressureValue(measurementDto.getBlood_pressure_upper(), measurementDto.getBlood_pressure_lower(), measurementDto.getBlood_pressure_unit()));
        }
        measurement.setGlucose(new GlucoseValue(measurementDto.getGlucose_value(), measurementDto.getGlucose_unit()));
        measurementRepository.save(measurement);
        return new MeasurementDto(measurement);
    }

    @Transactional
    public void deleteMeasurement(Patient patient, Long measurementId) throws Exception{

        Optional<Measurement> measurement = measurementRepository.findById(measurementId);
        if(measurement.isEmpty() || !measurement.get().getPatient().getId().equals(patient.getId())) {
            throw new EntityNotFoundException("Measurement to delete not found");
        }
        if(!measurement.get().getEditable()) {
            throw new UnprocessableEntityException("Measurement cannot be deleted, as it is report-related.");
        }
        measurementRepository.deleteById(measurementId);
    }

    public MeasurementDto getMeasurementById(Patient patient, Long id) throws Exception{
        Optional<Measurement> measurementOptional = measurementRepository.findByIdAndPatientId(id, patient.getId());
        if(measurementOptional.isPresent()) {
            measurementOptional.get().setGlucose((GlucoseValue) unitUtils.convert(measurementOptional.get().getGlucose(), patient.getDefaultGlucoseUnit()));
            return new MeasurementDto(measurementOptional.get());
        }
        else throw new NotFoundException();
    }
}
