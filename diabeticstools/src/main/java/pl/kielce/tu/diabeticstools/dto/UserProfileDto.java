package pl.kielce.tu.diabeticstools.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.kielce.tu.diabeticstools.model.Person;
import pl.kielce.tu.diabeticstools.model.units.GlucoseUnit;

import javax.validation.constraints.Email;
import java.sql.Timestamp;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserProfileDto {
    @Email
    private String email;

    private String firstname;

    private String lastname;

    private String phoneNumber;

    private Timestamp registrationDate;

    @JsonProperty("default_glucose_unit")
    private GlucoseUnit defaultGlucoseUnit;

    public UserProfileDto(Person person) {
        this.email = person.getEmail();
        this.firstname = person.getFirstname();
        this.lastname = person.getLastname();
        this.phoneNumber = person.getPhoneNumber();
        this.registrationDate = person.getRegistrationDate();
        this.defaultGlucoseUnit = person.getDefaultGlucoseUnit();
    }
}
