package pl.kielce.tu.diabeticstools.utils;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.temporal.ChronoUnit;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TimeValue {
    private Integer value;
    private ChronoUnit unit;
}
