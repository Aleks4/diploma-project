package pl.kielce.tu.diabeticstools.service;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.kielce.tu.diabeticstools.dto.PatientScheduleDto;
import pl.kielce.tu.diabeticstools.exception.NoPermissionException;
import pl.kielce.tu.diabeticstools.exception.NotFoundException;
import pl.kielce.tu.diabeticstools.exception.TimeWindowCollisionException;
import pl.kielce.tu.diabeticstools.exception.UnprocessableEntityException;
import pl.kielce.tu.diabeticstools.model.Measurement;
import pl.kielce.tu.diabeticstools.model.Patient;
import pl.kielce.tu.diabeticstools.model.PatientSchedule;
import pl.kielce.tu.diabeticstools.model.type.ScheduleType;
import pl.kielce.tu.diabeticstools.repository.MeasurementRepository;
import pl.kielce.tu.diabeticstools.repository.PatientScheduleRepository;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
public class PatientScheduleService {
    private final PatientScheduleRepository patientScheduleRepository;

    private final MeasurementRepository measurementRepository;

    private final Predicate<TimeWindowWrapper> timeWindowChecker = (
            timeWindowWrapper ->
                    timeWindowWrapper
                            .getCheckedTime()
                            .isAfter(timeWindowWrapper
                                    .getSchedule()
                                    .minus(timeWindowWrapper.getTimeWindow(), ChronoUnit.MINUTES)
                                    .minus(1, ChronoUnit.SECONDS)
                            )
                            && timeWindowWrapper
                            .getCheckedTime()
                            .isBefore(timeWindowWrapper
                                    .getSchedule()
                                    .plus(timeWindowWrapper.getTimeWindow(), ChronoUnit.MINUTES)
                                    .plus(1, ChronoUnit.SECONDS)
                            )
    );

    @Autowired
    public PatientScheduleService(PatientScheduleRepository patientScheduleRepository,
                                  MeasurementRepository measurementRepository) {

        this.patientScheduleRepository = patientScheduleRepository;
        this.measurementRepository = measurementRepository;
    }

    @Transactional
    public PatientScheduleDto editSchedule(Patient patient, PatientScheduleDto scheduleNewData) throws Exception{
        Optional<PatientSchedule> scheduleToEditOptional = patientScheduleRepository.findByPatientAndId(patient, scheduleNewData.getId());
        if(scheduleToEditOptional.isEmpty()) {
            throw new NotFoundException();
        }

        var collidingSchedules = getCollidingSchedules(patient, new PatientSchedule(scheduleNewData))
                .stream()
                .filter(patientSchedule -> !patientSchedule.getId().equals(scheduleNewData.getId()))
                .collect(Collectors.toList());
        if(collidingSchedules.size()>0) {
            throw new UnprocessableEntityException("Schedule is colliding with at least one another.");
        }

        PatientSchedule scheduleToEdit = scheduleToEditOptional.get();
        scheduleToEdit.setTimeWindow(scheduleNewData.getTimeWindow());
        scheduleToEdit.setName(scheduleNewData.getName());
        patientScheduleRepository.save(scheduleToEdit);
        return new PatientScheduleDto(scheduleToEdit);
    }

    private boolean isScheduleAlreadyTaken(Patient patient, PatientSchedule patientSchedule) {
        return measurementRepository.findByPatientAndPatientSchedule(patient, patientSchedule)
                .stream().anyMatch(measurement -> measurement
                        .getInsertionTimestamp()
                        .toLocalDateTime()
                        .toLocalDate()
                        .isEqual(LocalDate.now())
                );
    }

    public PatientSchedule getMeasurementSchedule(Measurement measurement) {
        var patientScheduleList = patientScheduleRepository.findByPatient(measurement.getPatient());

        List<PatientSchedule> scheduleForThisMeasurement = patientScheduleList
                .stream()
                .filter(patientSchedule -> patientSchedule.getType() == ScheduleType.MEASUREMENT)
                .filter(PatientSchedule::getActive)
                .filter(patientSchedule -> timeWindowChecker.test(new TimeWindowWrapper(measurement
                        .getInsertionTimestamp()
                        .toLocalDateTime().toLocalTime(), patientSchedule.getTime(), patientSchedule.getTimeWindow())))
                .collect(Collectors.toList());
        if (scheduleForThisMeasurement.size() == 0) {
            return null;
        }
        if(isScheduleAlreadyTaken(measurement.getPatient(), patientScheduleList.get(0))) {
            return null;
        }
        return patientScheduleList.get(0);
    }

    @Transactional
    public void addNewSchedule(Patient patient, PatientSchedule newSchedule) throws Exception {

        List<PatientSchedule> collidingSchedules = getCollidingSchedules(patient, newSchedule);
        if (newSchedule.getType().equals(ScheduleType.MEASUREMENT) && !collidingSchedules.isEmpty()) {
            throw new TimeWindowCollisionException("Collision with another schedule point");
        }
        newSchedule.setPatient(patient);
        patientScheduleRepository
                .save(newSchedule);
    }

    public List<PatientSchedule> getCollidingSchedules(Patient patient, PatientSchedule newSchedule) throws Exception {
        List<PatientSchedule> existingSchedules = patientScheduleRepository.findByPatient(patient);
        return existingSchedules
                .stream()
                .filter(patientSchedule -> patientSchedule.getType().equals(ScheduleType.MEASUREMENT))
                .filter(patientSchedule -> timeWindowChecker
                                .test(new TimeWindowWrapper(newSchedule
                                                .getTime()
                                                .plus(newSchedule.getTimeWindow(), ChronoUnit.MINUTES),
                                                patientSchedule.getTime(),
                                                patientSchedule.getTimeWindow()
                                        )
                                )
                                || timeWindowChecker.test(
                        new TimeWindowWrapper(
                                newSchedule
                                        .getTime()
                                        .minus(newSchedule.getTimeWindow(), ChronoUnit.MINUTES),
                                patientSchedule.getTime(),
                                patientSchedule.getTimeWindow()
                        )
                        )
                )
                .collect(Collectors.toList());
    }

    public PatientScheduleDto getScheduleById(Patient patient, Long id) throws Exception{
        return patientScheduleRepository
                .findByPatientAndId(patient, id)
                .map(PatientScheduleDto::new)
                .orElseThrow(NotFoundException::new);
    }

    public List<PatientScheduleDto> getScheduleList(Patient patient) {
        return patientScheduleRepository
                .findByPatient(patient)
                .stream()
                .filter(PatientSchedule::getActive)
                .map(PatientScheduleDto::new)
                .sorted(Comparator.comparing(PatientScheduleDto::getTime))
                .collect(Collectors.toList());
    }

    @Transactional
    public void deleteSchedule(Patient patient, Long patientScheduleId) throws Exception {
        Optional<PatientSchedule> schedule = patientScheduleRepository.findById(patientScheduleId);
        if (schedule.isEmpty() || !schedule.get().getActive()) {
            throw new EntityNotFoundException("Schedule with id " + patientScheduleId + " does not exist.");
        }
        if (!schedule
                .get()
                .getPatient()
                .getId()
                .equals(patient.getId())) {
            throw new NoPermissionException("That schedule does not belong to this patient!");
        }
        patientScheduleRepository.delete(schedule.get());
    }

    public List<PatientSchedule> getCurrentlyOpenedSchedulesForPatient(Patient patient) throws Exception {
        PatientSchedule probeSchedule = new PatientSchedule();
        probeSchedule.setTime(LocalTime.now());
        probeSchedule.setTimeWindow(0);
        return getCollidingSchedules(patient, probeSchedule);
    }

    public PatientSchedule getNextSchedule(Patient patient) throws Exception {
        List<PatientSchedule> activeSchedules = getCurrentlyOpenedSchedulesForPatient(patient);
        List<PatientSchedule> schedules = patientScheduleRepository.findByPatientAndTimeAfter(patient, LocalTime.now())
                .stream()
                .filter(patientSchedule -> !activeSchedules.contains(patientSchedule))
                .filter(patientSchedule -> patientSchedule.getType().equals(ScheduleType.MEASUREMENT))
                .sorted(Comparator.comparing(PatientSchedule::getTime))
                .collect(Collectors.toList());

        if(schedules.isEmpty()) return null;
        else return  schedules.get(0);
    }
}

@Data
@NoArgsConstructor
@AllArgsConstructor
class TimeWindowWrapper {
    private LocalTime checkedTime;

    private LocalTime schedule;

    private Integer timeWindow;
}
