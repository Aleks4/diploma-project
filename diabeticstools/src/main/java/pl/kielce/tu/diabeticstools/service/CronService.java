package pl.kielce.tu.diabeticstools.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import pl.kielce.tu.diabeticstools.security.jwt.JWTTokenProvider;

@Service
public class CronService {

    private final ReportService reportService;

    private final JWTTokenProvider tokenProvider;

    @Autowired
    public CronService(ReportService reportService, JWTTokenProvider tokenProvider) {
        this.reportService = reportService;
        this.tokenProvider = tokenProvider;
    }

    @Scheduled(cron = "0 0 6,18 * * *")
    public void execute() {
        reportService.generateReports();
        tokenProvider.cleanupTokens();
    }

}
