package pl.kielce.tu.diabeticstools.model;

public enum Role {
    ADMINISTRATOR,
    PATIENT,
    ANONYMOUS
}
