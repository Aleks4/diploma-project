package pl.kielce.tu.diabeticstools.dto.validation;

import lombok.Data;

import javax.validation.constraints.Email;

@Data
public class EmailValidationDto {

    @Email
    private String email;

    private Boolean isCorrect;

    private Boolean isFree;
}
