package pl.kielce.tu.diabeticstools.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.kielce.tu.diabeticstools.dto.PatientScheduleDto;
import pl.kielce.tu.diabeticstools.model.PatientSchedule;
import pl.kielce.tu.diabeticstools.security.SecurityUtils;
import pl.kielce.tu.diabeticstools.service.PatientScheduleService;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/patient/schedule")
public class PatientScheduleController {
    private final SecurityUtils utils;

    private final PatientScheduleService patientScheduleService;

    @Autowired
    public PatientScheduleController(SecurityUtils utils,
                                     PatientScheduleService patientScheduleService) {
        this.utils = utils;
        this.patientScheduleService = patientScheduleService;
    }
    @PostMapping(value = "/new")
    public ResponseEntity<?> newSchedule(@Valid @RequestBody PatientScheduleDto schedule) throws Exception {
        patientScheduleService.addNewSchedule(utils.getCurrentPatient(), new PatientSchedule(schedule));
        return ResponseEntity.ok().build();
    }

    @PostMapping(value="/colliding")
    public ResponseEntity<List<PatientScheduleDto>> retrieveCollidingSchedules(@Valid @RequestBody PatientScheduleDto scheduleDto) throws Exception{
        return ResponseEntity.ok(patientScheduleService.getCollidingSchedules(utils.getCurrentPatient(), new PatientSchedule(scheduleDto)).stream()
                .filter(patientSchedule -> !patientSchedule.getId().equals(scheduleDto.getId()))
                .map(PatientScheduleDto::new)
                .collect(Collectors.toList()));
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getScheduleById(@PathVariable Long id) throws Exception{
        return ResponseEntity.ok(patientScheduleService.getScheduleById(utils.getCurrentPatient(), id));
    }

    @PutMapping("/edit")
    public ResponseEntity<PatientScheduleDto> editSchedule(@Valid @RequestBody PatientScheduleDto scheduleDto) throws Exception{
        return ResponseEntity.ok(patientScheduleService.editSchedule(utils.getCurrentPatient(), scheduleDto));
    }

    @GetMapping(value = "/list")
    public ResponseEntity<List<PatientScheduleDto>> getPatientScheduleList() throws Exception{
        return ResponseEntity.ok(patientScheduleService.getScheduleList(utils.getCurrentPatient()));
    }

    @DeleteMapping(value = "/delete/{scheduleId}")
    public ResponseEntity<?> deleteSchedule(@PathVariable Long scheduleId) throws Exception {
        patientScheduleService.deleteSchedule(utils.getCurrentPatient(), scheduleId);
        return ResponseEntity.ok().build();
    }
}
