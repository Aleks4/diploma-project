package pl.kielce.tu.diabeticstools.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BulkMeasurementDto implements Serializable {
    private boolean abandonOnError = false;

    @NotNull
    private List<MeasurementDto> measurements;
}
