package pl.kielce.tu.diabeticstools.model.embeddable;

import lombok.*;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import pl.kielce.tu.diabeticstools.exception.ConversionException;
import pl.kielce.tu.diabeticstools.model.units.GlucoseUnit;
import pl.kielce.tu.diabeticstools.utils.converter.Unit;
import pl.kielce.tu.diabeticstools.utils.converter.UnitValue;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.io.Serializable;
import java.math.BigDecimal;

@Embeddable
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class GlucoseValue extends UnitValue implements Serializable {
    @Column(
            precision = 7,
            scale = 3,
            nullable = false
    )
    private BigDecimal value;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private GlucoseUnit unit;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        GlucoseValue that = (GlucoseValue) o;

        return new EqualsBuilder()
                .append(value, that.value)
                .append(unit, that.unit)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(value)
                .append(unit)
                .toHashCode();
    }

    @Override
    public void setUnit(Unit argUnit) throws IllegalArgumentException{
        this.unit = (GlucoseUnit)argUnit;
    }

}
