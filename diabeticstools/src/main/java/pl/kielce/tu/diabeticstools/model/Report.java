package pl.kielce.tu.diabeticstools.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import pl.kielce.tu.diabeticstools.model.type.ReportPeriod;
import pl.kielce.tu.diabeticstools.model.type.ReportType;
import pl.kielce.tu.diabeticstools.model.type.ScheduleType;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
public class Report implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;

    @ManyToMany
    private List<Measurement> measurements;

    private Timestamp creationDate = Timestamp.from(Instant.now());

    private Timestamp startDate;

    private Timestamp endDate;

    @ManyToOne
    private Patient patient;

    @Lob
    @Column(length = 2048)
    private String content;

    private ReportPeriod period;

    private ReportType type;
}
