package pl.kielce.tu.diabeticstools.service;

import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.kielce.tu.diabeticstools.config.ServerConfig;
import pl.kielce.tu.diabeticstools.dto.ServerVariablesDto;
import pl.kielce.tu.diabeticstools.dto.validation.EmailValidationDto;
import pl.kielce.tu.diabeticstools.exception.UnprocessableEntityException;
import pl.kielce.tu.diabeticstools.model.Patient;
import pl.kielce.tu.diabeticstools.model.embeddable.BloodPressureValue;
import pl.kielce.tu.diabeticstools.model.embeddable.GlucoseValue;
import pl.kielce.tu.diabeticstools.model.units.GlucoseUnit;
import pl.kielce.tu.diabeticstools.repository.PersonRepository;
import pl.kielce.tu.diabeticstools.security.SecurityUtils;
import pl.kielce.tu.diabeticstools.utils.ApplicationUtils;
import pl.kielce.tu.diabeticstools.utils.converter.UnitUtils;

import java.math.BigDecimal;
import java.util.Optional;

@Service
public class ValidationService {
    private final PersonRepository personRepository;

    private final ApplicationUtils appUtils;

    private final EmailValidator emailValidator;

    private final SecurityUtils utils;

    @Autowired
    public ValidationService(PersonRepository personRepository,
                             ApplicationUtils appUtils,
                             SecurityUtils utils) {
        this.personRepository = personRepository;
        this.appUtils = appUtils;
        this.emailValidator = EmailValidator.getInstance(appUtils.isDevelopmentMode());
        this.utils = utils;
    }

    public EmailValidationDto validateEmail(EmailValidationDto emailValidationDto) throws Exception{
        if(emailValidationDto.getEmail() == null || emailValidationDto.getEmail().isEmpty()) {
            throw new UnprocessableEntityException("email to validate cannot be null or empty string");
        }
        Optional<String> currentUserEmail = utils.getCurrentUserLogin();
        emailValidationDto.setIsCorrect(emailValidator.isValid(emailValidationDto.getEmail()));
        emailValidationDto.setIsFree(
                (currentUserEmail.isPresent() && currentUserEmail.get().equals(emailValidationDto.getEmail())) ||
                (emailValidationDto.getIsCorrect() && personRepository.findByEmail(emailValidationDto.getEmail()).isEmpty())
        );
        return emailValidationDto;
    }
}
