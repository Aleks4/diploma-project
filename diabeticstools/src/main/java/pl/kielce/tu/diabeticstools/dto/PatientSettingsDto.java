package pl.kielce.tu.diabeticstools.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import pl.kielce.tu.diabeticstools.model.PatientSettings;
import pl.kielce.tu.diabeticstools.model.type.ReportPeriod;
import pl.kielce.tu.diabeticstools.model.type.ReportType;

import javax.validation.constraints.Email;
import java.io.Serializable;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalTime;

@Data
@NoArgsConstructor
public class PatientSettingsDto implements Serializable {

    private Long id;

    private Boolean includeBloodPressure = false;

    private ReportType reportType;

    private ReportPeriod reportPeriod;

    private Integer timeWindow = 5;

    @Email
    private String doctorEmail;

    private Timestamp creationTimestamp;

    private Timestamp modificationTimestamp;

    private LocalDate modificationDate;

    private LocalTime modificationTime;

    public PatientSettingsDto(PatientSettings settings) {
        this.id = settings.getId();
        this.includeBloodPressure = settings.getIncludeBloodPressure();
        this.reportType = settings.getReportType();
        this.reportPeriod = settings.getReportPeriod();
        this.timeWindow = settings.getTimeWindow();
        this.doctorEmail = settings.getDoctorEmail();
        this.creationTimestamp = settings.getCreationDate();
        this.modificationTimestamp = settings.getModificationDate();
        this.modificationDate = settings.getModificationDate().toLocalDateTime().toLocalDate();
        this.modificationTime = settings.getModificationDate().toLocalDateTime().toLocalTime();

    }
}
