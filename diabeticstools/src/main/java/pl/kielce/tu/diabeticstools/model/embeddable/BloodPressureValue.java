package pl.kielce.tu.diabeticstools.model.embeddable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Transient;
import java.math.BigDecimal;

@Embeddable
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BloodPressureValue {

    @Column(precision = 5, scale = 2)
    private BigDecimal upperValue;

    @Column(precision = 5, scale = 2)
    private BigDecimal lowerValue;

    @Transient
    private String unit = "mmHg";
}
