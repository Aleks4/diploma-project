package pl.kielce.tu.diabeticstools.exception.handler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import pl.kielce.tu.diabeticstools.exception.*;

import javax.persistence.EntityNotFoundException;

@ControllerAdvice
public class RestApplicationExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {NotFoundException.class, EntityNotFoundException.class})
    public ResponseEntity<?> handleNotFound(Exception e, WebRequest request) {
        String bodyOfResponse = e.getMessage();
        return handleExceptionInternal(e, bodyOfResponse, null, HttpStatus.NOT_FOUND, request);
    }
    @ExceptionHandler(value = ConversionException.class)
    public ResponseEntity<?> handleInternal(Exception e, WebRequest request) {
        return handleExceptionInternal(e, e.getMessage(), null, HttpStatus.INTERNAL_SERVER_ERROR, request);
    }
    @ExceptionHandler(value = TimeWindowCollisionException.class)
    public ResponseEntity<?> handleTimeWindowCollision(Exception e, WebRequest request) {
        return handleExceptionInternal(e, e.getMessage(), null, HttpStatus.UNPROCESSABLE_ENTITY, request);
    }
    @ExceptionHandler(value = MissingBloodPressureException.class)
    public ResponseEntity<?> handleMissingBloodPressure(Exception e, WebRequest request) {
        return handleExceptionInternal(e, e.getMessage(), null, HttpStatus.UNPROCESSABLE_ENTITY, request);
    }
    @ExceptionHandler(value = UnprocessableEntityException.class)
    public ResponseEntity<?> handleUnprocessableEntity(Exception e, WebRequest request) {
        return handleExceptionInternal(e, e.getMessage(), null, HttpStatus.UNPROCESSABLE_ENTITY, request);
    }
}