package pl.kielce.tu.diabeticstools.model.type;

public enum ReportPeriod {
    WEEKLY,
    MONTHLY,
    QUARTERLY,
    ANNUAL
}
