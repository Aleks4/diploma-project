package pl.kielce.tu.diabeticstools.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.kielce.tu.diabeticstools.dto.CredentialsDto;
import pl.kielce.tu.diabeticstools.dto.JWTToken;
import pl.kielce.tu.diabeticstools.dto.RegistrationDto;
import pl.kielce.tu.diabeticstools.security.SecurityUtils;
import pl.kielce.tu.diabeticstools.security.jwt.JWTTokenFilter;
import pl.kielce.tu.diabeticstools.security.jwt.JWTTokenProvider;
import pl.kielce.tu.diabeticstools.service.PersonService;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping("/api")
public class AuthController {

    private final JWTTokenProvider tokenProvider;

    private final AuthenticationManager authenticationManager;

    private final SecurityUtils utils;

    private final PersonService personService;

    @Autowired
    public AuthController(JWTTokenProvider tokenProvider,
                          AuthenticationManager authenticationManager,
                          SecurityUtils utils,
                          PersonService personService) {
        this.tokenProvider = tokenProvider;
        this.authenticationManager = authenticationManager;
        this.utils = utils;
        this.personService = personService;
    }

    @PostMapping("/authenticate")
    public ResponseEntity<JWTToken> authorize(@Valid @RequestBody CredentialsDto credentials) {

        UsernamePasswordAuthenticationToken authenticationToken =
                new UsernamePasswordAuthenticationToken(credentials.getEmail(), credentials.getPassword());

        Authentication authentication = authenticationManager.authenticate(authenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        boolean rememberMe = (credentials.getRememberMe() == null) ? false : credentials.getRememberMe();
        String jwt = tokenProvider.createToken(authentication, rememberMe);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(JWTTokenFilter.AUTHORIZATION_HEADER, "Bearer " + jwt);
        return new ResponseEntity<>(new JWTToken(jwt), httpHeaders, HttpStatus.OK);
    }

    @PostMapping("/logout")
    public ResponseEntity<?> logout(HttpServletRequest request) {
        String tokenToBlacklist = utils.resolveToken(request);
        tokenProvider.blacklistToken(tokenToBlacklist);

        return ResponseEntity.ok().build();
    }

    @PostMapping("/register")
    public ResponseEntity<?> registerNewPatient(@Valid @RequestBody RegistrationDto registrationDto) throws Exception{
        personService.registerNewPatient(registrationDto);
        return ResponseEntity.ok().build();
    }
    //TODO: Cleanup expired tokens from blacklist periodically
}
