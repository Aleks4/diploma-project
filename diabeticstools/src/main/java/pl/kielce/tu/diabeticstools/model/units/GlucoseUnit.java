package pl.kielce.tu.diabeticstools.model.units;

import pl.kielce.tu.diabeticstools.utils.converter.Unit;

public enum GlucoseUnit implements Unit {
    mgPerDL {
        @Override
        public String toString() {
            return "mg/dL";
        }
    },
    mmolPerL {
        @Override
        public String toString() {
            return "mmol/L";
        }
    }
}
