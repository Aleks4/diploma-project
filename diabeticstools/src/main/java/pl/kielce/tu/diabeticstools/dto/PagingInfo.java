package pl.kielce.tu.diabeticstools.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PagingInfo {
    @JsonProperty("total_item_count")
    private Integer totalItemCount;

    @JsonProperty("total_page_count")
    private Integer totalPageCount;

    @JsonProperty("page_number")
    private Integer pageNumber;

    @JsonProperty("page_size")
    private Integer pageSize;
}
