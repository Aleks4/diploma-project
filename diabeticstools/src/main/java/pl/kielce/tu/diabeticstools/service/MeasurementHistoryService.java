package pl.kielce.tu.diabeticstools.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import pl.kielce.tu.diabeticstools.dto.*;
import pl.kielce.tu.diabeticstools.exception.ConversionException;
import pl.kielce.tu.diabeticstools.model.Measurement;
import pl.kielce.tu.diabeticstools.model.Patient;
import pl.kielce.tu.diabeticstools.model.embeddable.GlucoseValue;
import pl.kielce.tu.diabeticstools.repository.MeasurementRepository;
import pl.kielce.tu.diabeticstools.utils.converter.UnitUtils;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class MeasurementHistoryService {
    private final MeasurementRepository measurementRepository;

    private final HashMap<DateScope, ChronoUnit> unitMapper = new HashMap<>();

    private final UnitUtils unitUtils;

    @Autowired
    public MeasurementHistoryService(MeasurementRepository measurementRepository,
                                     UnitUtils unitUtils) {
        this.measurementRepository = measurementRepository;
        this.unitUtils = unitUtils;
        this.unitMapper.put(DateScope.DAY, ChronoUnit.DAYS);
        this.unitMapper.put(DateScope.WEEK, ChronoUnit.WEEKS);
        this.unitMapper.put(DateScope.MONTH, ChronoUnit.MONTHS);
        this.unitMapper.put(DateScope.YEAR, ChronoUnit.YEARS);
    }

    public MeasurementListDto getMeasurementsInRange(Patient patient, MeasurementRequestDto requestDto) {
        Page<Measurement> measurements =
                measurementRepository
                .findByPatientIdAndInsertionTimestampBetweenOrderByInsertionTimestampDesc(patient.getId(),
                        Timestamp.valueOf(requestDto.getStartDate().atStartOfDay()),
                        Timestamp.valueOf(requestDto.getStopDate().atTime(23,59,59)),
                        PageRequest.of(requestDto
                                        .getPagingInfo()
                                        .getPageNumber(),
                                requestDto
                                        .getPagingInfo()
                                        .getPageSize())
                );
        return extractMeasurementList(patient, measurements, requestDto);
    }

    public MeasurementListDto getLatestMeasurements(Patient patient, MeasurementRequestDto requestDto) throws Exception{
        Page<Measurement> measurements =
                measurementRepository
                        .findByPatientIdAndInsertionTimestampAfterOrderByInsertionTimestampDesc(
                                patient.getId(),
                                Timestamp.valueOf(
                                        LocalDateTime.of(LocalDate.now(), LocalTime.MIDNIGHT)
                                        .minus(1, unitMapper.get(requestDto.getDateScope()))
                                ),

                                PageRequest.of(requestDto
                                                .getPagingInfo()
                                                .getPageNumber(),
                                        requestDto
                                                .getPagingInfo()
                                                .getPageSize())
                        );
        return extractMeasurementList(patient, measurements, requestDto);
    }

    public MeasurementListDto extractMeasurementList(Patient patient, Page<Measurement> measurements, MeasurementRequestDto requestDto) {
        MeasurementListDto measurementListDto = new MeasurementListDto();
        measurementListDto
                .setMeasurementList(measurements.stream()
                .peek(measurement -> {
                    try {
                        measurement.setGlucose((GlucoseValue) unitUtils.convert(measurement.getGlucose(), patient.getDefaultGlucoseUnit()));
                    } catch (ConversionException e) {
                        e.printStackTrace();
                    }
                })
                .map(MeasurementDto::new)
                .collect(Collectors.toList()));
        measurementListDto.setPagingInfo(requestDto.getPagingInfo());
        measurementListDto
                .getPagingInfo()
                .setTotalItemCount(Long.valueOf(measurements.getTotalElements()).intValue());
        measurementListDto
                .getPagingInfo()
                .setTotalPageCount(measurements.getTotalPages());
        measurementListDto
                .getPagingInfo()
                .setPageNumber(measurements.getNumber());
        return measurementListDto;
    }
}
