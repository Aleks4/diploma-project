package pl.kielce.tu.diabeticstools.exception;

public class ConversionException extends Exception {

    public ConversionException(String message) {
        super(message);
    }
}
