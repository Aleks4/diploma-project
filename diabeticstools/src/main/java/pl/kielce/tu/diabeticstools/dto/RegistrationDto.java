package pl.kielce.tu.diabeticstools.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import pl.kielce.tu.diabeticstools.model.PatientSettings;
import pl.kielce.tu.diabeticstools.model.units.GlucoseUnit;

import javax.persistence.Column;
import javax.validation.constraints.Email;
import java.io.Serializable;

@Data
public class RegistrationDto implements Serializable {
    @Column(unique = true, nullable = false)
    @Email
    private String email;

    @Column(nullable = false)
    private String password;

    @Column(nullable = false)
    @JsonProperty("confirm_password")
    private String confirmPassword;

    private String firstname;

    private String lastname;

    @Column(length = 9)
    @JsonProperty("phone_number")
    private String phoneNumber;

    @JsonProperty("default_glucose_unit")
    private GlucoseUnit defaultGlucoseUnit = GlucoseUnit.mgPerDL;

    @JsonProperty("patient_settings")
    private PatientSettings patientSettings;
}
