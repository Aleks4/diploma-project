package pl.kielce.tu.diabeticstools.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.kielce.tu.diabeticstools.model.PatientSchedule;
import pl.kielce.tu.diabeticstools.model.type.ScheduleType;

import java.io.Serializable;
import java.time.LocalTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PatientScheduleDto implements Serializable {

    @JsonIgnore
    private PatientSchedule patientSchedule;

    private Long id;

    @JsonFormat(pattern = "HH:mm")
    private LocalTime time;

    private String name;

    private ScheduleType type;

    @JsonProperty("time_window")
    private Integer timeWindow;

    public PatientScheduleDto(PatientSchedule schedule) {
        this.id = schedule.getId();
        this.time = schedule.getTime();
        this.name = schedule.getName();
        this.type = schedule.getType();
        this.timeWindow = schedule.getTimeWindow();
        this.patientSchedule = schedule;
    }
}
