package pl.kielce.tu.diabeticstools.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import pl.kielce.tu.diabeticstools.model.embeddable.BloodPressureValue;
import pl.kielce.tu.diabeticstools.model.embeddable.GlucoseValue;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class ServerVariablesDto implements Serializable {
    @JsonProperty("min_glucose")
    private BigDecimal minGlucose;

    @JsonProperty("max_glucose")
    private BigDecimal maxGlucose;

    @JsonProperty("min_blood_pressure_upper")
    private BigDecimal minBloodPressureUpper;

    @JsonProperty("max_blood_pressure_upper")
    private BigDecimal maxBloodPressureUpper;

    @JsonProperty("min_blood_pressure_lower")
    private BigDecimal minBloodPressureLower;

    @JsonProperty("max_blood_pressure_lower")
    private BigDecimal maxBloodPressureLower;

}
