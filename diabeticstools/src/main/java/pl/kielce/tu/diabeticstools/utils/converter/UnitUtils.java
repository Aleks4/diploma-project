package pl.kielce.tu.diabeticstools.utils.converter;

import org.apache.commons.collections4.map.MultiKeyMap;
import org.springframework.stereotype.Component;
import pl.kielce.tu.diabeticstools.exception.ConversionException;
import pl.kielce.tu.diabeticstools.model.units.GlucoseUnit;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.function.Function;

@Component
public class UnitUtils {
    private final MultiKeyMap<Unit, Function<BigDecimal, BigDecimal>> unitMap = new MultiKeyMap<>();

    public UnitUtils() {
        unitMap.put(GlucoseUnit.mgPerDL, GlucoseUnit.mmolPerL, (arg) -> arg.divide(BigDecimal.valueOf(18), RoundingMode.HALF_UP));
        unitMap.put(GlucoseUnit.mmolPerL, GlucoseUnit.mgPerDL, (arg) -> arg.multiply(BigDecimal.valueOf(18)));
        unitMap.put(GlucoseUnit.mmolPerL, GlucoseUnit.mmolPerL, arg -> arg);
        unitMap.put(GlucoseUnit.mgPerDL, GlucoseUnit.mgPerDL, arg -> arg);
    }

    public UnitValue convert(UnitValue value, Unit targetUnit) throws ConversionException {

        try {
            return value
                    .getClass()
                    .getConstructor()
                    .newInstance()
                    .initialValue(unitMap.get(value.getUnit(), targetUnit).apply(value.getValue()))
                    .initialUnit(targetUnit);
        } catch (Exception e) {
            throw new ConversionException("Failed to convert " +
                    value.getUnit().toString() +
                    " to " +
                    targetUnit.toString() +
                    ". Internal error:" +
                    e.getClass().getName() +
                    "[" + e.getMessage() + "]");
        }
    }
}
