package pl.kielce.tu.diabeticstools.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.kielce.tu.diabeticstools.dto.UserProfileDto;
import pl.kielce.tu.diabeticstools.model.units.GlucoseUnit;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.io.Serializable;
import java.sql.Timestamp;
import java.time.Instant;

@NoArgsConstructor
@Data
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class Person implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @Column(unique = true, nullable = false)
    @Email
    protected String email;

    @Column(nullable = false)
    protected String password;

    protected String firstname;

    protected String lastname;

    @Enumerated(EnumType.STRING)
    protected Role role;

    @Column(length = 9)
    protected String phoneNumber;

    protected Timestamp registrationDate;

    protected Boolean active;

    protected Boolean locked;

    @Enumerated(EnumType.STRING)
    protected GlucoseUnit defaultGlucoseUnit = GlucoseUnit.mgPerDL;

    @JsonIgnore
    public String getPassword() {
        return password;
    }

    @JsonProperty
    public void setPassword(String password) {
        this.password = password;
    }

    private Timestamp creationDate = Timestamp.from(Instant.now());

    private Timestamp modificationDate = Timestamp.from(Instant.now());
}
