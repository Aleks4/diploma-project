package pl.kielce.tu.diabeticstools.dto;

public enum DateScope {
    DAY,
    WEEK,
    MONTH,
    YEAR
}
