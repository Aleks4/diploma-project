package pl.kielce.tu.diabeticstools.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.kielce.tu.diabeticstools.dto.BulkMeasurementDto;
import pl.kielce.tu.diabeticstools.dto.MeasurementDto;
import pl.kielce.tu.diabeticstools.model.Measurement;
import pl.kielce.tu.diabeticstools.model.Patient;
import pl.kielce.tu.diabeticstools.security.SecurityUtils;
import pl.kielce.tu.diabeticstools.service.MeasurementService;

@RestController
@RequestMapping("/api/measurement")
public class MeasurementController {

    private final SecurityUtils utils;

    private final MeasurementService measurementService;

    @Autowired
    public MeasurementController(SecurityUtils utils, MeasurementService measurementService) {
        this.utils = utils;
        this.measurementService = measurementService;
    }

    @PostMapping("/new")
    public ResponseEntity<?> postNewMeasurement(@RequestBody MeasurementDto measurementDto) throws Exception {
        Patient patient = utils.getCurrentPatient();

        measurementService.addNewMeasurement(patient, new Measurement(measurementDto));

        return ResponseEntity
                .ok()
                .build();
    }

    @PostMapping("/new/bulk")
    public ResponseEntity<?> postBulkMeasurements(@RequestBody BulkMeasurementDto bulkMeasurementDto) throws Exception{
        Patient patient = utils.getCurrentPatient();

        measurementService.addBulkMeasurements(patient, bulkMeasurementDto);

        return ResponseEntity
                .ok()
                .build();
    }

    @PutMapping("/edit")
    public ResponseEntity<MeasurementDto> editMeasurement(@RequestBody MeasurementDto measurementDto) throws Exception {
        return ResponseEntity.ok(measurementService.editMeasurement(utils.getCurrentPatient(), measurementDto));
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteMeasurement(@PathVariable Long id) throws Exception{
        measurementService.deleteMeasurement(utils.getCurrentPatient(), id);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getMeasurementById(@PathVariable Long id) throws Exception {
        return ResponseEntity.ok(measurementService.getMeasurementById(utils.getCurrentPatient(), id));
    }
}
