package pl.kielce.tu.diabeticstools.utils.converter;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import pl.kielce.tu.diabeticstools.model.units.GlucoseUnit;

@JsonDeserialize(as = GlucoseUnit.class)
public interface Unit {
}
