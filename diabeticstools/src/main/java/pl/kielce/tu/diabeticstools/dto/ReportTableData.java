package pl.kielce.tu.diabeticstools.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReportTableData {
    @JsonProperty("patient_schedule")
    private PatientScheduleDto patientSchedule;

    @JsonProperty("table_data")
    private List<ReportTableRow> tableData;
}
