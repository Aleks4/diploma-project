package pl.kielce.tu.diabeticstools.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import pl.kielce.tu.diabeticstools.config.ServerConfig;
import pl.kielce.tu.diabeticstools.model.*;
import pl.kielce.tu.diabeticstools.model.embeddable.BloodPressureValue;
import pl.kielce.tu.diabeticstools.model.embeddable.GlucoseValue;
import pl.kielce.tu.diabeticstools.model.type.ReportPeriod;
import pl.kielce.tu.diabeticstools.model.type.ReportType;
import pl.kielce.tu.diabeticstools.model.type.ScheduleType;
import pl.kielce.tu.diabeticstools.model.units.GlucoseUnit;
import pl.kielce.tu.diabeticstools.repository.*;
import pl.kielce.tu.diabeticstools.security.CustomUserDetailsService;
import pl.kielce.tu.diabeticstools.service.ReportService;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

@Service
public class DevelopmentModeStarter {
    private static Logger logger = Logger.getLogger(CustomUserDetailsService.class.getName());

    private final PersonRepository personRepository;

    private final ServerConfig serverConfig;

    private final PatientRepository patientRepository;

    private final PatientSettingsRepository patientSettingsRepository;

    private final ApplicationUtils appUtils;

    private final PatientScheduleRepository patientScheduleRepository;

    private final MeasurementRepository measurementRepository;

    private final ReportService reportService;

    @Autowired
    public DevelopmentModeStarter(PersonRepository personRepository,
                                  ServerConfig serverConfig,
                                  PatientRepository patientRepository,
                                  PatientSettingsRepository patientSettingsRepository,
                                  ApplicationUtils appUtils, PatientScheduleRepository patientScheduleRepository, MeasurementRepository measurementRepository, ReportService reportService) {
        this.personRepository = personRepository;
        this.serverConfig = serverConfig;
        this.patientRepository = patientRepository;
        this.patientSettingsRepository = patientSettingsRepository;
        this.appUtils = appUtils;
        this.patientScheduleRepository = patientScheduleRepository;
        this.measurementRepository = measurementRepository;
        this.reportService = reportService;
    }

    @PostConstruct
    public void initDevelopmentMode() {
        if(appUtils.isDevelopmentMode() && personRepository.findByEmail("j.zamoyski@localhost").isEmpty()
        ) {
            //TODO: exec only in dev
            logger.info("DEVELOPMENT MODE - creating test user...");
            Patient testPatient = new Patient();
            testPatient.setLocked(false);
            testPatient.setActive(true);
            testPatient.setRegistrationDate(new Timestamp(new Date().getTime()));
            testPatient.setRole(Role.ADMINISTRATOR);
            testPatient.setFirstname("Krzysztof");
            testPatient.setLastname("Nowak");
            testPatient.setEmail("j.zamoyski@localhost");
            testPatient.setRole(Role.PATIENT);
            PatientSettings patientSettings = new PatientSettings();
            patientSettings.setTimeWindow(15);
            patientSettings.setIncludeBloodPressure(true);
            patientSettings.setReportType(ReportType.PERIODIC_REPORT);
            patientSettings.setReportPeriod(ReportPeriod.MONTHLY);
            testPatient.setSettings(patientSettings);
            testPatient.setNextReportDate(appUtils.currentTimePlus(testPatient.getSettings().getReportPeriod()));
            String password = "test";
            testPatient.setPassword(new BCryptPasswordEncoder(11).encode(password));
            patientSettingsRepository.save(patientSettings);
            patientRepository.save(testPatient);
            logger.info("\nTest user credentials: \n" +
                    "Email: "+testPatient.getEmail()+"\n" +
                    "Password:"+password+"\n");
            testPatient = patientRepository.findByEmail("j.zamoyski@localhost").get();
            patientScheduleRepository.save(new PatientSchedule(null, testPatient, LocalTime.of(8, 0), "Na czczo", ScheduleType.MEASUREMENT, 15, true));
            patientScheduleRepository.save(new PatientSchedule(null, testPatient, LocalTime.of(14, 0), "Przed obiadem", ScheduleType.MEASUREMENT, 15, true));
            patientScheduleRepository.save(new PatientSchedule(null, testPatient, LocalTime.of(20, 0), "Przed kolacją", ScheduleType.MEASUREMENT, 15, true));

            List<PatientSchedule> schedules = patientScheduleRepository.findByPatient(testPatient);

            Timestamp startDate = Timestamp.valueOf(LocalDateTime.of(LocalDate.now().minus(2, ChronoUnit.MONTHS), LocalTime.MIDNIGHT));
            Timestamp stopDate = Timestamp.valueOf(LocalDateTime.of(LocalDate.now().minus(1, ChronoUnit.DAYS), LocalTime.MIDNIGHT));

            for(Timestamp dateIterator = Timestamp.valueOf(startDate.toLocalDateTime());
                dateIterator.before(stopDate) || dateIterator.equals(stopDate);
                dateIterator = Timestamp.valueOf(dateIterator.toLocalDateTime().plus(1, ChronoUnit.DAYS))) {
                for (PatientSchedule schedule:schedules
                     ) {
                    measurementRepository.save(new Measurement(
                            null,
                            schedule.getPatient(),
                            new GlucoseValue(numberFromRange(69.0, 101.0), GlucoseUnit.mgPerDL),
                            new BloodPressureValue(numberFromRange(89.0, 141.0), numberFromRange(49.0, 91.0), "mmHg"),
                            true,
                            Timestamp.valueOf(LocalDateTime.of(dateIterator.toLocalDateTime().toLocalDate(), schedule.getTime())),
                            schedule,
                            null
                    ));
                }
            }
            reportService.generatePeriodicReportForPatient(testPatient);
        }


    }

    private BigDecimal numberFromRange(Double min, Double max) {
        return BigDecimal.valueOf((Math.random()*((max-min)+1))+min);
    }
}
