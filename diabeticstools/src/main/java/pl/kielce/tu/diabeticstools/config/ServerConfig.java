package pl.kielce.tu.diabeticstools.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@ConfigurationProperties(prefix = "dt")
@Data
@Validated
public class ServerConfig {

    /** Default first-launch email address*/
    //@Pattern(regexp = "^[a-zA-Z0-9_!#$%&’*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$")
    @Email
    private String default_login = "user@localhost";

    /** Default first-launch password*/
    private String default_password;

    /** Default time window for planned measurements (in minutes)*/
    @Min(1)
    @Max(30)
    private Integer timeWindow = 10;

    /** Base64 encoded token signing key. If not defined, generated automatically every launch. */
    private String secret_key;

    private Double min_glucose = 70.0;

    private Double max_glucose = 99.9;
}