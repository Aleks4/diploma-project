package pl.kielce.tu.diabeticstools.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.kielce.tu.diabeticstools.model.Measurement;
import pl.kielce.tu.diabeticstools.model.embeddable.BloodPressureValue;
import pl.kielce.tu.diabeticstools.model.embeddable.GlucoseValue;
import pl.kielce.tu.diabeticstools.model.units.GlucoseUnit;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MeasurementDto implements Serializable {

    private Long id;

    private BigDecimal glucose_value;

    private GlucoseUnit glucose_unit;

    private BigDecimal blood_pressure_upper;

    private BigDecimal blood_pressure_lower;

    private String blood_pressure_unit;

    @JsonProperty("insertion_timestamp")
    @JsonFormat(pattern = "yyyy-MM-dd, HH:mm:ss")
    private Timestamp insertionTimestamp;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate date;

    @JsonFormat(pattern = "HH:mm")
    private LocalTime time;

    @JsonProperty("patient_comment")
    private String patientComment;

    @JsonProperty("patient_schedule")
    private PatientScheduleDto patientSchedule;

    private Boolean editable;

    public MeasurementDto(Measurement measurement) {
        this.editable = measurement.getEditable();
        this.id = measurement.getId();
        this.glucose_value = measurement.getGlucose().getValue();
        this.glucose_unit = measurement.getGlucose().getUnit();
        this.patientSchedule = measurement.getPatientSchedule()!=null?new PatientScheduleDto(measurement.getPatientSchedule()):null;
        if(measurement.getBloodPressure() != null) {
            this.blood_pressure_upper = measurement.getBloodPressure().getUpperValue();
            this.blood_pressure_lower = measurement.getBloodPressure().getLowerValue();
            this.blood_pressure_unit = measurement.getBloodPressure().getUnit();
        }
        this.insertionTimestamp = measurement.getInsertionTimestamp();
        this.patientComment = measurement.getPatientComment();
        this.date = insertionTimestamp.toLocalDateTime().toLocalDate();
        this.time = insertionTimestamp.toLocalDateTime().toLocalTime();
    }
}
