package pl.kielce.tu.diabeticstools.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.kielce.tu.diabeticstools.dto.MeasurementRequestDto;
import pl.kielce.tu.diabeticstools.dto.PatientBriefData;
import pl.kielce.tu.diabeticstools.dto.PatientSettingsDto;
import pl.kielce.tu.diabeticstools.dto.ServerVariablesDto;
import pl.kielce.tu.diabeticstools.model.PatientSettings;
import pl.kielce.tu.diabeticstools.security.SecurityUtils;
import pl.kielce.tu.diabeticstools.service.PatientService;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/patient")
public class PatientController {

    private final SecurityUtils utils;

    private final PatientService patientService;

    @Autowired
    public PatientController(SecurityUtils utils,
                             PatientService patientService) {
        this.utils = utils;
        this.patientService = patientService;
    }

    @PostMapping("/settings/change")
    public ResponseEntity<?> editPatientSettings(@Valid @RequestBody PatientSettingsDto newSettings) throws Exception{
        patientService.changePatientSettings(utils.getCurrentPatient(), newSettings);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/settings/default")
    public ResponseEntity<PatientSettingsDto> getDefaultPatientSettings() {
        return ResponseEntity.ok(new PatientSettingsDto(new PatientSettings()));
    }

    @PostMapping("/brief")
    public ResponseEntity<PatientBriefData> getPatientBriefData(@RequestBody MeasurementRequestDto requestDto) throws Exception{
        return ResponseEntity.ok(patientService.getPatientBriefData(utils.getCurrentPatient(), requestDto));
    }

    @GetMapping("/variables")
    public ResponseEntity<ServerVariablesDto> getServerVars() throws Exception{
        return ResponseEntity.ok(patientService.getServerVariables(utils.getCurrentPatient()));
    }
}
