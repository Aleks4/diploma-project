package pl.kielce.tu.diabeticstools.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import pl.kielce.tu.diabeticstools.dto.MeasurementRequestDto;
import pl.kielce.tu.diabeticstools.security.SecurityUtils;
import pl.kielce.tu.diabeticstools.service.MeasurementHistoryService;
import static org.mockito.Mockito.*;
import static org.junit.Assert.*;
@RunWith(MockitoJUnitRunner.class)
public class MeasurementHistoryControllerTest {
    @Mock
    private MeasurementHistoryService measurementHistoryService;
    @Mock
    private SecurityUtils utils;

    @InjectMocks
    private MeasurementHistoryController controller;

    @Test
    public void shouldInvokeMeasurementsInRangeServiceMethod() throws Exception{
        controller.getMeasurementsInDateRange(new MeasurementRequestDto());
        verify(measurementHistoryService).getMeasurementsInRange(any(), any());
    }

    @Test
    public void shouldInvokeLatestMeasurementsServiceMethod() throws Exception {
        controller.getLatestMeasurements(new MeasurementRequestDto());
        verify(measurementHistoryService).getLatestMeasurements(any(), any());
    }
}
