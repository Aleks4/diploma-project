package pl.kielce.tu.diabeticstools.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import pl.kielce.tu.diabeticstools.dto.BulkMeasurementDto;
import pl.kielce.tu.diabeticstools.dto.MeasurementDto;
import pl.kielce.tu.diabeticstools.security.SecurityUtils;
import pl.kielce.tu.diabeticstools.service.MeasurementService;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class MeasurementControllerTest {
    @Mock
    private SecurityUtils utils;

    @Mock
    private MeasurementService measurementService;

    @InjectMocks
    private MeasurementController measurementController;

    @Test
    public void shouldInvokeAddServiceMethod() throws Exception{
        measurementController.postNewMeasurement(new MeasurementDto());
        verify(measurementService).addNewMeasurement(any(), any());
    }

    @Test
    public void shouldInvokeBulkAddServiceMethod() throws Exception {
        measurementController.postBulkMeasurements(new BulkMeasurementDto());
        verify(measurementService).addBulkMeasurements(any(), any());
    }

    @Test
    public void shouldInvokeEditServiceMethod() throws Exception {
        measurementController.editMeasurement(new MeasurementDto());
        verify(measurementService).editMeasurement(any(), any());
    }

    @Test
    public void shouldInvokeDeleteServiceMethod() throws Exception {
        measurementController.deleteMeasurement(1L);
        verify(measurementService).deleteMeasurement(any(), anyLong());
    }
    @Test
    public void shouldInvokeGetIdServiceMethod() throws Exception {
        measurementController.getMeasurementById(1L);
        verify(measurementService).getMeasurementById(any(), anyLong());
    }
}
