package pl.kielce.tu.diabeticstools.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import pl.kielce.tu.diabeticstools.config.ServerConfig;
import pl.kielce.tu.diabeticstools.dto.BriefPersonData;
import pl.kielce.tu.diabeticstools.dto.RegistrationDto;
import pl.kielce.tu.diabeticstools.dto.UserProfileDto;
import pl.kielce.tu.diabeticstools.exception.UnprocessableEntityException;
import pl.kielce.tu.diabeticstools.model.Patient;
import pl.kielce.tu.diabeticstools.model.PatientSettings;
import pl.kielce.tu.diabeticstools.model.Person;
import pl.kielce.tu.diabeticstools.model.Role;
import pl.kielce.tu.diabeticstools.model.units.GlucoseUnit;
import pl.kielce.tu.diabeticstools.repository.PatientRepository;
import pl.kielce.tu.diabeticstools.repository.PatientSettingsRepository;
import pl.kielce.tu.diabeticstools.repository.PersonRepository;
import pl.kielce.tu.diabeticstools.security.SecurityUtils;
import pl.kielce.tu.diabeticstools.security.jwt.JWTTokenProvider;
import pl.kielce.tu.diabeticstools.utils.ApplicationUtils;
import pl.kielce.tu.diabeticstools.utils.converter.UnitUtils;

import javax.persistence.EntityNotFoundException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.Optional;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class PersonServiceTest {

    @Mock
    private PersonRepository personRepository;
    @Mock
    private PatientRepository patientRepository;
    @Mock
    private PatientSettingsRepository patientSettingsRepository;
    @Mock
    private SecurityUtils utils;
    @Mock
    private JWTTokenProvider tokenProvider;
    @Mock
    private ApplicationUtils appUtils;
    @Mock
    private ServerConfig serverConfig;
    @Spy
    private UnitUtils unitUtils = new UnitUtils();

    @InjectMocks
    private PersonService personService;

    private Person person;

    @Before
    public void setupFixture() {
        person = new Person();
        person.setId(1L);
        person.setLocked(false);
        person.setActive(true);
        person.setRegistrationDate(new Timestamp(new Date().getTime()));
        person.setRole(Role.ADMINISTRATOR);
        person.setFirstname("Krzysztof");
        person.setLastname("Nowak");
        person.setEmail("j.zamoyski@localhost");
        person.setRole(Role.PATIENT);
        person.setDefaultGlucoseUnit(GlucoseUnit.mgPerDL);
        when(utils.getCurrentUserJWT()).thenReturn(Optional.of("x"));
    }

    @Test
    public void shouldSuccessfullyChangeProfile() throws Exception {
        UserProfileDto profileDto = new UserProfileDto(person);
        profileDto.setDefaultGlucoseUnit(GlucoseUnit.mmolPerL);
        profileDto.setFirstname("Jan");
        profileDto.setLastname("Kowalski");
        personService.changeUserProfile(person, profileDto);
        verifyZeroInteractions(tokenProvider, utils);
        verify(personRepository).save(any());
    }

    @Test
    public void shouldLogOutAfterChangingEmail() throws Exception {
        UserProfileDto profileDto = new UserProfileDto(person);
        profileDto.setDefaultGlucoseUnit(GlucoseUnit.mmolPerL);
        profileDto.setEmail("j.kowalski@localhost");
        profileDto.setFirstname("Jan");
        profileDto.setLastname("Kowalski");
        personService.changeUserProfile(person, profileDto);
        verify(tokenProvider, times(1)).blacklistToken("x");
        verify(utils, times(1)).getCurrentUserJWT();
        verify(personRepository).save(any());
    }

    @Test
    public void shouldGetPersonBrief() throws Exception {
        Authentication authentication = Mockito.mock(Authentication.class);
        when(authentication.getName()).thenReturn("j.zamoyski@localhost");
        SecurityContext context = mock(SecurityContext.class);
        when(context.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(context);
        when(personRepository.findByEmail("j.zamoyski@localhost")).thenReturn(Optional.of(person));

        BriefPersonData briefPersonData = personService.getPersonBrief();
        assertNotNull(briefPersonData);
    }

    @Test(expected = EntityNotFoundException.class)
    public void shouldNotGetBriefAndThrowException() throws Exception {
        Authentication authentication = Mockito.mock(Authentication.class);
        when(authentication.getName()).thenReturn("j.zamoyskj@localhost");
        SecurityContext context = mock(SecurityContext.class);
        when(context.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(context);
        when(personRepository.findByEmail("j.zamoyskj@localhost")).thenReturn(Optional.empty());

        BriefPersonData briefPersonData = personService.getPersonBrief();
        assertNotNull(briefPersonData);
    }

    @Test
    public void shouldRegisterSuccessfully() throws Exception{
        RegistrationDto registrationDto = new RegistrationDto();
        registrationDto.setFirstname("Jan");
        registrationDto.setLastname("Kowalski");
        registrationDto.setEmail("j.kowalski@127.0.0.1");
        registrationDto.setPassword("test123");
        registrationDto.setConfirmPassword("test123");
        registrationDto.setDefaultGlucoseUnit(GlucoseUnit.mgPerDL);
        when(appUtils.currentTimePlus(any())).thenReturn(Timestamp.valueOf(LocalDateTime.now().plus(1, ChronoUnit.MONTHS)));
        personService.registerNewPatient(registrationDto);

        verify(patientRepository).save(any());
    }

    @Test(expected = UnprocessableEntityException.class)
    public void shouldRejectBecauseOfInvalidPasswords() throws Exception {
        RegistrationDto registrationDto = new RegistrationDto();
        registrationDto.setFirstname("Jan");
        registrationDto.setLastname("Kowalski");
        registrationDto.setEmail("j.kowalski@127.0.0.1");
        registrationDto.setPassword("test123");
        registrationDto.setConfirmPassword("test1234");
        registrationDto.setDefaultGlucoseUnit(GlucoseUnit.mgPerDL);
        personService.registerNewPatient(registrationDto);
    }

    @Test(expected = UnprocessableEntityException.class)
    public void shouldRejectBecauseOfAlreadyTakenEmail() throws Exception {
        RegistrationDto registrationDto = new RegistrationDto();
        registrationDto.setFirstname("Jan");
        registrationDto.setLastname("Kowalski");
        registrationDto.setEmail("j.kowalski@127.0.0.1");
        registrationDto.setPassword("test123");
        registrationDto.setConfirmPassword("test1234");
        registrationDto.setDefaultGlucoseUnit(GlucoseUnit.mgPerDL);
        personService.registerNewPatient(registrationDto);
    }

    @Test
    public void passwordHashesShouldMatch() throws Exception {
        RegistrationDto registrationDto = new RegistrationDto();
        registrationDto.setFirstname("Jan");
        registrationDto.setLastname("Kowalski");
        registrationDto.setEmail("j.kowalski@127.0.0.1");
        registrationDto.setPassword("test123");
        registrationDto.setConfirmPassword("test123");
        registrationDto.setDefaultGlucoseUnit(GlucoseUnit.mgPerDL);
        personService.registerNewPatient(registrationDto);
        ArgumentCaptor<Patient> argument = ArgumentCaptor.forClass(Patient.class);
        verify(patientRepository).save(argument.capture());

        Patient patient = argument.getValue();
        assertTrue(new BCryptPasswordEncoder(11).matches("test123", patient.getPassword()));
    }
}
