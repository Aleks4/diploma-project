package pl.kielce.tu.diabeticstools.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;
import pl.kielce.tu.diabeticstools.dto.MeasurementRequestDto;
import pl.kielce.tu.diabeticstools.dto.PatientSettingsDto;
import pl.kielce.tu.diabeticstools.security.SecurityUtils;
import pl.kielce.tu.diabeticstools.service.PatientService;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;
@RunWith(MockitoJUnitRunner.class)
public class PatientControllerTest {
    @Mock
    private SecurityUtils utils;
    @Mock
    private PatientService patientService;

    @InjectMocks
    private PatientController controller;

    @Test
    public void shouldInvokeSettingsEditingServiceMethod() throws Exception{
        controller.editPatientSettings(new PatientSettingsDto());
        verify(patientService).changePatientSettings(any(), any());
    }

    @Test
    public void shouldGetDefaultSettings() throws Exception {
        ResponseEntity response = controller.getDefaultPatientSettings();
        assertNotNull(response.getBody());
        assertTrue(response.getBody() instanceof PatientSettingsDto);
    }

    @Test
    public void shouldInvokePatientBriefDataServiceMethod() throws Exception {
        controller.getPatientBriefData(new MeasurementRequestDto());
        verify(patientService).getPatientBriefData(any(), any());
    }

    @Test
    public void shouldGetServerVars() throws Exception {
        controller.getServerVars();
        verify(patientService).getServerVariables(any());
    }
}
