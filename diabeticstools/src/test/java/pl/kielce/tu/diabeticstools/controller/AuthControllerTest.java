package pl.kielce.tu.diabeticstools.controller;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import pl.kielce.tu.diabeticstools.dto.CredentialsDto;
import pl.kielce.tu.diabeticstools.dto.JWTToken;
import pl.kielce.tu.diabeticstools.dto.RegistrationDto;
import pl.kielce.tu.diabeticstools.security.SecurityUtils;
import pl.kielce.tu.diabeticstools.security.jwt.JWTTokenProvider;
import pl.kielce.tu.diabeticstools.service.PersonService;

import javax.servlet.http.HttpServletRequest;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class AuthControllerTest {
    @Mock
    private JWTTokenProvider tokenProvider;
    @Mock
    private AuthenticationManager authenticationManager = mock(AuthenticationManager.class);
    @Mock
    private SecurityUtils utils;
    @Mock
    private PersonService personService;
    @InjectMocks
    private AuthController authController;

    @Mock
    private SecurityContext context = mock(SecurityContext.class);
    @Mock
    private Authentication authentication;

    @Before
    public void startupStatic() {
        SecurityContextHolder.setContext(context);
        tokenProvider = mock(JWTTokenProvider.class);
        when(authenticationManager.authenticate(any()))
                .thenReturn(authentication);
    }

    @Test
    public void shouldSuccessfullyAuthenticate() {
        CredentialsDto credentialsDto = new CredentialsDto();
        credentialsDto.setEmail("j.zamoyski@localhost");
        credentialsDto.setPassword("test");
        credentialsDto.setRememberMe(true);
        ResponseEntity<JWTToken> response = authController.authorize(credentialsDto);
        JWTToken tokenWrapper = response.getBody();
        assertNotNull(tokenWrapper);
    }

    @Test
    public void shouldRegisterSuccessfully() throws Exception {
        authController.registerNewPatient(new RegistrationDto());

        verify(personService).registerNewPatient(any());
    }

//    @Test
//    public void shouldLogoutSuccessfully() throws Exception {
//        HttpServletRequest request = mock(HttpServletRequest.class);
//        when(utils.resolveToken(any())).thenReturn("token_to_blacklist");
//        authController.logout(request);
//        verify(tokenProvider).blacklistToken(any());
//    }
}
