package pl.kielce.tu.diabeticstools.controller;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import pl.kielce.tu.diabeticstools.dto.ReportRequestDto;
import pl.kielce.tu.diabeticstools.security.SecurityUtils;
import pl.kielce.tu.diabeticstools.service.ReportService;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class ReportControllerTest {
    @Mock
    private SecurityUtils securityUtils;
    @Mock
    private ReportService reportService;
    @InjectMocks
    private ReportController controller;

    @Test
    public void shouldInvokeGetReportByIdServiceMethod() throws Exception{
        controller.getReportById(1L);
        verify(reportService).getReportById(any(), anyLong());
    }

    @Test
    public void shouldInvokeGetReportBriefListServiceMethod() throws Exception {
        controller.getReportBriefList();
        verify(reportService).getReportBriefList(any());
    }

    @Test
    public void shouldInvokeReportGenerationServiceMethod() throws Exception{
        controller.generateManualReport(new ReportRequestDto());
        verify(reportService).generateManualReportForPatient(any(), any());
    }
}
