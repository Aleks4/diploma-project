package pl.kielce.tu.diabeticstools.service;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import pl.kielce.tu.diabeticstools.dto.ReportBriefDto;
import pl.kielce.tu.diabeticstools.dto.ReportDto;
import pl.kielce.tu.diabeticstools.dto.ReportRequestDto;
import pl.kielce.tu.diabeticstools.model.*;
import pl.kielce.tu.diabeticstools.model.embeddable.BloodPressureValue;
import pl.kielce.tu.diabeticstools.model.embeddable.GlucoseValue;
import pl.kielce.tu.diabeticstools.model.type.ReportPeriod;
import pl.kielce.tu.diabeticstools.model.type.ReportType;
import pl.kielce.tu.diabeticstools.model.type.ScheduleType;
import pl.kielce.tu.diabeticstools.model.units.GlucoseUnit;
import pl.kielce.tu.diabeticstools.repository.MeasurementRepository;
import pl.kielce.tu.diabeticstools.repository.PatientRepository;
import pl.kielce.tu.diabeticstools.repository.PatientScheduleRepository;
import pl.kielce.tu.diabeticstools.repository.ReportRepository;
import pl.kielce.tu.diabeticstools.utils.ApplicationUtils;
import pl.kielce.tu.diabeticstools.utils.TimeValue;
import pl.kielce.tu.diabeticstools.utils.converter.UnitUtils;

import java.math.BigDecimal;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class ReportServiceTest {

    @Mock
    private MeasurementRepository measurementRepository;
    @Mock
    private PatientRepository patientRepository;
    @Mock
    private PatientScheduleRepository patientScheduleRepository;
    @Mock
    private ReportRepository reportRepository;
    @Mock
    private ApplicationUtils applicationUtils;
    @Spy
    private UnitUtils unitUtils = new UnitUtils();

    @InjectMocks
    private ReportService reportService;

    private List<PatientSchedule> schedules = new ArrayList<>();

    private List<Measurement> measurements = new ArrayList<>();

    private Patient testPatient = new Patient();

    private Report report = new Report();

    @Before
    public void setupClass() {
        testPatient.setId(1L);
        testPatient.setLocked(false);
        testPatient.setActive(true);
        testPatient.setRegistrationDate(new Timestamp(new Date().getTime()));
        testPatient.setRole(Role.ADMINISTRATOR);
        testPatient.setFirstname("Krzysztof");
        testPatient.setLastname("Nowak");
        testPatient.setEmail("j.zamoyski@localhost");
        testPatient.setRole(Role.PATIENT);
        PatientSettings patientSettings = new PatientSettings();
        patientSettings.setTimeWindow(15);
        patientSettings.setIncludeBloodPressure(true);
        patientSettings.setReportType(ReportType.PERIODIC_REPORT);
        patientSettings.setReportPeriod(ReportPeriod.MONTHLY);
        testPatient.setSettings(patientSettings);
        testPatient.setNextReportDate(Timestamp.valueOf(LocalDateTime.now().plus(1, ChronoUnit.MONTHS)));
        String password = "test";
        testPatient.setPassword(new BCryptPasswordEncoder(11).encode(password));

        schedules.add(new PatientSchedule(1L, testPatient, LocalTime.of(8, 0), "Na czczo", ScheduleType.MEASUREMENT, 15, true));
        schedules.add(new PatientSchedule(2L, testPatient, LocalTime.of(14, 0), "Przed obiadem", ScheduleType.MEASUREMENT, 15, true));
        schedules.add(new PatientSchedule(3L, testPatient, LocalTime.of(20, 0), "Przed kolacją", ScheduleType.MEASUREMENT, 15, true));

        Timestamp startDate = Timestamp.valueOf(LocalDateTime.of(LocalDate.now().minus(2, ChronoUnit.MONTHS), LocalTime.MIDNIGHT));
        Timestamp stopDate = Timestamp.valueOf(LocalDateTime.of(LocalDate.now().minus(1, ChronoUnit.DAYS), LocalTime.MIDNIGHT));


        for(Timestamp dateIterator = Timestamp.valueOf(startDate.toLocalDateTime());
            dateIterator.before(stopDate) || dateIterator.equals(stopDate);
            dateIterator = Timestamp.valueOf(dateIterator.toLocalDateTime().plus(1, ChronoUnit.DAYS))) {
            for (PatientSchedule schedule:schedules
            ) {
                measurements.add(new Measurement(
                        null,
                        schedule.getPatient(),
                        new GlucoseValue(numberFromRange(69.0, 101.0), GlucoseUnit.mgPerDL),
                        new BloodPressureValue(numberFromRange(89.0, 141.0), numberFromRange(49.0, 91.0), "mmHg"),
                        true,
                        Timestamp.valueOf(LocalDateTime.of(dateIterator.toLocalDateTime().toLocalDate(), schedule.getTime())),
                        schedule,
                        null
                ));
            }
        }
        List<Measurement> measurementsForReport = new ArrayList<>();

        Timestamp start = Timestamp.valueOf(LocalDate.now().minus(1, ChronoUnit.MONTHS).atTime(LocalTime.MIDNIGHT));
        Timestamp stop = Timestamp.valueOf(LocalDate.now().minus(1, ChronoUnit.DAYS).atTime(LocalTime.MAX));

        measurements.forEach(measurement -> {
            if((measurement.getInsertionTimestamp().toLocalDateTime().isAfter(start.toLocalDateTime())
                    || measurement.getInsertionTimestamp().toLocalDateTime().isEqual(start.toLocalDateTime()))
            &&
                    measurement.getInsertionTimestamp().toLocalDateTime().isBefore(stop.toLocalDateTime())
                    ||measurement.getInsertionTimestamp().toLocalDateTime().isEqual(stop.toLocalDateTime())
            ) {
                measurementsForReport.add(measurement);
            }
        });

        report.setMeasurements(measurementsForReport);
        report.setType(ReportType.MANUAL_REPORT);
        report.setStartDate(start);
        report.setEndDate(stop);
        report.setPatient(testPatient);
        Page<Measurement> measurementPage = new PageImpl<>(measurementsForReport);
        when(measurementRepository.findByPatientIdAndInsertionTimestampBetweenOrderByInsertionTimestampDesc(any(), any(), any(), any()))
                .thenReturn(measurementPage);
        when(patientScheduleRepository.findByPatient(any()))
                .thenReturn(schedules);
        TimeValue timeValue = new TimeValue();
        timeValue.setUnit(ChronoUnit.MONTHS);
        timeValue.setValue(1);
        when(applicationUtils.mapPeriodToUnit(ReportPeriod.MONTHLY)).thenReturn(timeValue);
        when(reportRepository.findById(anyLong())).thenReturn(Optional.of(report));
    }

    @Test
    public void shouldGenerateReportSuccessfully() {
        reportService.generatePeriodicReportForPatient(testPatient);
        ArgumentCaptor<Report> argumentCaptor = ArgumentCaptor.forClass(Report.class);
        verify(reportRepository).save(argumentCaptor.capture());
    }

    @Test
    public void shouldGenerateManualReportSuccessfully() {
        ReportRequestDto requestDto = new ReportRequestDto();
        requestDto.setStartDate(LocalDate.now().minus(1, ChronoUnit.MONTHS));
        requestDto.setEndDate(LocalDate.now().minus(1, ChronoUnit.DAYS));
        reportService.generateManualReportForPatient(testPatient, requestDto);
    }

    @Test
    public void shouldRetrieveOneReportSuccessfully() throws Exception {
        ReportDto reportDto = reportService.getReportById(testPatient, 1L);
        assertNotNull(reportDto);
    }

    @Test
    public void shouldRetrieveListSuccessfully() {
        when(reportRepository.findByPatient(any()))
                .thenReturn(
                        List.of(report)
                );
        List<ReportBriefDto> reports = reportService.getReportBriefList(testPatient);

        assertTrue(reports.size()>0);
    }

    private static BigDecimal numberFromRange(Double min, Double max) {
        return BigDecimal.valueOf((Math.random()*((max-min)+1))+min);
    }
}
