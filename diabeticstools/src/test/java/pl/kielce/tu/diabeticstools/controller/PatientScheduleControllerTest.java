package pl.kielce.tu.diabeticstools.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import pl.kielce.tu.diabeticstools.dto.PatientScheduleDto;
import pl.kielce.tu.diabeticstools.security.SecurityUtils;
import pl.kielce.tu.diabeticstools.service.PatientScheduleService;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;
@RunWith(MockitoJUnitRunner.class)
public class PatientScheduleControllerTest {
    @Mock
    private SecurityUtils utils;
    @Mock
    private PatientScheduleService patientScheduleService;

    @InjectMocks
    private PatientScheduleController controller;

    @Test
    public void shouldInvokeAddScheduleServiceMethod() throws Exception{
        controller.newSchedule(new PatientScheduleDto());
        verify(patientScheduleService).addNewSchedule(any(), any());
    }

    @Test
    public void shouldInvokeGetByIdServiceMethod() throws Exception {
        controller.getScheduleById(1L);
        verify(patientScheduleService).getScheduleById(any(), anyLong());
    }

    @Test
    public void shouldInvokeGetListServiceMethod() throws Exception {
        controller.getPatientScheduleList();
        verify(patientScheduleService).getScheduleList(any());
    }

    @Test
    public void shouldInvokeDeleteServiceMethod() throws Exception {
        controller.deleteSchedule(1L);
        verify(patientScheduleService).deleteSchedule(any(), anyLong());
    }
}
