package pl.kielce.tu.diabeticstools.security;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import pl.kielce.tu.diabeticstools.model.Person;
import pl.kielce.tu.diabeticstools.model.Role;
import pl.kielce.tu.diabeticstools.model.units.GlucoseUnit;

import java.sql.Timestamp;
import java.util.Date;
import java.util.stream.Collectors;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;
@RunWith(MockitoJUnitRunner.class)
public class PersonWrapperTest {
    private Person person;

    private PersonWrapper wrapper;

    @Before
    public void setupFixture() {
        person = new Person();
        person.setId(1L);
        person.setLocked(false);
        person.setActive(true);
        person.setRegistrationDate(new Timestamp(new Date().getTime()));
        person.setFirstname("Krzysztof");
        person.setLastname("Nowak");
        person.setEmail("j.zamoyski@localhost");
        person.setRole(Role.PATIENT);
        person.setDefaultGlucoseUnit(GlucoseUnit.mgPerDL);

        wrapper = new PersonWrapper(person);
    }

    @Test
    public void shouldRetrieveRole() {
        assertEquals(
                person.getRole().toString(),
                wrapper.getAuthorities()
                        .stream()
                        .collect(Collectors.toList())
                        .get(0)
                        .getAuthority()
        );
    }

    @Test
    public void shouldGetPassword() {
        assertEquals(
                person.getPassword(),
                wrapper.getPassword()

        );
    }

    @Test
    public void shouldGetUsername() {
        assertEquals(
                person.getEmail(),
                wrapper.getUsername()
        );
    }

    @Test
    public void shouldGetNonExpired() {
        assertTrue(wrapper.isAccountNonExpired());
    }

    @Test
    public void shouldGetAccountLockInformation() {
        assertEquals(person.getLocked(),
                !wrapper.isAccountNonLocked());
    }

    @Test
    public void shouldGetCredentialsExpirationData() {
        assertTrue(wrapper.isCredentialsNonExpired());
    }

    @Test
    public void shouldGetInfoIfAccountIsEnabled() {
        assertEquals(
                person.getActive(),
                wrapper.isEnabled()
        );
    }
}
