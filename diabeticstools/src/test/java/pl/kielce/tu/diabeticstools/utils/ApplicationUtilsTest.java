package pl.kielce.tu.diabeticstools.utils;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.env.Environment;
import pl.kielce.tu.diabeticstools.model.type.ReportPeriod;

import java.lang.reflect.Array;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class ApplicationUtilsTest {

    @Mock
    private Environment environment;

    @InjectMocks
    private ApplicationUtils applicationUtils;

    @Test
    public void shouldConfirmDevMode() {
        String[] profiles = {"dev"};
        when(environment.getActiveProfiles()).thenReturn(profiles);
        assertTrue(applicationUtils.isDevelopmentMode());
    }

    @Test
    public void checkPeriodMapping() {
        assertEquals(applicationUtils.mapPeriodToUnit(ReportPeriod.WEEKLY), new TimeValue(1, ChronoUnit.WEEKS));
        assertEquals(applicationUtils.mapPeriodToUnit(ReportPeriod.MONTHLY), new TimeValue(1, ChronoUnit.MONTHS));
        assertEquals(applicationUtils.mapPeriodToUnit(ReportPeriod.QUARTERLY), new TimeValue(3, ChronoUnit.MONTHS));
        assertEquals(applicationUtils.mapPeriodToUnit(ReportPeriod.ANNUAL), new TimeValue(1, ChronoUnit.YEARS));
    }

    @Test
    public void checkIfAddsTimeCorrectly() {
        assertEquals(
                applicationUtils.currentTimePlus(ReportPeriod.QUARTERLY).toLocalDateTime().toLocalDate(),
                LocalDateTime.now().plus(3, ChronoUnit.MONTHS).toLocalDate()
        );
    }
}
