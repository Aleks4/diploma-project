package pl.kielce.tu.diabeticstools.security;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.userdetails.UserDetails;
import pl.kielce.tu.diabeticstools.config.ServerConfig;
import pl.kielce.tu.diabeticstools.model.Person;
import pl.kielce.tu.diabeticstools.model.Role;
import pl.kielce.tu.diabeticstools.model.units.GlucoseUnit;
import pl.kielce.tu.diabeticstools.repository.PersonRepository;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Optional;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class CustomUserDetailsServiceTest {

    private Person person;

    @Mock
    private PersonRepository personRepository;

    @Mock
    private ServerConfig serverConfig;

    @InjectMocks
    private CustomUserDetailsService service;

    @Before
    public void setupFixture() {
        person = new Person();
        person.setId(1L);
        person.setLocked(false);
        person.setActive(true);
        person.setRegistrationDate(new Timestamp(new Date().getTime()));
        person.setRole(Role.ADMINISTRATOR);
        person.setFirstname("Krzysztof");
        person.setLastname("Nowak");
        person.setEmail("j.zamoyski@localhost");
        person.setRole(Role.PATIENT);
        person.setDefaultGlucoseUnit(GlucoseUnit.mgPerDL);
        when(personRepository.findByEmail(anyString())).thenReturn(Optional.of(person));

    }

    @Test
    public void shouldLoadPersonByUsername() {
        UserDetails wrapper = service.loadUserByUsername("any");

        assertTrue(wrapper instanceof PersonWrapper);
    }
}
