package pl.kielce.tu.diabeticstools.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import pl.kielce.tu.diabeticstools.security.jwt.JWTTokenProvider;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class CronServiceTest {

    @Mock
    private ReportService reportService;

    @Mock
    private JWTTokenProvider tokenProvider;

    @InjectMocks
    private CronService cronService;

    @Test
    public void shouldGenerateReportsAndCleanupTokens() {
        cronService.execute();
        verify(reportService).generateReports();
        verify(tokenProvider).cleanupTokens();
    }
}
