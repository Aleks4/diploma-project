package pl.kielce.tu.diabeticstools.utils;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.dialect.Dialect;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;
@RunWith(MockitoJUnitRunner.class)
public class CustomMariaDBDialectTest {

    @Test
    public void shouldAppendRowFormatSpecifier() {
        Dialect dialect = new CustomMariaDBDialect();
        assertTrue(StringUtils.contains(dialect.getTableTypeString(), " ROW_FORMAT=DYNAMIC"));

    }
}
