package pl.kielce.tu.diabeticstools.service;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import pl.kielce.tu.diabeticstools.dto.DateScope;
import pl.kielce.tu.diabeticstools.dto.MeasurementListDto;
import pl.kielce.tu.diabeticstools.dto.MeasurementRequestDto;
import pl.kielce.tu.diabeticstools.dto.PagingInfo;
import pl.kielce.tu.diabeticstools.model.Measurement;
import pl.kielce.tu.diabeticstools.model.Patient;
import pl.kielce.tu.diabeticstools.model.PatientSettings;
import pl.kielce.tu.diabeticstools.model.Role;
import pl.kielce.tu.diabeticstools.model.embeddable.BloodPressureValue;
import pl.kielce.tu.diabeticstools.model.embeddable.GlucoseValue;
import pl.kielce.tu.diabeticstools.model.type.ReportPeriod;
import pl.kielce.tu.diabeticstools.model.type.ReportType;
import pl.kielce.tu.diabeticstools.model.units.GlucoseUnit;
import pl.kielce.tu.diabeticstools.repository.MeasurementRepository;
import pl.kielce.tu.diabeticstools.utils.converter.UnitUtils;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class MeasurementHistoryServiceTest {

    @Mock
    private MeasurementRepository measurementRepository;

    @Spy
    private static UnitUtils unitUtils;

    @InjectMocks
    private MeasurementHistoryService measurementHistoryService;

    static Patient testPatient;

    static MeasurementRequestDto requestDto = new MeasurementRequestDto();

    List<Measurement> measurements;

    @Before
    public void setup() {
        testPatient = new Patient();
        testPatient.setId(1L);
        testPatient.setLocked(false);
        testPatient.setActive(true);
        testPatient.setRegistrationDate(new Timestamp(new Date().getTime()));
        testPatient.setRole(Role.ADMINISTRATOR);
        testPatient.setFirstname("Krzysztof");
        testPatient.setLastname("Nowak");
        testPatient.setEmail("j.zamoyski@localhost");
        testPatient.setRole(Role.PATIENT);
        testPatient.setDefaultGlucoseUnit(GlucoseUnit.mgPerDL);
        PatientSettings patientSettings = new PatientSettings();
        patientSettings.setTimeWindow(15);
        patientSettings.setIncludeBloodPressure(true);
        patientSettings.setReportType(ReportType.PERIODIC_REPORT);
        patientSettings.setReportPeriod(ReportPeriod.MONTHLY);
        testPatient.setSettings(patientSettings);

        requestDto.setPagingInfo(new PagingInfo());
        requestDto.getPagingInfo().setPageNumber(1);
        requestDto.getPagingInfo().setPageSize(10);
        requestDto.setStartDate(LocalDate.now().minus(1, ChronoUnit.MONTHS));
        requestDto.setStopDate(LocalDate.now());
        requestDto.setDateScope(DateScope.MONTH);

        measurements = new ArrayList<>();
        measurements.add(new Measurement(
                1L,
                testPatient,
                new GlucoseValue(numberFromRange(69.0, 101.0), GlucoseUnit.mgPerDL),
                new BloodPressureValue(numberFromRange(89.0, 141.0), numberFromRange(49.0, 91.0), "mmHg"),
                true,
                Timestamp.valueOf(LocalDateTime.of(2020, 1, 24, 7, 45, 0)),
                null,
                null
        ));
        measurements.add(new Measurement(
                2L,
                testPatient,
                new GlucoseValue(numberFromRange(69.0, 101.0), GlucoseUnit.mgPerDL),
                new BloodPressureValue(numberFromRange(89.0, 141.0), numberFromRange(49.0, 91.0), "mmHg"),
                true,
                Timestamp.valueOf(LocalDateTime.of(2020, 1, 24, 7, 45, 0)),
                null,
                null
        ));
        Page<Measurement> measurementPage = new PageImpl<>(measurements, PageRequest.of(
                requestDto.getPagingInfo().getPageNumber(),
                requestDto.getPagingInfo().getPageSize()),
                measurements.size()
        );
        when(measurementRepository.findByPatientIdAndInsertionTimestampBetweenOrderByInsertionTimestampDesc(anyLong(), any(), any(), any()))
                .thenReturn(measurementPage);
        when(measurementRepository.findByPatientIdAndInsertionTimestampAfterOrderByInsertionTimestampDesc(anyLong(), any(), any()))
                .thenReturn(measurementPage);
    }

    @Test
    public void returnsMeasurementsInRange() throws Exception{

        assertEquals(measurementHistoryService.getMeasurementsInRange(testPatient, requestDto).getMeasurementList().size(), 2);
    }

    @Test
    public void returnsMeasurementsLatest() throws Exception{

        assertEquals(measurementHistoryService.getLatestMeasurements(testPatient, requestDto).getMeasurementList().size(), 2);
    }

    private static BigDecimal numberFromRange(Double min, Double max) {
        return BigDecimal.valueOf((Math.random()*((max-min)+1))+min);
    }
}
