package pl.kielce.tu.diabeticstools.security;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.AuthenticationException;

import javax.servlet.http.HttpServletResponse;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;
@RunWith(MockitoJUnitRunner.class)
public class RestAuthenticationEntryPointTest {

    private RestAuthenticationEntryPoint restAuthenticationEntryPoint = new RestAuthenticationEntryPoint();

    @Test
    public void shouldSendError() throws Exception{
        HttpServletResponse response = mock(HttpServletResponse.class);
        AuthenticationException e = mock(AuthenticationException.class);
        when(e.getLocalizedMessage()).thenReturn("message");
        restAuthenticationEntryPoint.commence(null, response, e);
        verify(response).sendError(eq(401), anyString());
    }
}
