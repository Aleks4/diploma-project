package pl.kielce.tu.diabeticstools.security.jwt;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import pl.kielce.tu.diabeticstools.config.ServerConfig;
import pl.kielce.tu.diabeticstools.model.Role;
import pl.kielce.tu.diabeticstools.model.TokenBlacklist;
import pl.kielce.tu.diabeticstools.repository.TokenBlacklistRepository;

import javax.crypto.KeyGenerator;
import java.security.Key;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class JWTTokenProviderTest {
    @Mock
    private TokenBlacklistRepository tokenBlacklistRepository;
    @Mock
    private ServerConfig serverConfig;
    @InjectMocks
    private JWTTokenProvider tokenProvider;

    private Authentication authentication;

    private Key key;

    @Before
    public void setupFixture() throws Exception{
        authentication = mock(Authentication.class);
        when(authentication.getName()).thenReturn("j.zamoyski@localhost");

        Vector<SimpleGrantedAuthority> role = new Vector<>();
        role.add(new SimpleGrantedAuthority(Role.PATIENT.toString()));
        Collection authorities = Collections.list(role.elements());
        when(authentication.getAuthorities()).thenReturn(authorities);
        KeyGenerator keyGenerator = KeyGenerator.getInstance("HmacSHA512");
        this.key = keyGenerator.generateKey();
        when(serverConfig.getSecret_key()).thenReturn(Base64.getEncoder().encodeToString(this.key.getEncoded()));
        tokenProvider.afterPropertiesSet();
    }

    @After
    public void tearDown() {
        reset(tokenBlacklistRepository);
    }

    @Test
    public void shouldGetAuthentication() {
        String token = Jwts.builder()
                .setSubject("j.zamoyski@localhost")
                .claim("auth", Role.PATIENT.toString())
                .signWith(key, SignatureAlgorithm.HS512)
                .setExpiration(
                        new Date((new Date()).getTime() + 1000 * 3600)
                )
                .compact();
        Authentication auth = tokenProvider.getAuthentication(token);
        assertNotNull(auth);
        assertEquals(Role.PATIENT.toString(), auth.getAuthorities().stream().collect(Collectors.toList()).get(0).getAuthority());
        assertEquals("j.zamoyski@localhost", auth.getName());
    }

    @Test
    public void shouldSuccessfullyCreateToken() {
        String token = tokenProvider.createToken(authentication, true);
        assertNotNull(token);
        Claims claims = Jwts.parser()
                .setSigningKey(key)
                .parseClaimsJws(token)
                .getBody();
        Collection<? extends GrantedAuthority> authorities =
                Arrays.stream(claims.get("auth").toString().split(","))
                        .map(SimpleGrantedAuthority::new)
                        .collect(Collectors.toList());
        assertEquals(Role.PATIENT.toString(), authorities.stream().collect(Collectors.toList()).get(0).getAuthority());
        assertEquals("j.zamoyski@localhost", claims.get("sub"));
    }

    @Test
    public void shouldSuccessfullyValidateGeneratedToken() {
        String token = Jwts.builder()
                .setSubject("j.zamoyski@localhost")
                .claim("auth", Role.PATIENT.toString())
                .signWith(key, SignatureAlgorithm.HS512)
                .setExpiration(
                        new Date((new Date()).getTime() + 1000 * 3600)
                )
                .compact();
        assertTrue(tokenProvider.validateToken(token));
    }

    @Test
    public void shouldCleanupOnlyExpiredToken() {
        List<String> tokens = new ArrayList<>();
        tokens.add(Jwts.builder()
                .setSubject("j.zamoyski@localhost")
                .claim("auth", Role.PATIENT.toString())
                .signWith(key, SignatureAlgorithm.HS512)
                .setExpiration(
                        new Date((new Date()).getTime() + 1000 * 3600)
                )
                .compact());
        tokens.add(Jwts.builder()
                .setSubject("j.zamoyski@localhost")
                .claim("auth", Role.PATIENT.toString())
                .signWith(key, SignatureAlgorithm.HS512)
                .setIssuedAt(Date.from(
                        Instant.now().minus(1, ChronoUnit.DAYS)
                ))
                .setExpiration(
                        Date.from(Instant.now().minus(10L, ChronoUnit.MINUTES))
                )
                .compact());
        List<TokenBlacklist> tokenBlacklist = tokens
                .stream()
                .map(TokenBlacklist::new)
                .collect(Collectors.toList());
        when(tokenBlacklistRepository.findAll()).thenReturn(tokenBlacklist);
        tokenProvider.cleanupTokens();
        verify(tokenBlacklistRepository, times(1)).delete(any());
    }

    @Test
    public void shouldBlacklistToken() {
        when(tokenBlacklistRepository.findByToken(any()))
                .thenReturn(Optional.empty());
        tokenProvider.blacklistToken("token");
        verify(tokenBlacklistRepository).save(any());

    }

    @Test
    public void shouldNotBlacklistWhenAlreadyBlacklisted() {
        String token = Jwts.builder()
                .setSubject("j.zamoyski@localhost")
                .claim("auth", Role.PATIENT.toString())
                .signWith(key, SignatureAlgorithm.HS512)
                .setExpiration(
                        new Date((new Date()).getTime() + 1000 * 3600)
                )
                .compact();
        when(tokenBlacklistRepository.findByToken(any()))
                .thenReturn(Optional.of(new TokenBlacklist(token)));
        tokenProvider.blacklistToken("x");
        verify(tokenBlacklistRepository, times(0)).save(any());
    }
}
