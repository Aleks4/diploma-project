package pl.kielce.tu.diabeticstools.security;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import pl.kielce.tu.diabeticstools.model.Patient;
import pl.kielce.tu.diabeticstools.model.Person;
import pl.kielce.tu.diabeticstools.repository.PatientRepository;
import pl.kielce.tu.diabeticstools.repository.PersonRepository;

import javax.servlet.http.HttpServletRequest;

import java.util.Optional;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class SecurityUtilsTest {

    @Mock
    private PersonRepository personRepository;
    @Mock
    private PatientRepository patientRepository;

    @InjectMocks
    private SecurityUtils utils;

    @Test
    public void shouldResolveTokenSuccessfully() {
        HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getHeader("Authorization")).thenReturn("Bearer testString");
        assertEquals("testString", utils.resolveToken(request));
    }

    @Test
    public void shouldReturnNullBecauseInvalidFormat() {
        HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getHeader("Authorization")).thenReturn("Invalid testString");
        assertNull(utils.resolveToken(request));
    }

    @Test
    public void shouldReturnNullBecauseNoText() {
        HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getHeader("Authorization")).thenReturn("");
        assertNull(utils.resolveToken(request));
    }

    @Test
    public void shouldGetCurrentLoginSuccessfullyOnUserDetails() {
        Authentication authentication = Mockito.mock(Authentication.class);
        UserDetails details = mock(UserDetails.class);
        when(details.getUsername()).thenReturn("j.zamoyski@localhost");
        when(authentication.getPrincipal()).thenReturn(details);

        SecurityContext context = mock(SecurityContext.class);
        when(context.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(context);
        assertTrue(utils.getCurrentUserLogin().isPresent());
    }

    @Test
    public void shouldGetCurrentLoginSuccessfullyOnStringUsername() {
        Authentication authentication = Mockito.mock(Authentication.class);
        when(authentication.getPrincipal()).thenReturn("j.zamoyski@localhost");
        SecurityContext context = mock(SecurityContext.class);
        when(context.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(context);
        assertTrue(utils.getCurrentUserLogin().isPresent());
    }

    @Test
    public void shouldGetCurrentPersonSuccessfully() throws Exception{
        Authentication authentication = Mockito.mock(Authentication.class);
        when(authentication.getPrincipal()).thenReturn("j.zamoyski@localhost");
        SecurityContext context = mock(SecurityContext.class);
        when(context.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(context);
        when(personRepository.findByEmail(anyString())).thenReturn(Optional.of(new Person()));

        Person person = utils.getCurrentPerson();
        assertNotNull(person);
    }

    @Test
    public void shouldGetCurrentPatientSuccessfully() throws Exception{
        Authentication authentication = Mockito.mock(Authentication.class);
        when(authentication.getPrincipal()).thenReturn("j.zamoyski@localhost");
        SecurityContext context = mock(SecurityContext.class);
        when(context.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(context);
        when(patientRepository.findByEmail(anyString())).thenReturn(Optional.of(new Patient()));

        Patient patient = utils.getCurrentPatient();
        assertNotNull(patient);
    }

    @Test
    public void shouldGetCurrentUserJWT() throws Exception{
        Authentication authentication = Mockito.mock(Authentication.class);
        when(authentication.getCredentials()).thenReturn("token");
        SecurityContext context = mock(SecurityContext.class);
        when(context.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(context);

        assertTrue(utils.getCurrentUserJWT().isPresent());
    }
}
