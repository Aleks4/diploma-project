package pl.kielce.tu.diabeticstools.service;
import org.apache.commons.validator.routines.EmailValidator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import pl.kielce.tu.diabeticstools.dto.validation.EmailValidationDto;
import pl.kielce.tu.diabeticstools.exception.UnprocessableEntityException;
import pl.kielce.tu.diabeticstools.model.Person;
import pl.kielce.tu.diabeticstools.repository.PersonRepository;
import pl.kielce.tu.diabeticstools.security.SecurityUtils;
import pl.kielce.tu.diabeticstools.utils.ApplicationUtils;

import java.util.Optional;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class ValidationServiceTest {
    @Mock
    private PersonRepository personRepository;

    @Mock
    private ApplicationUtils appUtils;

    @Mock
    private SecurityUtils utils;

    @InjectMocks
    private ValidationService validationService;

    @Before
    public void startUp() {
        when(utils.getCurrentUserLogin()).thenReturn(Optional.of("test1@localhost"));
    }

    @Test
    public void shouldSuccessfullyVerify() throws Exception{
        when(personRepository.findByEmail(anyString())).thenReturn(Optional.empty());
        EmailValidationDto validationDto = new EmailValidationDto();
        validationDto.setEmail("test@onet.pl");
        EmailValidationDto result = validationService.validateEmail(validationDto);
        assertTrue(result.getIsCorrect());
        assertTrue(result.getIsFree());
    }

    @Test
    public void shouldReturnAlreadyTaken() throws Exception {
        when(personRepository.findByEmail(anyString())).thenReturn(Optional.of(new Person()));
        EmailValidationDto validationDto = new EmailValidationDto();
        validationDto.setEmail("test@onet.pl");
        EmailValidationDto result = validationService.validateEmail(validationDto);
        assertTrue(result.getIsCorrect());
        assertFalse(result.getIsFree());
    }

    @Test(expected = UnprocessableEntityException.class)
    public void shouldFailBecauseNull() throws Exception {
        EmailValidationDto validationDto = new EmailValidationDto();
        validationDto.setEmail(null);
        EmailValidationDto result = validationService.validateEmail(validationDto);
        verifyZeroInteractions(personRepository);
    }

    @Test(expected = UnprocessableEntityException.class)
    public void shouldFailBecauseEmpty() throws Exception {
        EmailValidationDto validationDto = new EmailValidationDto();
        validationDto.setEmail("");
        EmailValidationDto result = validationService.validateEmail(validationDto);
        verifyZeroInteractions(personRepository);
    }
}
