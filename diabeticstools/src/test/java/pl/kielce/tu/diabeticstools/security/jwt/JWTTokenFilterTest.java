package pl.kielce.tu.diabeticstools.security.jwt;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import pl.kielce.tu.diabeticstools.security.SecurityUtils;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class JWTTokenFilterTest {
    @Mock
    private JWTTokenProvider tokenProvider;

    @Mock
    private SecurityUtils utils;

    @InjectMocks
    private JWTTokenFilter filter;

    @Test
    public void shouldFilterSuccessfully() throws Exception{
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        FilterChain chain = mock(FilterChain.class);
        SecurityContext context = mock(SecurityContext.class);
        SecurityContextHolder.setContext(context);
        filter.doFilter(request, response, chain);
        verify(chain).doFilter(request, response);
    }
}
