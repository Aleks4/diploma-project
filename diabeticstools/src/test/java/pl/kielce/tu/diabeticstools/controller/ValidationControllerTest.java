package pl.kielce.tu.diabeticstools.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import pl.kielce.tu.diabeticstools.dto.validation.EmailValidationDto;
import pl.kielce.tu.diabeticstools.service.ValidationService;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class ValidationControllerTest {
    @Mock
    private ValidationService validationService;

    @InjectMocks
    private ValidationController controller;

    @Test
    public void shouldInvokeValidationServiceMethod() throws Exception {
        controller.isEmailAlreadyTaken(new EmailValidationDto());
        verify(validationService).validateEmail(any());
    }
}
