package pl.kielce.tu.diabeticstools.service;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import pl.kielce.tu.diabeticstools.config.ServerConfig;
import pl.kielce.tu.diabeticstools.dto.*;
import pl.kielce.tu.diabeticstools.model.Measurement;
import pl.kielce.tu.diabeticstools.model.Patient;
import pl.kielce.tu.diabeticstools.model.PatientSettings;
import pl.kielce.tu.diabeticstools.model.Role;
import pl.kielce.tu.diabeticstools.model.embeddable.BloodPressureValue;
import pl.kielce.tu.diabeticstools.model.embeddable.GlucoseValue;
import pl.kielce.tu.diabeticstools.model.type.ReportPeriod;
import pl.kielce.tu.diabeticstools.model.type.ReportType;
import pl.kielce.tu.diabeticstools.model.units.GlucoseUnit;
import pl.kielce.tu.diabeticstools.repository.MeasurementRepository;
import pl.kielce.tu.diabeticstools.repository.PatientRepository;
import pl.kielce.tu.diabeticstools.repository.PatientSettingsRepository;
import pl.kielce.tu.diabeticstools.utils.ApplicationUtils;
import pl.kielce.tu.diabeticstools.utils.converter.UnitUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class PatientServiceTest {

    @Mock
    private PatientSettingsRepository patientSettingsRepository;

    @Mock
    private MeasurementRepository measurementRepository;

    @Mock
    private PatientScheduleService patientScheduleService;

    @Mock
    private MeasurementHistoryService measurementHistoryService;

    @Spy
    private UnitUtils unitUtils = new UnitUtils();

    @Mock
    private PatientRepository patientRepository;
    @Mock
    private ApplicationUtils appUtils;

    @Mock
    private ServerConfig serverConfig;

    @InjectMocks
    private PatientService patientService;

    static Patient testPatient;

    static MeasurementRequestDto requestDto = new MeasurementRequestDto();
    static List<Measurement> measurements;

    @BeforeClass
    public static void setup() {
        testPatient = new Patient();
        testPatient.setId(1L);
        testPatient.setLocked(false);
        testPatient.setActive(true);
        testPatient.setRegistrationDate(new Timestamp(new Date().getTime()));
        testPatient.setRole(Role.ADMINISTRATOR);
        testPatient.setFirstname("Krzysztof");
        testPatient.setLastname("Nowak");
        testPatient.setEmail("j.zamoyski@localhost");
        testPatient.setRole(Role.PATIENT);
        PatientSettings patientSettings = new PatientSettings();
        patientSettings.setTimeWindow(15);
        patientSettings.setIncludeBloodPressure(true);
        patientSettings.setReportType(ReportType.PERIODIC_REPORT);
        patientSettings.setReportPeriod(ReportPeriod.MONTHLY);
        testPatient.setSettings(patientSettings);

        requestDto.setPagingInfo(new PagingInfo());
        requestDto.getPagingInfo().setPageNumber(1);
        requestDto.getPagingInfo().setPageSize(10);
        requestDto.setStartDate(LocalDate.now().minus(1, ChronoUnit.MONTHS));
        requestDto.setStopDate(LocalDate.now());
        requestDto.setDateScope(DateScope.MONTH);

        measurements = new ArrayList<>();
        measurements.add(new Measurement(
                1L,
                testPatient,
                new GlucoseValue(numberFromRange(69.0, 101.0), GlucoseUnit.mgPerDL),
                new BloodPressureValue(numberFromRange(89.0, 141.0), numberFromRange(49.0, 91.0), "mmHg"),
                true,
                Timestamp.valueOf(LocalDateTime.of(2020, 1, 24, 7, 45, 0)),
                null,
                null
        ));
        measurements.add(new Measurement(
                2L,
                testPatient,
                new GlucoseValue(numberFromRange(69.0, 101.0), GlucoseUnit.mgPerDL),
                new BloodPressureValue(numberFromRange(89.0, 141.0), numberFromRange(49.0, 91.0), "mmHg"),
                true,
                Timestamp.valueOf(LocalDateTime.of(2020, 1, 24, 7, 45, 0)),
                null,
                null
        ));

    }



    @Test
    public void shouldChangeSettingsSuccessfully() throws Exception{
        PatientSettingsDto newSettingsDto = new PatientSettingsDto(testPatient.getSettings());
        newSettingsDto.setIncludeBloodPressure(false);
        newSettingsDto.setReportPeriod(ReportPeriod.QUARTERLY);
        newSettingsDto.setReportType(ReportType.MANUAL_REPORT);
        newSettingsDto.setTimeWindow(30);
        patientService.changePatientSettings(testPatient, newSettingsDto);
        verify(patientSettingsRepository).save(any());
    }

    @Test
    public void shouldRetrievePatientBrief() throws Exception {
        Page<Measurement> measurementPage = new PageImpl<>(measurements, PageRequest.of(
                requestDto.getPagingInfo().getPageNumber(),
                requestDto.getPagingInfo().getPageSize()),
                measurements.size()
        );
        when(measurementRepository.findByPatientIdAndInsertionTimestampBetween(any(), any(), any(), any()))
                .thenReturn(measurementPage);
        PatientBriefData patientBriefData = patientService.getPatientBriefData(testPatient, requestDto);
        assertNotNull(patientBriefData);
    }

    @Test
    public void shouldGetServerVarsSuccessfully() throws Exception {
        when(serverConfig.getMin_glucose()).thenReturn(70.0);
        when(serverConfig.getMax_glucose()).thenReturn(99.9);
        ServerVariablesDto variablesDto = patientService.getServerVariables(testPatient);

        assertNotNull(variablesDto);
    }

    @Test
    public void shouldReturnConvertedVars() throws  Exception {
        when(serverConfig.getMin_glucose()).thenReturn(70.0);
        when(serverConfig.getMax_glucose()).thenReturn(99.9);
        Patient patient = new Patient();
        patient.setDefaultGlucoseUnit(GlucoseUnit.mmolPerL);
        ServerVariablesDto variablesDto = patientService.getServerVariables(patient);
        assertEquals(variablesDto.getMinGlucose(), BigDecimal.valueOf(70.0).divide(BigDecimal.valueOf(18.0), RoundingMode.HALF_UP));
        assertEquals(variablesDto.getMaxGlucose(), BigDecimal.valueOf(99.9).divide(BigDecimal.valueOf(18.0), RoundingMode.HALF_UP));
    }

    private static BigDecimal numberFromRange(Double min, Double max) {
        return BigDecimal.valueOf((Math.random()*((max-min)+1))+min);
    }
}
