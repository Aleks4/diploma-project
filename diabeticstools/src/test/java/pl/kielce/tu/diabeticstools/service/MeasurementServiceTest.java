package pl.kielce.tu.diabeticstools.service;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.jupiter.api.BeforeAll;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import pl.kielce.tu.diabeticstools.dto.BulkMeasurementDto;
import pl.kielce.tu.diabeticstools.dto.MeasurementDto;
import pl.kielce.tu.diabeticstools.exception.MissingBloodPressureException;
import pl.kielce.tu.diabeticstools.exception.UnprocessableEntityException;
import pl.kielce.tu.diabeticstools.model.Measurement;
import pl.kielce.tu.diabeticstools.model.Patient;
import pl.kielce.tu.diabeticstools.model.PatientSettings;
import pl.kielce.tu.diabeticstools.model.Role;
import pl.kielce.tu.diabeticstools.model.embeddable.BloodPressureValue;
import pl.kielce.tu.diabeticstools.model.embeddable.GlucoseValue;
import pl.kielce.tu.diabeticstools.model.type.ReportPeriod;
import pl.kielce.tu.diabeticstools.model.type.ReportType;
import pl.kielce.tu.diabeticstools.model.units.GlucoseUnit;
import pl.kielce.tu.diabeticstools.repository.MeasurementRepository;
import pl.kielce.tu.diabeticstools.utils.converter.UnitUtils;

import javax.persistence.EntityNotFoundException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class MeasurementServiceTest {

    @Mock
    private MeasurementRepository measurementRepository;

    @Mock
    private PatientScheduleService patientScheduleService;

    @Spy
    private UnitUtils unitUtils = new UnitUtils();

    @InjectMocks
    private MeasurementService measurementService;

    static Patient testPatient;

    static Measurement measurement;

    @BeforeClass
    public static void setup() {
        testPatient = new Patient();
        testPatient.setId(1L);
        testPatient.setLocked(false);
        testPatient.setActive(true);
        testPatient.setRegistrationDate(new Timestamp(new Date().getTime()));
        testPatient.setRole(Role.ADMINISTRATOR);
        testPatient.setFirstname("Krzysztof");
        testPatient.setLastname("Nowak");
        testPatient.setEmail("j.zamoyski@localhost");
        testPatient.setRole(Role.PATIENT);
        PatientSettings patientSettings = new PatientSettings();
        patientSettings.setTimeWindow(15);
        patientSettings.setIncludeBloodPressure(true);
        patientSettings.setReportType(ReportType.PERIODIC_REPORT);
        patientSettings.setReportPeriod(ReportPeriod.MONTHLY);
        testPatient.setSettings(patientSettings);

        measurement = new Measurement(
                1L,
                testPatient,
                new GlucoseValue(BigDecimal.valueOf(90),
                        GlucoseUnit.mgPerDL),
                new BloodPressureValue(BigDecimal.valueOf(120),
                        BigDecimal.valueOf(80),
                        "mmHg"),
                true,
                Timestamp.from(Instant.now()),
                null,
                null);
    }

    @Before
    public void startupAll() {
        when(measurementRepository.findByIdAndPatientId(anyLong(), anyLong()))
                .thenReturn(Optional.of(measurement));
    }

    @Test
    public void shouldAddSuccessfully() throws Exception{
        Measurement measurement = new Measurement(
                null,
                testPatient,
                new GlucoseValue(numberFromRange(69.0, 101.0), GlucoseUnit.mgPerDL),
                new BloodPressureValue(numberFromRange(89.0, 141.0), numberFromRange(49.0, 91.0), "mmHg"),
                true,
                Timestamp.valueOf(LocalDateTime.of(2020, 1, 24, 7, 45, 0)),
                null,
                null
        );

        measurementService.addNewMeasurement(testPatient, measurement);

        verify(measurementRepository).save(any());
    }

    @Test(expected = EntityNotFoundException.class)
    public void shouldRejectOnEmptyBody() throws  Exception {
        Measurement measurement = new Measurement(
                null,
                testPatient,
                new GlucoseValue(numberFromRange(69.0, 101.0), GlucoseUnit.mgPerDL),
                new BloodPressureValue(numberFromRange(89.0, 141.0), numberFromRange(49.0, 91.0), "mmHg"),
                true,
                Timestamp.valueOf(LocalDateTime.of(2020, 1, 24, 7, 45, 0)),
                null,
                null
        );
        measurementService.addNewMeasurement(testPatient, null);
    }

    @Test(expected = MissingBloodPressureException.class)
    public void shouldRejectOnLackOfBloodPressure() throws Exception{
        Measurement measurement = new Measurement(
                null,
                testPatient,
                new GlucoseValue(numberFromRange(69.0, 101.0), GlucoseUnit.mgPerDL),
                null,
                true,
                Timestamp.valueOf(LocalDateTime.of(2020, 1, 24, 7, 45, 0)),
                null,
                null
        );
        measurementService.addNewMeasurement(testPatient, measurement);
    }

    @Test
    public void shouldAddAllMeasurementsFromBulk() throws Exception{
        List<MeasurementDto> measurements = List.of(
            new MeasurementDto(
                    null,
                    BigDecimal.valueOf(90),
                    GlucoseUnit.mgPerDL,
                    BigDecimal.valueOf(120),
                    BigDecimal.valueOf(80),
                    "mmHg",
                    null,
                    LocalDate.now(),
                    LocalTime.now(),
                    null,
                    null,
                    true
            ),
                new MeasurementDto(
                        null,
                        BigDecimal.valueOf(90),
                        GlucoseUnit.mgPerDL,
                        BigDecimal.valueOf(120),
                        BigDecimal.valueOf(80),
                        "mmHg",
                        null,
                        LocalDate.now(),
                        LocalTime.now(),
                        null,
                        null,
                        true
                )
        );
        BulkMeasurementDto bulkMeasurementDto = new BulkMeasurementDto();
        bulkMeasurementDto.setMeasurements(measurements);
        measurementService.addBulkMeasurements(testPatient, bulkMeasurementDto);
        verify(measurementRepository, times(2)).save(any());
    }

    @Test
    public void shouldEditSuccessfully() throws Exception{
        MeasurementDto measurementDto = new MeasurementDto(
                1L,
                BigDecimal.valueOf(90),
                GlucoseUnit.mgPerDL,
                BigDecimal.valueOf(120),
                BigDecimal.valueOf(80),
                "mmHg",
                null,
                LocalDate.now(),
                LocalTime.now(),
                null,
                null,
                true
        );
        measurementService.editMeasurement(testPatient, measurementDto);
        verify(measurementRepository, times(1)).findByIdAndPatientId(anyLong(), anyLong());
    }

    @Test
    public void shouldDeleteSuccessfully() throws Exception{
        when(measurementRepository.findById(anyLong())).thenReturn(Optional.of(measurement));
        measurementService.deleteMeasurement(testPatient, 1L);
        verify(measurementRepository, times(1)).deleteById(1L);
    }

    @Test(expected = EntityNotFoundException.class)
    public void shouldRejectDeletionBecauseOfNotMatchingPatient() throws Exception {
        Patient wrongPatient = new Patient();
        wrongPatient.setId(13L);
        when(measurementRepository.findById(anyLong())).thenReturn(Optional.of(measurement));
        measurementService.deleteMeasurement(wrongPatient, 1L);
    }

    @Test(expected = UnprocessableEntityException.class)
    public void shouldRejectDeletionBecauseOfBeingNotEditable() throws Exception {
        measurement.setEditable(false);
        when(measurementRepository.findById(anyLong())).thenReturn(Optional.of(measurement));
        measurementService.deleteMeasurement(testPatient, 1L);
    }

    @Test
    public void shouldRetrieveOne() throws Exception {
        MeasurementDto measurementDto = measurementService.getMeasurementById(testPatient, 1L);
        assertNotNull(measurementDto);
    }

    @After
    public void tearDown() {
        measurement.setEditable(true);
    }

    private BigDecimal numberFromRange(Double min, Double max) {
        return BigDecimal.valueOf((Math.random()*((max-min)+1))+min);
    }
}
