package pl.kielce.tu.diabeticstools.service;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import static org.mockito.Mockito.*;
import static org.junit.Assert.*;
import static org.assertj.core.api.Assertions.assertThat;

import pl.kielce.tu.diabeticstools.dto.PatientScheduleDto;
import pl.kielce.tu.diabeticstools.exception.NoPermissionException;
import pl.kielce.tu.diabeticstools.exception.TimeWindowCollisionException;
import pl.kielce.tu.diabeticstools.model.*;
import pl.kielce.tu.diabeticstools.model.embeddable.BloodPressureValue;
import pl.kielce.tu.diabeticstools.model.embeddable.GlucoseValue;
import pl.kielce.tu.diabeticstools.model.type.ReportPeriod;
import pl.kielce.tu.diabeticstools.model.type.ReportType;
import pl.kielce.tu.diabeticstools.model.type.ScheduleType;
import pl.kielce.tu.diabeticstools.model.units.GlucoseUnit;
import pl.kielce.tu.diabeticstools.repository.MeasurementRepository;
import pl.kielce.tu.diabeticstools.repository.PatientScheduleRepository;

import javax.persistence.EntityNotFoundException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RunWith(MockitoJUnitRunner.class)
public class PatientScheduleServiceTest {

    @Mock
    public static PatientScheduleRepository patientScheduleRepository;

    @Mock
    public static MeasurementRepository measurementRepository;

    @InjectMocks
    PatientScheduleService patientScheduleService;

    static Patient testPatient;

    @Before
    public void setup() {
        testPatient = new Patient();
        testPatient.setId(1L);
        testPatient.setLocked(false);
        testPatient.setActive(true);
        testPatient.setRegistrationDate(new Timestamp(new Date().getTime()));
        testPatient.setRole(Role.ADMINISTRATOR);
        testPatient.setFirstname("Krzysztof");
        testPatient.setLastname("Nowak");
        testPatient.setEmail("j.zamoyski@localhost");
        testPatient.setRole(Role.PATIENT);
        PatientSettings patientSettings = new PatientSettings();
        patientSettings.setTimeWindow(15);
        patientSettings.setIncludeBloodPressure(true);
        patientSettings.setReportType(ReportType.PERIODIC_REPORT);
        patientSettings.setReportPeriod(ReportPeriod.MONTHLY);
        testPatient.setSettings(patientSettings);
    }

    @Before
    public void setupEach() {
        ArrayList<PatientSchedule> schedules = new ArrayList<>();
        schedules.add(new PatientSchedule(1L, testPatient, LocalTime.of(8, 0), "Na czczo", ScheduleType.MEASUREMENT, 15, true));
        schedules.add(new PatientSchedule(2L, testPatient, LocalTime.of(14, 0), "Przed obiadem", ScheduleType.MEASUREMENT, 15, true));
        schedules.add(new PatientSchedule(3L, testPatient, LocalTime.of(20, 0), "Przed kolacją", ScheduleType.MEASUREMENT, 15, true));
        when(patientScheduleRepository.findByPatient(any())).thenReturn(schedules);
    }

    @Test
    public void shouldAssignSchedule() {
        Measurement measurement = new Measurement(
                null,
                testPatient,
                new GlucoseValue(numberFromRange(69.0, 101.0), GlucoseUnit.mgPerDL),
                new BloodPressureValue(numberFromRange(89.0, 141.0), numberFromRange(49.0, 91.0), "mmHg"),
                true,
                Timestamp.valueOf(LocalDateTime.of(2020, 1, 24, 7, 45)),
                null,
                null
        );

        PatientSchedule schedule = patientScheduleService.getMeasurementSchedule(measurement);
        assertNotNull(schedule);
    }

    @Test
    public void shouldNotAssignAnything() {
        Measurement measurement = new Measurement(
                null,
                testPatient,
                new GlucoseValue(numberFromRange(69.0, 101.0), GlucoseUnit.mgPerDL),
                new BloodPressureValue(numberFromRange(89.0, 141.0), numberFromRange(49.0, 91.0), "mmHg"),
                true,
                Timestamp.valueOf(LocalDateTime.of(LocalDate.now(), LocalTime.of(7, 44, 59))),
                null,
                null
        );
        PatientSchedule schedule = patientScheduleService.getMeasurementSchedule(measurement);
        assertNull(schedule);
    }

    @Test
    public void shouldAssignAtLowerTimeLimit() {
        Measurement measurement = new Measurement(
                null,
                testPatient,
                new GlucoseValue(numberFromRange(69.0, 101.0), GlucoseUnit.mgPerDL),
                new BloodPressureValue(numberFromRange(89.0, 141.0), numberFromRange(49.0, 91.0), "mmHg"),
                true,
                Timestamp.valueOf(LocalDateTime.of(LocalDate.now(), LocalTime.of(7, 45, 0))),
                null,
                null
        );
        PatientSchedule schedule = patientScheduleService.getMeasurementSchedule(measurement);
        assertNotNull(schedule);
    }

    @Test
    public void shouldAssignAtUpperTimeLimit() {
        Measurement measurement = new Measurement(
                null,
                testPatient,
                new GlucoseValue(numberFromRange(69.0, 101.0), GlucoseUnit.mgPerDL),
                new BloodPressureValue(numberFromRange(89.0, 141.0), numberFromRange(49.0, 91.0), "mmHg"),
                true,
                Timestamp.valueOf(LocalDateTime.of(2020, 1, 24, 8, 15, 0)),
                null,
                null
        );
        PatientSchedule schedule = patientScheduleService.getMeasurementSchedule(measurement);
        assertNotNull(schedule);

    }

    @Test
    public void shouldNotAssignWhenScheduleForThatDayIsDone() {
        when(measurementRepository.findByPatientAndPatientSchedule(any(), any())).thenReturn(
                List.of(
                        new Measurement(
                                null,
                                testPatient,
                                new GlucoseValue(numberFromRange(69.0, 101.0), GlucoseUnit.mgPerDL),
                                new BloodPressureValue(numberFromRange(89.0, 141.0), numberFromRange(49.0, 91.0), "mmHg"),
                                true,
                                Timestamp.valueOf(LocalDateTime.of(LocalDate.now(), LocalTime.of(8, 15, 0))),
                                null,
                                null
                        )
                )
        );

        Measurement measurement = new Measurement(
                null,
                testPatient,
                new GlucoseValue(numberFromRange(69.0, 101.0), GlucoseUnit.mgPerDL),
                new BloodPressureValue(numberFromRange(89.0, 141.0), numberFromRange(49.0, 91.0), "mmHg"),
                true,
                Timestamp.valueOf(LocalDateTime.of(2020, 1, 24, 8, 0, 0)),
                null,
                null
        );
        PatientSchedule schedule = patientScheduleService.getMeasurementSchedule(measurement);
        assertNull(schedule);
    }

    @Test(expected = TimeWindowCollisionException.class)
    public void shouldRaiseExceptionWhenAddingBecauseOfCollision() throws Exception {
        PatientSchedule collidingSchedule = new PatientSchedule(null, testPatient, LocalTime.of(8, 0), "Na czczo", ScheduleType.MEASUREMENT, 15, true);
        patientScheduleService.addNewSchedule(testPatient, collidingSchedule);
    }

    @Test
    public void shouldAddBelowLowerBound() throws Exception {
        PatientSchedule collidingSchedule = new PatientSchedule(null, testPatient, LocalTime.of(7, 29), "Na czczo", ScheduleType.MEASUREMENT, 15, true);
        patientScheduleService.addNewSchedule(testPatient, collidingSchedule);
    }
    @Test(expected = TimeWindowCollisionException.class)
    public void shouldRaiseExceptionWhenAddingBecauseOfCollisionOnLowerBound() throws Exception {
        PatientSchedule collidingSchedule = new PatientSchedule(null, testPatient, LocalTime.of(7, 30), "Na czczo", ScheduleType.MEASUREMENT, 15, true);
        patientScheduleService.addNewSchedule(testPatient, collidingSchedule);
    }

    @Test
    public void shouldAddBelowUpperBound() throws Exception {
        PatientSchedule collidingSchedule = new PatientSchedule(null, testPatient, LocalTime.of(8, 31), "Na czczo", ScheduleType.MEASUREMENT, 15, true);
        patientScheduleService.addNewSchedule(testPatient, collidingSchedule);
    }
    @Test(expected = TimeWindowCollisionException.class)
    public void shouldRaiseExceptionWhenAddingBecauseOfCollisionOnUpperBound() throws Exception {
        PatientSchedule collidingSchedule = new PatientSchedule(null, testPatient, LocalTime.of(8, 30), "Na czczo", ScheduleType.MEASUREMENT, 15, true);
        patientScheduleService.addNewSchedule(testPatient, collidingSchedule);
    }

    @Test
    public void shouldRetrieveOneItem() throws Exception{
        when(patientScheduleRepository.findByPatientAndId(any(), any())).thenReturn(
                Optional.of(new PatientSchedule(1L, testPatient, LocalTime.of(8, 0), "Na czczo", ScheduleType.MEASUREMENT, 15, true))
        );
        assertNotNull(patientScheduleService.getScheduleById(testPatient, 1L));
    }

    @Test
    public void shouldRetrieveList() {
        List<PatientScheduleDto> scheduleDtos = patientScheduleService.getScheduleList(testPatient);
        assertNotNull(scheduleDtos);
    }

    @Test
    public void shouldDeleteSchedule() throws Exception{
        PatientSchedule schedule = new PatientSchedule(1L, testPatient, LocalTime.of(8, 0), "Na czczo", ScheduleType.MEASUREMENT, 15, true);
        when(patientScheduleRepository.findById(anyLong())).thenReturn(Optional.of(schedule));
        patientScheduleService.deleteSchedule(testPatient, schedule.getId());
        verify(patientScheduleRepository).delete(schedule);
    }

    @Test(expected = EntityNotFoundException.class)
    public void shouldNotDeleteBecauseOfNoSchedule() throws Exception {
        when(patientScheduleRepository.findById(anyLong())).thenReturn(Optional.empty());
        patientScheduleService.deleteSchedule(testPatient, anyLong());
    }

    @Test(expected = EntityNotFoundException.class)
    public void shouldNotDeleteScheduleBecauseInactive() throws Exception{
        PatientSchedule schedule = new PatientSchedule(1L, testPatient, LocalTime.of(8, 0), "Na czczo", ScheduleType.MEASUREMENT, 15, false);
        when(patientScheduleRepository.findById(anyLong())).thenReturn(Optional.of(schedule));
        patientScheduleService.deleteSchedule(testPatient, schedule.getId());
        verify(patientScheduleRepository).delete(schedule);
    }

    @Test(expected = NoPermissionException.class)
    public void shouldNotDeleteScheduleBecauseInvalidPerson() throws Exception{
        Patient invalidPatient = new Patient();
        invalidPatient.setId(2L);

        PatientSchedule schedule = new PatientSchedule(1L, testPatient, LocalTime.of(8, 0), "Na czczo", ScheduleType.MEASUREMENT, 15, true);
        when(patientScheduleRepository.findById(anyLong())).thenReturn(Optional.of(schedule));
        patientScheduleService.deleteSchedule(invalidPatient, schedule.getId());
    }

    @Test
    public void shouldGetCurrentlyOpenedSchedule() throws Exception{
        when(patientScheduleRepository.findByPatient(testPatient)).thenReturn(
                List.of(
                        new PatientSchedule(1L, testPatient, LocalTime.now(), "Na czczo", ScheduleType.MEASUREMENT, 15, true),
                        new PatientSchedule(2L, testPatient, LocalTime.now().plus(1, ChronoUnit.HOURS), "Na czczo", ScheduleType.MEASUREMENT, 15, true)
                )
        );

        List<PatientSchedule> openedSchedules = patientScheduleService.getCurrentlyOpenedSchedulesForPatient(testPatient);
        assertEquals(1, openedSchedules.size());
    }

    @Test
    public void shouldGetNextScheduleOtherThanOpenedOne() throws Exception{
        List<PatientSchedule> schedules = List.of(
                new PatientSchedule(1L, testPatient, LocalTime.now(), "Na czczo", ScheduleType.MEASUREMENT, 15, true),
                new PatientSchedule(2L, testPatient, LocalTime.now().plus(1, ChronoUnit.HOURS), "Na czczo", ScheduleType.MEASUREMENT, 15, true)
        );
        when(patientScheduleRepository.findByPatient(testPatient)).thenReturn(schedules);
        when(patientScheduleRepository.findByPatientAndTimeAfter(any(), any())).thenReturn(schedules);

        PatientSchedule nextSchedule = patientScheduleService.getNextSchedule(testPatient);
        assertEquals(Long.valueOf(2), nextSchedule.getId());
    }

    private BigDecimal numberFromRange(Double min, Double max) {
        return BigDecimal.valueOf((Math.random()*((max-min)+1))+min);
    }
}
