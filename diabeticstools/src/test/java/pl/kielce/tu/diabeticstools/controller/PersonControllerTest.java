package pl.kielce.tu.diabeticstools.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import pl.kielce.tu.diabeticstools.dto.UserProfileDto;
import pl.kielce.tu.diabeticstools.security.SecurityUtils;
import pl.kielce.tu.diabeticstools.service.PersonService;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class PersonControllerTest {
    @Mock
    private PersonService personService;
    @Mock
    private SecurityUtils utils;

    @InjectMocks
    private PersonController controller;

    @Test
    public void shouldInvokeGetProfileServiceMethod() throws Exception{
        controller.getUserProfile();
        verify(personService).getUserProfile(any());
    }

    @Test
    public void shouldInvokeChangeProfileServiceMethod() throws Exception {
        controller.changeUserProfile(new UserProfileDto());
        verify(personService).changeUserProfile(any(), any());
    }

    @Test
    public void shouldInvokePersonBriefMethod() throws Exception{
        controller.getUserBriefData();
        verify(personService).getPersonBrief();
    }
}
