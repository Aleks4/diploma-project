package pl.kielce.tu.diabeticstools.utils.converter;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import pl.kielce.tu.diabeticstools.model.embeddable.GlucoseValue;
import pl.kielce.tu.diabeticstools.model.units.GlucoseUnit;

import java.math.BigDecimal;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class UnitUtilsTest {

    private UnitUtils unitUtils = new UnitUtils();

    @Test
    public void shouldCorrectlyConvertMgPerDLToMmolPerL() throws Exception{
        GlucoseValue value = new GlucoseValue(BigDecimal.ONE, GlucoseUnit.mmolPerL);
        GlucoseValue expectedValue = new GlucoseValue(BigDecimal.valueOf(18), GlucoseUnit.mgPerDL);

        GlucoseValue actualValue = (GlucoseValue) unitUtils.convert(value, GlucoseUnit.mgPerDL);
        assertEquals(expectedValue, actualValue);
    }
}
