import {PatientSchedule} from "../dto/patient-schedule";
import {LocalNotification, LocalNotificationId} from "expo/build/Notifications/Notifications.types";
import moment from "moment";
import {Notifications} from "expo";
import {AsyncStorage} from "react-native";

export class NotificationService {

    async refreshNotifications() {
        await Notifications.cancelAllScheduledNotificationsAsync();

        let schedules: PatientSchedule[] = JSON.parse(await AsyncStorage.getItem('patient_schedules'));
        for(const item of schedules) {
            await this.registerScheduleAsNotification(item);
        }
        console.log('Notifications refreshed');
    }

    async registerScheduleAsNotification(schedule: PatientSchedule): Promise<string> {
        let time = this.getEffectiveTime(schedule);
        return await Notifications.scheduleNotificationWithCalendarAsync(this.getNotificationBody(schedule), {
            hour: time.hour(),
            minute: time.minute(),
            second: 0,
            repeat: true
        });
    }

    getNotificationBody(schedule: PatientSchedule): LocalNotification {
        return schedule.type==='MEASUREMENT'?{
            title: 'Przypomnienie o pomiarze',
            body: schedule.time +' '+ schedule.name,
            android: {
                channelId: 'diabetes_assistant'
            },
            ios: {
                sound: true
            }

        }:{
            title:'Inne przypomnienie',
            body: schedule.time +' '+ schedule.name,
            android: {
                channelId: 'diabetes_assistant'
            },
            ios: {
                sound: true
            }
        };
    }

    getEffectiveTime(schedule: PatientSchedule) {
        let nominalTime = moment(schedule.time, 'HH:mm');
        nominalTime.seconds(0);
        if(schedule.type==='MEASUREMENT') {
            nominalTime = nominalTime.subtract(schedule.time_window, 'minutes');
        }
        return nominalTime;
    }
}
