import {NotificationService} from "./notification-service";
import {SERVER_API_URL} from "../app.constants";
import {AsyncStorage} from "react-native";
import {PatientSchedule} from "../dto/patient-schedule";
import {MeasurementRequestDto} from "../dto/measurement-request-dto";
import {Measurement} from "../dto/measurement";

export class DataLoader {
    private notificationService: NotificationService = new NotificationService();

    constructor() {

    }

    resolveRequiredData = async () => {
        let token = await AsyncStorage.getItem('id_token');
        let briefResponse = await fetch(SERVER_API_URL+'/api/person/profile/brief', {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer '+ token
            }
        });

        let briefResponseJson = await briefResponse.json();
        await AsyncStorage.setItem('default_glucose_unit', briefResponseJson.default_glucose_unit);
        await AsyncStorage.setItem('includeBloodPressure', briefResponseJson.patient_settings.includeBloodPressure.toString());
        await AsyncStorage.setItem('min_glucose', briefResponseJson.min_glucose.toString());
        await AsyncStorage.setItem('max_glucose', briefResponseJson.max_glucose.toString());
        await AsyncStorage.setItem('patient_settings', JSON.stringify(briefResponseJson.patient_settings));
        await this.initPendingCache;
    };

    initPendingCache = async() => {
        let existingPendingMeasurements = await AsyncStorage.getItem('measurements_pending');
        if(existingPendingMeasurements === null) {
            await AsyncStorage.setItem('measurements_pending', JSON.stringify([]));
        }
    };

    fetchProfile = async() => {
        try {
            let response = await fetch(SERVER_API_URL+'/api/person/profile', {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Authorization': 'Bearer '+ await AsyncStorage.getItem('id_token')
                },
            });
            let responseJson = await response.json();
            await AsyncStorage.setItem('user_profile', JSON.stringify(responseJson));
        }catch(error) {
            console.log(error);
        }
    };
    fetchSchedules = async () => {
        let response = await fetch(SERVER_API_URL+'/api/patient/schedule/list', {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer '+ await AsyncStorage.getItem('id_token')
            }
        });
        let responseJson: PatientSchedule[] = await response.json();

        if(response.status===200) {
            await AsyncStorage.setItem('patient_schedules', JSON.stringify(responseJson));
            await this.notificationService.refreshNotifications();
        }
    };
    fetchMeasurements = async (requestDto?: MeasurementRequestDto) => {
        let response = await fetch(SERVER_API_URL + '/api/measurement/history/range/latest', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer '+ await AsyncStorage.getItem('id_token')
            },
            body: JSON.stringify({
                date_scope: 'WEEK',
                paging_info: {
                    page_number: requestDto?requestDto.paging_info.page_number:0,
                    page_size: requestDto?requestDto.paging_info.page_size:7
                }
            })
        });
        let responseJson = await response.json();
        await AsyncStorage.setItem('measurements_cache', JSON.stringify(responseJson));
    };

    bulkInsertMeasurement = async(measurements: Measurement[]) => {
        let bulkMeasurement = {
            abandonOnError: true,
            measurements: measurements,
        };
        try {
            let response = await fetch(SERVER_API_URL+'/api/measurement/new/bulk', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer '+ await AsyncStorage.getItem('id_token')
                },
                body: JSON.stringify(bulkMeasurement)
            })
        }catch(error) {

        }
    };
}
