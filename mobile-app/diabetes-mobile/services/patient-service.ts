import {Credentials} from "../dto/credentials";
import {SERVER_API_URL} from "../app.constants";
import {NotificationService} from "./notification-service";
import {AsyncStorage} from "react-native";

export class PatientService {
    private notificationService: NotificationService = new NotificationService();

    isLoggedIn = async (): Promise<boolean> => {
        return (!!(await AsyncStorage.getItem('id_token')));
    }
}
