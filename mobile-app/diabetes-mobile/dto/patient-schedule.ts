export class PatientSchedule {
  id: number;
  time: string;
  name: string;
  type: string;
  time_window: number;
}
