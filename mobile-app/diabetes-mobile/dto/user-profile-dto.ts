export class UserProfileDto {
  email: string;
  firstname: string;
  lastname: string;
  phoneNumber: string;
  registrationDate: number;
  default_glucose_unit: string;
}
