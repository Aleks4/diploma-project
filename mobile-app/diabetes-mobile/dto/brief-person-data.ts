import {PatientSettings} from "./patient-settings";

export class BriefPersonData {
  firstname: string;
  lastname: string;
  role: string;
  patient_settings: PatientSettings;
  default_glucose_unit: string;
  current_date: string;
  min_glucose: number;
  max_glucose: number;
}
