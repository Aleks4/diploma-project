import {PagingInfoDto} from "./paging-info-dto";

export class MeasurementRequestDto {
  date_scope: string;
  paging_info: PagingInfoDto;
  start_date: string;
  stop_date: string;

  constructor(date_scope: string, paging_info: PagingInfoDto) {
    this.date_scope = date_scope;
    this.paging_info = paging_info;
  }
}
