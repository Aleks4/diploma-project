import {UnitValue} from "./unit-value";
import {BloodPressureValue} from "./blood-pressure-value";
import {PatientSchedule} from "./patient-schedule";

export class Measurement {
  id: number;
  glucose_value: number;
  glucose_unit: string;
  blood_pressure_upper: number;
  blood_pressure_lower: number;
  blood_pressure_unit: string = "mmHg";
  insertion_timestamp: string;
  patient_comment: string;
  patient_schedule: PatientSchedule;
  editable: boolean;
  date: string;
  time: string;
}
