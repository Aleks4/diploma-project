import {PatientSchedule} from "./patient-schedule";
import {MeasurementListDto} from "./measurement-list-dto";

export class PatientBriefData {
  active_schedules: PatientSchedule[];
  next_schedule: PatientSchedule;
  measurements_today: MeasurementListDto;
}
