export class PagingInfoDto {
  total_item_count: number;
  total_page_count: number;
  page_number: number;
  page_size: number;

  constructor(page_number: number, page_size: number) {
    this.page_number = page_number;
    this.page_size = page_size;
  }
}
