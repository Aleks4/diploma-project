import {Component} from "react";
import {AsyncStorage, StyleSheet, Text, TextInput, TouchableOpacity, View} from "react-native";
import * as React from "react";
import {SERVER_API_URL} from "../app.constants";
import {Formik} from 'formik';
import * as yup from 'yup';
import {Measurement} from "../dto/measurement";
import {DataLoader} from "../services/data-loader";
import FontAwesome5Icon from "react-native-vector-icons/FontAwesome5";
import FontAwesomeIcon from "react-native-vector-icons/FontAwesome";

export class MeasurementEditComponent extends Component {

    state = {
        measurement: null,
        glucose_value: null,
        glucose_unit: null,
        blood_pressure_upper: null,
        blood_pressure_lower: null,
        blood_pressure_unit: "mmHg",
        patient_comment: null,
        measurementId: null,
        default_glucose_unit: null,
        includeBloodPressure: false,

        min_glucose: 70,
        max_glucose: 99.9,
        editSuccess: false,
        editFailure: false,

        dataLoader: new DataLoader()
    };

    componentDidMount(): void {
        //@ts-ignore
        this.setState({measurementId: this.props.navigation.getParam('id')});
        this.fetchSettingsFromStorage();
        //@ts-ignore
        this.fetchMeasurement(this.props.navigation.getParam('id'));
    }
    fetchSettingsFromStorage = async () => {
        this.setState({
            default_glucose_unit: await AsyncStorage.getItem('default_glucose_unit'),
            includeBloodPressure: (await AsyncStorage.getItem('includeBloodPressure')) === "true",
            min_glucose: parseFloat(await AsyncStorage.getItem('min_glucose')),
            max_glucose: parseFloat(await AsyncStorage.getItem('max_glucose')),
        })
    };

    validationSchemaMeasurement = yup.object().shape({
        glucose_value: yup.number()
            .required('Pole wymagane')
            .typeError('Należy podać liczbę')
            .positive('Wartość musi być dodatnia.')
    });

    render() {
        const postMeasurementSuccess =
            <Text style={styles.alertSuccess}>
                Pomiar Edytowany pomyślnie.
            </Text>;
        const postMeasurementFailure =
            <Text style={styles.alertDanger}>
                Nie udało się edytować pomiaru.
            </Text>;

        return(
            <Formik
                initialValues={{
                    //@ts-ignore
                    glucose_value: parseFloat(this.props.navigation.getParam('glucose_value')),
                    //@ts-ignore
                    blood_pressure_upper: parseFloat(this.props.navigation.getParam('blood_pressure_upper')),
                    //@ts-ignore
                    blood_pressure_lower: parseFloat(this.props.navigation.getParam('blood_pressure_lower')),
                    //@ts-ignore
                    patient_comment: this.props.navigation.getParam('patient_comment')
                }}
                onSubmit={(values) => {
                    this.setState({
                       glucose_value: values.glucose_value,
                       blood_pressure_upper: values.blood_pressure_upper,
                       blood_pressure_lower: values.blood_pressure_lower
                    });
                    this.handleMeasurementEdit();
                }}
                validationSchema={this.validationSchemaMeasurement}
            >
                {formikProps => (
                    <View>
                        {this.state.editSuccess === true ? postMeasurementSuccess : null}
                        {this.state.editFailure === true ? postMeasurementFailure : null}
                        <Text style={styles.text}>Poziom glukozy</Text>
                        <View style={styles.inputBundle}>
                            <TextInput style={styles.input}
                                       onChangeText={(text) => {
                                           this.setState({glucose_value: parseFloat(text)});
                                           formikProps.setFieldValue('glucose_value', text)
                                       }}
                                       keyboardType={'phone-pad'}
                                       //@ts-ignore
                                       defaultValue={this.props.navigation.getParam('glucose_value').toString()}
                            />

                            <Text style={styles.text}>{this.state.default_glucose_unit ? MeasurementEditComponent.simpleUnitTranslator(this.state.default_glucose_unit) : null}</Text>
                        </View>
                        <Text style={{color: 'red'}}>{formikProps.errors.glucose_value}</Text>
                        {this.state.includeBloodPressure && this.state.includeBloodPressure === true ? (
                            <View>
                                <Text style={styles.text}>Ciśnienie krwi</Text>
                                <View style={styles.inputBundle}>
                                    <TextInput style={styles.input}
                                               onChangeText={(text) => {
                                                   this.setState({blood_pressure_upper: parseFloat(text)});
                                                   formikProps.setFieldValue('blood_pressure_upper', text)

                                               }}
                                               keyboardType={'phone-pad'}
                                               placeholder={'Skurczowe'}
                                        //@ts-ignore
                                               defaultValue={this.props.navigation.getParam('blood_pressure_upper').toString()}
                                    />
                                    <Text style={styles.text}>mmHg</Text>
                                </View>
                                <Text style={{color: 'red'}}>{formikProps.errors.blood_pressure_upper}</Text>
                                <View style={styles.inputBundle}>
                                    <TextInput style={styles.input}
                                               onChangeText={(text) => {
                                                   this.setState({blood_pressure_lower: parseFloat(text)});
                                                   formikProps.setFieldValue('blood_pressure_lower', text)
                                               }}
                                               keyboardType={'phone-pad'}
                                               placeholder={'Rozkurczowe'}
                                               //@ts-ignore
                                               defaultValue={this.props.navigation.getParam('blood_pressure_lower').toString()}
                                    />
                                    <Text style={styles.text}>mmHg</Text>
                                </View>
                                <Text style={{color: 'red'}}>{formikProps.errors.blood_pressure_lower}</Text>
                            </View>) : null}
                        {!(this.state.glucose_value>=this.state.min_glucose && this.state.glucose_value<=this.state.max_glucose)?
                            <View style={styles.alertDanger}>
                                <Text style={styles.textSmall}>Nieprawidłowy poziom glukozy!</Text>
                                <Text style={styles.textSmall}>Opisz przyczynę(np. słodkie potrawy)</Text>
                                <TextInput
                                    style={styles.inputText}
                                    onChangeText={(text) => {
                                        this.setState({patient_comment: text});
                                        formikProps.setFieldValue('patient_comment', text);
                                    }}
                                    //@ts-ignore
                                    defaultValue={this.props.navigation.getParam('patient_comment')}
                                />
                            </View>:null}
                        <TouchableOpacity
                            style={[styles.buttonBundle, styles.buttonBundleBlue]}
                            //@ts-ignore
                            onPress={formikProps.handleSubmit}
                        >
                            <FontAwesomeIcon name={'save'} size={33}
                                             style=
                                                 {[styles.buttonIcon, {paddingTop: 6, paddingLeft: 9, paddingRight: 9, paddingBottom: 6}]}/>
                            <Text style={styles.buttonText}>        Zapisz</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={[styles.buttonBundle, styles.buttonBundleBlue]}
                            onPress={() => {
                                // @ts-ignore
                                this.props.navigation.navigate('Dashboard')
                            }}
                        >
                            <FontAwesome5Icon
                                name={'sign-out-alt'}
                                size={33}
                                style=
                                    {[styles.buttonIcon, {paddingTop: 6, paddingLeft: 8, paddingRight: 8, paddingBottom: 6}]}
                            />
                            <Text style={styles.buttonText}> Wróć do menu</Text>
                        </TouchableOpacity>
                    </View>)}
            </Formik>
        );
    }

    static simpleUnitTranslator(unit: string): string {
        if (unit === 'mgPerDL') return 'mg/dL';
        else if (unit === 'mmolPerL') return 'mmol/L';
        else return '';
    }

    handleMeasurementEdit = (ev?) => {
        let measurement: Measurement = new Measurement();
        measurement.id = this.state.measurementId;
        measurement.glucose_value = this.state.glucose_value;
        measurement.glucose_unit = this.state.default_glucose_unit;
        measurement.blood_pressure_upper = this.state.blood_pressure_upper;
        measurement.blood_pressure_lower = this.state.blood_pressure_lower;
        measurement.blood_pressure_unit = this.state.blood_pressure_unit;
        measurement.patient_comment = this.state.patient_comment;
        this.editMeasurement(measurement);
    };

    editMeasurement = async (measurement: Measurement) => {
        try {
            let response = await fetch(SERVER_API_URL + '/api/measurement/edit', {
                method: 'PUT',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + await AsyncStorage.getItem('id_token')
                },
                body: JSON.stringify({
                    id: measurement.id,
                    glucose_value: measurement.glucose_value,
                    glucose_unit: measurement.glucose_unit,
                    blood_pressure_upper: measurement.blood_pressure_upper,
                    blood_pressure_lower: measurement.blood_pressure_lower,
                    blood_pressure_unit: measurement.blood_pressure_unit,
                    patient_comment: measurement.patient_comment
                })
            });
            if (response.status === 200) {
                this.setState({
                    editSuccess: true,
                    handleIncorrectGlucose: false
                });
                await this.state.dataLoader.fetchMeasurements();
                setTimeout(() => {
                    //@ts-ignore
                    this.props.navigation.navigate('Dashboard')
                }, 3000);
            }
            else {
                this.setState({
                    editFailure: true
                });
                setTimeout(() => {this.setState({editFailure: false})}, 3000);
            }
        } catch (error) {
            console.log(error);

        }
    };

    fetchMeasurement = async (id: number) => {
        let response = await fetch(SERVER_API_URL+'/api/measurement/'+id, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer '+ await AsyncStorage.getItem('id_token')
            },
        });
        let responseJson = await response.json();
        this.setState({measurement: responseJson});
    }
}
const styles = StyleSheet.create({
    input: {
        backgroundColor: '#FFFFFF',
        borderRadius: 10,
        fontSize: 25,
        width: 220,
        padding: 5,
        textAlign: 'center',
        margin: 5
    },
    inputText: {
        backgroundColor: '#FFFFFF',
        borderRadius: 10,
        fontSize: 25,
        width: 320,
        padding: 5,
        textAlign: 'center',
        margin: 5
    },
    text: {
        fontSize: 28,
        textAlign: 'center'
    },
    textSmall: {
        fontSize: 18,
    },
    button: {
        width: 300,
        margin: 10,
        borderRadius: 20
    },
    inputBundle: {
        flexDirection: 'row'
    },
    alertDanger: {
        backgroundColor: '#ff9e8d',
        fontSize: 18,
        borderRadius: 10,
        textAlign: 'center'
    },
    alertSuccess: {
        backgroundColor: '#71ff92',
        fontSize: 18,
        borderRadius: 10,
        textAlign: 'center'
    },
    buttonBundle: {
        flexDirection: "row",
        width: 300,
        alignSelf: "center",
        alignContent: "center",
        borderRadius: 50,
        margin: 5
    },
    buttonBundleGreen: {
        backgroundColor: "#4dff5e",
    },
    buttonBundleBlue: {
        backgroundColor: "#4685ff"
    },
    buttonText: {
        fontSize: 32,
        justifyContent: "space-between",
        alignSelf: "stretch"
    },
    buttonIcon: {
        backgroundColor: "#FFFFFF",
        borderRadius: 25,
    }
});
