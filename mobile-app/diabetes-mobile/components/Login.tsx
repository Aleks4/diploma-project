import {Component} from "react";
import {
    Button,
    NativeSyntheticEvent,
    NativeTouchEvent,
    Text,
    TextInput,
    View,
    StyleSheet, Switch, Keyboard, AsyncStorage
} from "react-native";
import * as React from "react";
import {Credentials} from "../dto/credentials";
import {SERVER_API_URL} from "../app.constants";
import {PatientSchedule} from "../dto/patient-schedule";
import {NotificationService} from "../services/notification-service";
import {DataLoader} from "../services/data-loader";
import {ValidationComponent} from "react-native-form-validator";

import {Formik} from 'formik';
import * as yup from 'yup';

export class LoginComponent extends Component {

    constructor(props) {
        super(props);
    }

    state = {
        email: '',
        password: '',
        rememberMe: false,
        authSuccess: false,
        authFailure: false,
        serverFailure: false,
        serverConnectionError: false,
        notificationService: new NotificationService(),
        dataLoader: new DataLoader(),

        date_scope: 'WEEK',
        page_number: 0,
        page_size: 7,
    };

    validationSchemaLogin = yup.object().shape({
        email: yup.string()
            .label('Email')
            .required('Adres e-mail jest wymagany'),
        password: yup.string()
            .label('Hasło')
            .required('Hasło jest wymagane')
    });

    myFunc = (ev?: NativeSyntheticEvent<NativeTouchEvent>) => {
        let credentials: Credentials = new Credentials();
        credentials.email = this.state.email;
        credentials.password = this.state.password;
        credentials.rememberMe = this.state.rememberMe;
        this.login(credentials);
    };

    componentDidMount(): void {

    }

    render() {
        const authFailureMessage = <Text style={styles.alertDanger}>Invalid credentials</Text>;
        const serverErrorMessage = <Text style={styles.alertDanger}>Internal server error</Text>;
        const serverConnectionErrorMessage = <Text style={styles.alertDanger}>Server connection error</Text>;
        const invalidEmail = <Text style={[]}>Nieprawidłowy email</Text>;
        // @ts-ignore
        const { navigate } = this.props.navigation;
        return (
            <Formik initialValues={{
                email: this.state.email,
                password: this.state.password,
            }} onSubmit={values => {
                this.setState({
                    email: values.email,
                    password: values.password
                });
                this.myFunc();
            }}
                    validationSchema={this.validationSchemaLogin}
            >
                {formikProps => (<View style={styles.loginPage}>
                    <View>
                        {this.state.authFailure===true?authFailureMessage:null}
                        {this.state.serverFailure===true?serverErrorMessage:null}
                        {this.state.serverConnectionError===true?serverConnectionErrorMessage:null}
                        <Text style={styles.text}>Twój adres email</Text>
                        <TextInput
                            style={styles.input}
                            placeholder={'Np. user@example.com'}
                            onChangeText={formikProps.handleChange('email')}
                            onSubmitEditing={Keyboard.dismiss}
                            autoCompleteType={'email'}
                            autoCorrect={false}
                            keyboardType={'email-address'}
                        />
                        <Text style={{color: 'red'}}>{formikProps.errors.email}</Text>
                        <Text style={styles.text}>Hasło</Text>
                        <TextInput
                            secureTextEntry={true}
                            style={styles.input}
                            placeholder={'Twoje hasło'}
                            onChangeText={formikProps.handleChange('password')}
                            autoCompleteType={'password'}
                            autoCorrect={false}
                        />
                        <Text style={{color: 'red'}}>{formikProps.errors.password}</Text>
                        <View style={styles.rememberMe}>
                            <Text style={styles.textSmall}>Zapamiętaj mnie</Text>
                            <Switch
                                value = {this.state.rememberMe}
                                onValueChange={value => {
                                    this.setState({rememberMe: value});
                                }}/>
                        </View>
                    </View>
                    <View style={styles.button}><Button title={'Zaloguj'}
                                                        //@ts-ignore
                                                        onPress={formikProps.handleSubmit}/></View>
                    <View style={styles.button}><Button color={'#3abf3a'} title={'Zarejestruj się'} onPress={(ev) => {navigate('Register')}}/></View>
                </View>)}
            </Formik>
        );
    }

    login = async (credentials: Credentials) =>{
        //@ts-ignore
        const { navigate } = this.props.navigation;
        try {
            let response = await fetch(SERVER_API_URL+'/api/authenticate',{
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    email: credentials.email,
                    password: credentials.password,
                    rememberMe: credentials.rememberMe
                })
            });
            if(response.status === 200) {
                console.log(response.headers.get('Authorization').substring(7));
                await AsyncStorage.setItem('id_token', response.headers.get('Authorization').substring(7));
                await this.state.dataLoader.resolveRequiredData();
                await this.state.dataLoader.fetchSchedules();
                await this.state.dataLoader.fetchMeasurements();
                await this.state.dataLoader.fetchProfile();

                //@ts-ignore
                this.props.navigation.navigate('Dashboard');
            }
            else if(response.status === 401) {
                this.setState({authFailure: true});
                setTimeout(() => {this.setState({authFailure: false})}, 3000);
            }
            else if(response.status === 500) {
                this.setState({serverFailure: true});
                setTimeout(() => {this.setState({serverFailure: false})}, 3000);
            }
        }
        catch(error) {
            console.log(error);
        }

    };

}
const styles = StyleSheet.create({
    loginPage: {
        justifyContent: 'center',
        borderRadius: 10,
        flex: 1,
        flexDirection: 'column'
    },
    text: {
        fontSize: 28,
    },
    button: {
        width: 350,
        margin: 10,
        borderRadius: 20
    },
    textSmall: {
        fontSize: 18,
    },
    rememberMe: {
        flexDirection: 'row',
        paddingBottom: 10,
        paddingTop: 10
    },
    input: {
        backgroundColor: '#FFFFFF',
        borderRadius: 10,
        fontSize: 25,
        width: 350,
        padding: 5
    },
    alertDanger: {
        backgroundColor: '#ff9e8d',
        fontSize: 18,
        borderRadius: 10,
        textAlign: 'center'
    }
});
