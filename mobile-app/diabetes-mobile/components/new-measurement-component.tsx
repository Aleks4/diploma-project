import {Component} from "react";
import {AppRegistry, AsyncStorage, StyleSheet, TextInput, View, Text, Button, TouchableOpacity} from "react-native";
import * as React from "react";
import {SERVER_API_URL} from "../app.constants";
import {Measurement} from "../dto/measurement";
import Icon from "react-native-vector-icons/Entypo";
import FontAwesome5Icon from "react-native-vector-icons/FontAwesome5";
import {DataLoader} from "../services/data-loader";
import moment from "moment";
import NetInfo, {NetInfoState} from "@react-native-community/netinfo";

import {Formik, Field} from 'formik';
import * as yup from 'yup';

export class NewMeasurementComponent extends Component {

    state = {
        default_glucose_unit: null,
        includeBloodPressure: null,
        min_glucose: null,
        max_glucose: null,

        glucose_value: null,
        glucose_unit: null,
        blood_pressure_upper: null,
        blood_pressure_lower: null,
        blood_pressure_unit: null,
        patient_comment: null,

        handleIncorrectGlucose: false,
        handleIncorrectGlucoseWarning: false,
        saveButtonDisabled: false,
        measurementPostSuccess: false,
        measurementCached: false,
        dataLoader: new DataLoader(),
    };

    componentDidMount(): void {
        //this.resolveRequiredData();
        this.fetchSettingsFromStorage();
    }

    fetchSettingsFromStorage = async () => {
        this.setState({
            default_glucose_unit: await AsyncStorage.getItem('default_glucose_unit'),
            includeBloodPressure: (await AsyncStorage.getItem('includeBloodPressure')) === "true",
            min_glucose: parseFloat(await AsyncStorage.getItem('min_glucose')),
            max_glucose: parseFloat(await AsyncStorage.getItem('max_glucose')),
        })
    };

    validationSchemaNewMeasurement = yup.object().shape({
        glucose_value: yup.number()
            .required('Pole wymagane')
            .typeError('Należy podać liczbę')
            .positive('Wartość musi być dodatnia.')
    });

    render() {

        const glucoseIncorrectLevelForm =
            <View style={styles.alertDanger}>
                <Text style={styles.textSmall}>Nieprawidłowy poziom glukozy!</Text>
                <Text style={styles.textSmall}>Opisz przyczynę(np. słodkie potrawy)</Text>
                <TextInput
                    style={styles.inputText}
                    onChangeText={(text) => {
                        this.setState({patient_comment: text})
                    }}
                    defaultValue={this.state.patient_comment}
                />
            </View>;

        const postMeasurementSuccess =
            <Text style={styles.alertSuccess}>
                Pomiar wstawiony pomyślnie.
            </Text>;
        const measurementCachedMessage = <Text style={styles.alertDanger}>Brak internetu. Pomiar zapisano do pamięci
            podręcznej.</Text>;
        return (
            <Formik initialValues={{
                glucose_value: this.state.glucose_value,
                blood_pressure_upper: this.state.blood_pressure_upper,
                blood_pressure_lower: this.state.blood_pressure_lower,
            }} onSubmit={values => {
                    this.setState({
                        glucose_value: values.glucose_value,
                        blood_pressure_upper: values.blood_pressure_upper,
                        blood_pressure_lower: values.blood_pressure_lower
                    });
                    this.handleSaveButton();
            }}
                    validationSchema={this.validationSchemaNewMeasurement}
            >
                {formikProps => (
                    <View>
                        {this.state.measurementPostSuccess === true ? postMeasurementSuccess : null}
                        {this.state.measurementCached === true ? measurementCachedMessage : null}
                        <Text style={styles.text}>Poziom glukozy</Text>
                        <View style={styles.inputBundle}>
                            <TextInput style={styles.input}
                                       onChangeText={(text) => {
                                           this.setState({glucose_value: parseFloat(text)});
                                           formikProps.setFieldValue('glucose_value', text)
                                       }}
                                       keyboardType={'phone-pad'}
                                       defaultValue={this.state.glucose_value ? this.state.glucose_value.toString() : null}
                            />

                            <Text
                                style={styles.text}>{this.state.default_glucose_unit ? NewMeasurementComponent.simpleUnitTranslator(this.state.default_glucose_unit) : null}</Text>
                        </View>
                        <Text style={{color: 'red'}}>{formikProps.errors.glucose_value}</Text>
                        {this.state.includeBloodPressure && this.state.includeBloodPressure === true ? (
                            <View>
                                <Text style={styles.text}>Ciśnienie krwi</Text>
                                <View style={styles.inputBundle}>
                                    <TextInput style={styles.input}
                                               onChangeText={(text) => {
                                                   this.setState({blood_pressure_upper: parseFloat(text)});
                                                   formikProps.setFieldValue('blood_pressure_upper', text)

                                               }}
                                               keyboardType={'phone-pad'}
                                               placeholder={'Skurczowe'}
                                               defaultValue={this.state.blood_pressure_upper ? this.state.blood_pressure_upper.toString() : null}
                                    />
                                    <Text style={styles.text}>mmHg</Text>
                                </View>
                                <Text style={{color: 'red'}}>{formikProps.errors.blood_pressure_upper}</Text>
                                <View style={styles.inputBundle}>
                                    <TextInput style={styles.input}
                                               onChangeText={(text) => {
                                                   this.setState({blood_pressure_lower: parseFloat(text)});
                                                   formikProps.setFieldValue('blood_pressure_lower', text)
                                               }}
                                               keyboardType={'phone-pad'}
                                               placeholder={'Rozkurczowe'}
                                               defaultValue={this.state.blood_pressure_lower ? this.state.blood_pressure_lower.toString() : null}
                                    />
                                    <Text style={styles.text}>mmHg</Text>
                                </View>
                                <Text style={{color: 'red'}}>{formikProps.errors.blood_pressure_lower}</Text>
                            </View>) : null}
                        {this.state.handleIncorrectGlucose === true ? glucoseIncorrectLevelForm : null}

                        <TouchableOpacity
                            style={[styles.buttonBundle, styles.buttonBundleBlue]}
                            //@ts-ignore
                            onPress={formikProps.handleSubmit}
                        >
                            <Icon name={'plus'} size={45} style={[styles.buttonIcon, styles.buttonBundleGreen]}/>
                            <Text style={styles.buttonText}>         Zapisz</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={[styles.buttonBundle, styles.buttonBundleBlue]}
                            onPress={() => {
                                // @ts-ignore
                                this.props.navigation.navigate('Dashboard')
                            }}
                        >
                            <FontAwesome5Icon
                                name={'sign-out-alt'}
                                size={33}
                                style=
                                    {[styles.buttonIcon, {paddingTop: 6, paddingLeft: 8, paddingRight: 8, paddingBottom: 6}]}
                            />
                            <Text style={styles.buttonText}> Wróć do menu</Text>
                        </TouchableOpacity>
                    </View>
                )}
            </Formik>
        );
    }

    static simpleUnitTranslator(unit: string): string {
        if (unit === 'mgPerDL') return 'mg/dL';
        else if (unit === 'mmolPerL') return 'mmol/L';
        else return '';
    }

    handleSaveButton = async () => {
        this.setState({saveButtonDisabled: true});
        if ((this.state.glucose_value < this.state.min_glucose || this.state.glucose_value > this.state.max_glucose)
            && this.state.handleIncorrectGlucose === false) {
            this.setState({handleIncorrectGlucose: true});
            setTimeout(() => {
                this.setState({saveButtonDisabled: false})
            }, 2000);
            return;
        }
        let measurement = new Measurement();
        measurement.glucose_value = this.state.glucose_value;
        measurement.glucose_unit = this.state.default_glucose_unit;
        measurement.blood_pressure_upper = this.state.blood_pressure_upper;
        measurement.blood_pressure_lower = this.state.blood_pressure_lower;
        measurement.blood_pressure_unit = 'mmHg';
        measurement.patient_comment = this.state.patient_comment;
        let netInfoState: NetInfoState = await NetInfo.fetch();
        if (netInfoState.isInternetReachable) {
            await this.postMeasurementToAPI(measurement);
        } else {
            await this.state.dataLoader.initPendingCache();
            let pending_list: Measurement[] = JSON.parse(await AsyncStorage.getItem('measurements_pending'));
            measurement.insertion_timestamp = moment().format('YYYY-MM-DD, HH:mm:ss');
            measurement.date = moment().format('YYYY-MM-DD');
            measurement.time = moment().format('HH:mm');
            pending_list.push(measurement);
            await AsyncStorage.setItem('measurements_pending', JSON.stringify(pending_list));

            this.setState({measurementCached: true});
        }
        this.setState({saveButtonDisabled: false});
    };

    postMeasurementToAPI = async (measurement: Measurement) => {
        try {
            let response = await fetch(SERVER_API_URL + '/api/measurement/new', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + await AsyncStorage.getItem('id_token')
                },
                body: JSON.stringify({
                    glucose_value: measurement.glucose_value,
                    glucose_unit: measurement.glucose_unit,
                    blood_pressure_upper: measurement.blood_pressure_upper,
                    blood_pressure_lower: measurement.blood_pressure_lower,
                    blood_pressure_unit: measurement.blood_pressure_unit,
                    patient_comment: measurement.patient_comment
                })
            });
            if (response.status === 200) {
                this.setState({
                    measurementPostSuccess: true,
                    handleIncorrectGlucose: false
                });
                await this.state.dataLoader.fetchMeasurements();
                setTimeout(() => {
                    //@ts-ignore
                    this.props.navigation.navigate('Dashboard')
                }, 3000);
            }
        } catch (error) {
            console.log(error);

        }
    }
}

const styles = StyleSheet.create({
    input: {
        backgroundColor: '#FFFFFF',
        borderRadius: 10,
        fontSize: 25,
        width: 220,
        padding: 5,
        textAlign: 'center',
        margin: 5
    },
    inputText: {
        backgroundColor: '#FFFFFF',
        borderRadius: 10,
        fontSize: 25,
        width: 320,
        padding: 5,
        textAlign: 'center',
        margin: 5
    },
    text: {
        fontSize: 28,
        textAlign: 'center'
    },
    textSmall: {
        fontSize: 18,
    },
    button: {
        width: 300,
        margin: 10,
        borderRadius: 20
    },
    inputBundle: {
        flexDirection: 'row'
    },
    alertDanger: {
        backgroundColor: '#ff9e8d',
        fontSize: 18,
        borderRadius: 10,
        textAlign: 'center'
    },
    alertSuccess: {
        backgroundColor: '#71ff92',
        fontSize: 18,
        borderRadius: 10,
        textAlign: 'center'
    },
    buttonBundle: {
        flexDirection: "row",
        width: 300,
        alignSelf: "center",
        alignContent: "center",
        borderRadius: 50,
        margin: 5
    },
    buttonBundleGreen: {
        backgroundColor: "#4dff5e",
    },
    buttonBundleBlue: {
        backgroundColor: "#4685ff"
    },
    buttonText: {
        fontSize: 32,
        justifyContent: "space-between",
        alignSelf: "stretch"
    },
    buttonIcon: {
        backgroundColor: "#FFFFFF",
        borderRadius: 25,
    }
});
