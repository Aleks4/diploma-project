import {Component} from "react";
import {Button, StyleSheet, View, Text, AsyncStorage, Alert, TouchableOpacity} from "react-native";
import * as React from "react";
import {SERVER_API_URL} from "../app.constants";
import FontAwesome5Icon from "react-native-vector-icons/FontAwesome5";
import {PatientSchedule} from "../dto/patient-schedule";
import Icon from "react-native-vector-icons/FontAwesome";

export class MeasurementDetailsComponent extends Component {
    state = {
        measurement: null,
        measurementId: null,
        deleteSuccess: false
    };

    constructor(props) {
        super(props);
        this.deleteMeasurementHandler.bind(this);
    }

    componentDidMount(): void {
        // @ts-ignore
        this.setState({measurementId: this.props.navigation.getParam('id')});
        // @ts-ignore
        this.fetchMeasurement(this.props.navigation.getParam('id'));
    }

    renderScheduleInfo = (patientSchedule: PatientSchedule) => {
        return (
            <View>
                <Text style={styles.text}>Harmonogram</Text>
                <Text style={styles.measurementRow}>
                    {patientSchedule.time} +/-{patientSchedule.time_window}[min]
                </Text>
                <Text style={styles.measurementRow}>{patientSchedule.name}</Text>
            </View>
        );
    };

    render() {
        const deleteSuccessMessage = <Text style={styles.alertDanger}>Pomyślne usunięto.</Text>;

        const patient_comment = <Text
            style={styles.measurementRow}>Opis: {this.state.measurement !== null && this.state.measurement.patient_comment !== null ? this.state.measurement.patient_comment : null}</Text>;

        const deleteButton = <TouchableOpacity
            style={[styles.buttonBundle, styles.buttonBundleBlue]}
            onPress={this.deleteMeasurementHandler}
        >
            <FontAwesome5Icon
                name={'trash-alt'}
                size={33}
                style=
                    {[styles.buttonIcon, {
                        backgroundColor: "#ff645d",
                        paddingTop: 7,
                        paddingLeft: 10,
                        paddingRight: 10,
                        paddingBottom: 7
                    }]}
            />
            <Text style={styles.buttonText}> Usuń pomiar</Text>
        </TouchableOpacity>;

        const measurementDetails =
            <View>
                <Text
                    style={styles.measurementRow}>Glukoza: {this.state.measurement ? this.state.measurement.glucose_value : '-'} [{this.state.measurement ? MeasurementDetailsComponent.simpleUnitTranslator(this.state.measurement.glucose_unit) : '-'}]</Text>
                <Text style={styles.measurementRow}>Ciśnienie
                    krwi: {this.state.measurement ? this.state.measurement.blood_pressure_upper : '-'}/{this.state.measurement ? this.state.measurement.blood_pressure_lower : '-'} [{this.state.measurement ? this.state.measurement.blood_pressure_unit : '-'}]</Text>
                <Text
                    style={styles.measurementRow}>Data: {this.state.measurement ? this.state.measurement.date : '-'}</Text>
                <Text
                    style={styles.measurementRow}>Godzina: {this.state.measurement ? this.state.measurement.time : '-'}</Text>
                {this.state.measurement !== null && this.state.measurement.patient_comment !== null ? patient_comment : null}
                {this.state.measurement !== null && this.state.measurement.patient_schedule !== null ? this.renderScheduleInfo(this.state.measurement.patient_schedule) : null}
                {this.state.measurement !== null && this.state.measurement.editable === true ? deleteButton : null}

            </View>;

        const editMeasurementButton =
            <TouchableOpacity
                style={[styles.buttonBundle, styles.buttonBundleBlue]}
                onPress={() => {
                    // @ts-ignore
                    this.props.navigation.navigate('MeasurementEdit',
                        {
                            id: this.state.measurementId,
                            glucose_value: this.state.measurement.glucose_value,
                            blood_pressure_upper: this.state.measurement.blood_pressure_upper,
                            blood_pressure_lower: this.state.measurement.blood_pressure_lower,
                            patient_comment: this.state.measurement.patient_comment
                    })
                }}
            >
                <Icon name={'pencil'}
                      size={33}
                      style=
                          {[styles.buttonIcon, {paddingTop: 7, paddingLeft: 10, paddingRight: 10, paddingBottom: 8}]}
                />
                <Text style={styles.buttonText}>        Edytuj</Text>
            </TouchableOpacity>;

        return (
            <View>
                {this.state.deleteSuccess === true ? deleteSuccessMessage : null}
                <Text style={styles.text}>Szczegóły pomiaru</Text>
                {this.state.measurement !== null ? measurementDetails : null}
                {this.state.measurement !== null && this.state.measurement.editable === true?editMeasurementButton:null}
                <TouchableOpacity
                    style={[styles.buttonBundle, styles.buttonBundleBlue]}
                    onPress={() => {
                        // @ts-ignore
                        this.props.navigation.navigate('Dashboard')
                    }}
                >
                    <FontAwesome5Icon
                        name={'sign-out-alt'}
                        size={33}
                        style=
                            {[styles.buttonIcon, {paddingTop: 6, paddingLeft: 8, paddingRight: 8, paddingBottom: 6}]}
                    />
                    <Text style={styles.buttonText}> Wróć do menu</Text>
                </TouchableOpacity>
            </View>
        );
    }

    deleteMeasurementHandler = (ev) => {
        Alert.alert('Usuwanie pomiaru', 'Usunąć pomiar?', [
            {
                text: 'Anuluj',
                style: 'cancel'
            },
            {
                text: 'Usuń',
                //@ts-ignore
                onPress: value => {
                    this.deleteMeasurement(this.state.measurementId)
                }
            }
        ])
    };

    deleteMeasurement = async (id: number) => {
        let response = await fetch(SERVER_API_URL + '/api/measurement/delete/' + id, {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + await AsyncStorage.getItem('id_token')
            }
        });
        if (response.status === 200) {
            this.setState({deleteSuccess: true});
            setTimeout(() => {
                this.setState({deleteSuccess: false});
                //@ts-ignore
                this.props.navigation.navigate('MeasurementHistory');
            }, 2000);
        }
    };

    static simpleUnitTranslator(unit: string): string {
        if (unit === 'mgPerDL') return 'mg/dL';
        else if (unit === 'mmolPerL') return 'mmol/L';
        else return '';
    }


    fetchMeasurement = async (id: number) => {
        let response = await fetch(SERVER_API_URL + '/api/measurement/' + id, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + await AsyncStorage.getItem('id_token')
            },
        });
        let responseJson = await response.json();
        this.setState({measurement: responseJson});
    }
}

const styles = StyleSheet.create({
    button: {
        width: 300,
        margin: 10,
        borderRadius: 20
    },
    text: {
        fontSize: 28,
        textAlign: 'center'
    },
    textSmall: {
        fontSize: 18,
    },
    input: {
        backgroundColor: '#FFFFFF',
        borderRadius: 10,
        fontSize: 25,
        width: 310,
        padding: 5,
        textAlign: 'center',
        margin: 5
    },
    measurementRow: {
        backgroundColor: '#FFFFFF',
        borderRadius: 10,
        fontSize: 20,
        width: 320,
        padding: 5,
        margin: 1,
        textAlign: 'center'
    },
    alertDanger: {
        backgroundColor: '#ff9e8d',
        fontSize: 18,
        borderRadius: 10,
        textAlign: 'center'
    },
    buttonBundle: {
        flexDirection: "row",
        width: 300,
        alignSelf: "center",
        alignContent: "center",
        borderRadius: 50,
        margin: 5
    },
    buttonBundleGreen: {
        backgroundColor: "#4dff5e",
    },
    buttonBundleBlue: {
        backgroundColor: "#4685ff"
    },
    buttonText: {
        fontSize: 32,
        justifyContent: "space-between",
        alignSelf: "stretch"
    },
    buttonIcon: {
        backgroundColor: "#FFFFFF",
        borderRadius: 25,
    }
});
