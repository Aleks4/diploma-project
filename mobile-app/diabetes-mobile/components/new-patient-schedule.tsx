import {Component} from "react";
import {
    StyleSheet,
    View,
    Text,
    Button,
    Platform,
    TouchableOpacity,
    Picker,
    TextInput,
    AsyncStorage
} from "react-native";
import * as React from "react";
import DateTimePicker from  '@react-native-community/datetimepicker';
import NumericInput from 'react-native-numeric-input';
import {SERVER_API_URL} from "../app.constants";
import Icon from "react-native-vector-icons/Entypo";
import FontAwesome5Icon from "react-native-vector-icons/FontAwesome5";
import {NotificationService} from "../services/notification-service";
import {DataLoader} from "../services/data-loader";

export class NewPatientScheduleComponent extends Component {

    state = {
        datetime: new Date(),
        show: false,
        time: "00:00",
        schedule_type: "MEASUREMENT",
        time_window: 15,
        name: null,

        postScheduleSuccess: false,
        postScheduleFailure: false,
        notificationService: new NotificationService(),
        dataLoader: new DataLoader()
    };

    componentDidMount(): void {
        console.disableYellowBox = true;
    }



    render() {

        const timeWindowForm =
            <View style={{flexDirection: "row", alignSelf: "center"}}>
                <Text style={styles.text}>Okno czasowe:</Text>
                <View style={{alignSelf: 'center'}}>
                    <NumericInput
                        onChange={(value) => {this.setState({time_window: value})}}
                        minValue={5}
                        maxValue={30}
                        value={this.state.time_window}
                        rounded
                        valueType={"integer"}
                        type={"up-down"}
                    />
                </View>
            </View>;
        const postScheduleSuccess =
            <Text style={styles.alertSuccess}>
                Pomyślnie wstawiono punkt harmonogramu.
            </Text>;
        const postScheduleFailure =
            <Text style={styles.alertDanger}>
                Coś poszło nie tak!
            </Text>;
        return(
            <View>
                {this.state.postScheduleSuccess?postScheduleSuccess:null}
                {this.state.postScheduleFailure?postScheduleFailure:null}
                <Text style={styles.text}>Nowy punkt harmonogramu</Text>
                <TouchableOpacity
                    style={styles.input}
                    onPress={() => {this.setState({show: true})}}
                >
                    <Text style={styles.text}>{this.state.time}</Text>
                </TouchableOpacity>
                <View style={styles.input}>
                    <Picker itemStyle={styles.text}
                            selectedValue={this.state.schedule_type}
                            onValueChange={(itemValue, itemIndex) =>
                            {
                                this.setState({schedule_type: itemValue});
                            }
                            }>
                        <Picker.Item label="Pomiary" value="MEASUREMENT" />
                        <Picker.Item label="Inne, np. lekarstwa" value="MEDICINE" />
                    </Picker>
                </View>
                {this.state.show &&
                <DateTimePicker
                    value={this.state.datetime}
                    mode={'time'}
                    is24Hour={true}
                    display={'default'}
                    onChange={this.setDate}
                />}
                {this.state.schedule_type==="MEASUREMENT"?timeWindowForm:null}
                <TextInput
                    defaultValue={this.state.name}
                    onChangeText={(text) => {this.setState({name: text})}}
                    style={styles.input}
                    placeholder={'Opis'}
                />
                <TouchableOpacity
                    style={[styles.buttonBundle, styles.buttonBundleBlue]}
                    onPress={(ev) => {this.saveSchedule()}}
                >
                    <Icon name={'plus'} size={45} style={[styles.buttonIcon, styles.buttonBundleGreen]}/>
                    <Text style={styles.buttonText}>         Zapisz</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={[styles.buttonBundle, styles.buttonBundleBlue]}
                    onPress={() => {
                        // @ts-ignore
                        this.props.navigation.navigate('PatientSchedule')}}
                >
                    <FontAwesome5Icon
                        name={'sign-out-alt'}
                        size={33}
                        style=
                            {[styles.buttonIcon, {paddingTop: 6, paddingLeft: 8, paddingRight: 8, paddingBottom: 6}]}
                    />
                    <Text style={styles.buttonText}>        Powrót</Text>
                </TouchableOpacity>
            </View>
        );
    }

    saveSchedule = async () => {
        let response = await fetch(SERVER_API_URL+'/api/patient/schedule/new', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer '+ await AsyncStorage.getItem('id_token')
            },
            body: JSON.stringify({
                time: this.state.time,
                type: this.state.schedule_type,
                time_window: this.state.time_window,
                name: this.state.name
            })
        });
        if(response.status === 200) {
            this.setState({postScheduleSuccess: true});
            await this.state.dataLoader.fetchSchedules();
            setTimeout(() => {this.setState({postScheduleSuccess: false})}, 2000);
            //@ts-ignore
            this.props.navigation.navigate('PatientSchedule');
        }
        else {
            this.setState({postScheduleFailure: true});
            setTimeout(() => {this.setState({postScheduleFailure: false})}, 2000);
        }
    };

    setDate = (event, datetime) => {
        datetime = datetime || this.state.datetime;
        let timeHMS= datetime.toLocaleTimeString('pl-PL');
        let timeArray = timeHMS.split(':');
        let time = timeArray[0]+':'+timeArray[1];
        this.setState({
            show: Platform.OS === 'ios' ? true : false,
            datetime,
            time
        });
    }
}
const styles = StyleSheet.create({
    button: {
        width: 300,
        margin: 10,
        borderRadius: 20,
        alignSelf: 'center'
    },
    text: {
        fontSize: 28,
        textAlign: 'center'
    },
    textSmall: {
        fontSize: 18,
    },
    input: {
        backgroundColor: '#FFFFFF',
        borderRadius: 10,
        fontSize: 25,
        width: 310,
        padding: 5,
        textAlign: 'center',
        margin: 5,
        alignSelf: 'center'
    },
    scheduleRow: {
        backgroundColor: '#FFFFFF',
        borderRadius: 10,
        fontSize: 20,
        width: 320,
        padding: 5,
        margin: 1,
        textAlign: 'center',
        alignSelf: 'center'
    },
    alertDanger: {
        backgroundColor: '#ff9e8d',
        fontSize: 18,
        borderRadius: 10,
        textAlign: 'center'
    },
    alertSuccess: {
        backgroundColor: '#71ff92',
        fontSize: 18,
        borderRadius: 10,
        textAlign: 'center'
    },
    buttonBundle: {
        flexDirection: "row",
        width: 300,
        alignSelf: "center",
        alignContent: "center",
        borderRadius: 50,
        margin: 5
    },
    buttonBundleGreen: {
        backgroundColor: "#4dff5e",
    },
    buttonBundleBlue: {
        backgroundColor: "#4685ff"
    },
    buttonText: {
        fontSize: 32,
        justifyContent: "space-between",
        alignSelf: "stretch"
    },
    buttonIcon: {
        backgroundColor: "#FFFFFF",
        borderRadius: 25,
    }
});
