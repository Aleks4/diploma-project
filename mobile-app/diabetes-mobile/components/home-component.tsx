import {Component} from "react";
import {View, Text, StyleSheet, Button, AsyncStorage, TouchableOpacity, Alert} from "react-native";
import * as React from "react";
import Icon from "react-native-vector-icons/Entypo";
import FontAwesome5Icon from "react-native-vector-icons/FontAwesome5";
import {Ionicons} from "@expo/vector-icons"
import {UserProfileDto} from "../dto/user-profile-dto";
import {Notifications} from "expo";
import NetInfo, {NetInfoState, NetInfoSubscription} from "@react-native-community/netinfo";
import {DataLoader} from "../services/data-loader";
import {Measurement} from "../dto/measurement";
export class HomeComponent extends Component {

    private net_info: NetInfoSubscription = null;

    state = {
        patient_settings: null,
        dataLoader: new DataLoader(),

    };

    componentDidMount(): void {

    }

    componentWillUnmount(): void {

    }

    render() {
        return (
            <View style={styles.dashboardContainer}>
                <Text style={styles.text}>Witamy!</Text>

                <TouchableOpacity
                    style={[styles.buttonBundle, styles.buttonBundleBlue]}
                    onPress={() => {
                        // @ts-ignore
                        this.props.navigation.navigate('NewMeasurement')}}
                >
                    <Icon name={'plus'} size={45} style={[styles.buttonIcon, styles.buttonBundleGreen]}/>
                    <Text style={styles.buttonText}>     Nowy pomiar</Text>
                </TouchableOpacity>

                <TouchableOpacity
                    style={[styles.buttonBundle, styles.buttonBundleBlue]}
                    onPress={() => {
                        // @ts-ignore
                        this.props.navigation.navigate('MeasurementHistory')}}
                >
                    <FontAwesome5Icon name={'history'} size={35} style={[styles.buttonIcon, {padding: 6}]}/>
                    <Text style={styles.buttonText}>  Historia pomiarów</Text>
                </TouchableOpacity>

                <TouchableOpacity
                    style={[styles.buttonBundle, styles.buttonBundleBlue]}
                    onPress={() => {
                        // @ts-ignore
                        this.props.navigation.navigate('PatientSchedule')}}
                >
                    <FontAwesome5Icon name={'list-ul'} size={35} style={[styles.buttonIcon, {padding: 6}]}/>
                    <Text style={styles.buttonText}>     Harmonogram</Text>
                </TouchableOpacity>

                <TouchableOpacity
                    style={[styles.buttonBundle, styles.buttonBundleBlue]}
                    onPress={this.settingsButtonHandler}
                >
                    <Icon name={'cog'} size={45} style={[styles.buttonIcon, {paddingTop: 1, paddingLeft: 2, paddingRight: 2, paddingBottom: 1}]}/>
                    <Text style={styles.buttonText}>        Ustawienia</Text>
                </TouchableOpacity>

                <TouchableOpacity
                    style={[styles.buttonBundle, styles.buttonBundleBlue]}
                    onPress={this.profileButtonHandler}
                >
                    <FontAwesome5Icon name={'user-alt'} size={35} style={[styles.buttonIcon, {padding: 7}]}/>
                    <Text style={styles.buttonText}>            Profil</Text>
                </TouchableOpacity>

                <TouchableOpacity
                    style={[styles.buttonBundle, styles.buttonBundleBlue]}
                    onPress={() => {
                        // @ts-ignore
                        Alert.alert('Wylogowanie', 'Na pewno wylogować?', [
                            {
                                text: 'Anuluj',
                                style: 'cancel'
                            },
                            {
                                text: 'Wyloguj',
                                //@ts-ignore
                                onPress: value => {this.logout()}
                            }
                        ])
                    }}
                >
                    <FontAwesome5Icon
                        name={'sign-out-alt'}
                        size={33}
                        style=
                            {[styles.buttonIcon, {paddingTop: 6, paddingLeft: 8, paddingRight: 8, paddingBottom: 6}]}
                    />
                    <Text style={styles.buttonText}>          Wyloguj</Text>
                </TouchableOpacity>
            </View>
        );
    }

    logout = async () => {
        await AsyncStorage.multiRemove(['id_token',
            'default_glucose_unit',
            'includeBloodPressure',
            'min_glucose',
            'max_glucose',
            'patient_schedules',
            'patient_settings',
            'measurements_cache',
            'user_profile'
        ]);
        console.log('Cancelling scheduled notifications...');
        await Notifications.cancelAllScheduledNotificationsAsync();
        //@ts-ignore
        this.props.navigation.navigate('Login');
    };

    settingsButtonHandler = (ev) => {
        this.fetchSettings();
    };
    fetchSettings = async () => {
        let netInfoState: NetInfoState = await NetInfo.fetch();
        if(netInfoState.isInternetReachable) {
            await this.state.dataLoader.resolveRequiredData();
        }
        let patient_settings = JSON.parse(await AsyncStorage.getItem('patient_settings'));
        patient_settings.timeWindow = patient_settings.timeWindow.toString();
        //@ts-ignore
        this.props.navigation.navigate('PatientSettings', {
            reportType: patient_settings.reportType,
            reportPeriod: patient_settings.reportPeriod,
            includeBloodPressure: patient_settings.includeBloodPressure,
            doctorEmail: patient_settings.doctorEmail,
            timeWindow: patient_settings.timeWindow
        })
    };
    profileButtonHandler = (ev) =>{
        this.fetchProfile();
    };
    fetchProfile = async() => {
        let user_profile: UserProfileDto = JSON.parse(await AsyncStorage.getItem('user_profile'));
        //@ts-ignore
        this.props.navigation.navigate('UserProfile', {
            firstname: user_profile.firstname,
            lastname: user_profile.lastname,
            email: user_profile.email,
            phoneNumber: user_profile.phoneNumber,
            default_glucose_unit: user_profile.default_glucose_unit
        });
    }
}
const styles = StyleSheet.create({
    dashboardContainer: {
        flexDirection: 'column',
    },
    text: {
        fontSize: 28,
        textAlign: 'center'
    },
    buttonOld: {
        width: 350,
        margin: 10,
        borderRadius: 20
    },
    button: {
        width: 350,
        margin: 10,
        borderRadius: 20
    },
    buttonBundle: {
        flexDirection: "row",
        width: 350,
        alignSelf: "center",
        alignContent: "center",
        borderRadius: 50,
        margin: 5
    },
    buttonBundleGreen: {
        backgroundColor: "#4dff5e",
    },
    buttonBundleBlue: {
        backgroundColor: "#4685ff"
    },
    buttonText: {
        fontSize: 32,
        justifyContent: "space-between",
        alignSelf: "stretch"
    },
    buttonIcon: {
        backgroundColor: "#FFFFFF",
        borderRadius: 25,
    }

});
