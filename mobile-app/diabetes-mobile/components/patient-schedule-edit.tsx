import {Component} from "react";
import {AsyncStorage, Picker, Platform, StyleSheet, Text, TextInput, TouchableOpacity, View} from "react-native";
import * as React from "react";
import FontAwesome5Icon from "react-native-vector-icons/FontAwesome5";
import DateTimePicker from  '@react-native-community/datetimepicker';
import NumericInput from "react-native-numeric-input";
import FontAwesomeIcon from "react-native-vector-icons/FontAwesome";
import {SERVER_API_URL} from "../app.constants";
export class PatientScheduleEditComponent extends Component {

    state = {
        //@ts-ignore
        id: this.props.navigation.getParam('id'),
        //@ts-ignore
        time: this.props.navigation.getParam('time'),
        //@ts-ignore
        type: this.props.navigation.getParam('type'),
        //@ts-ignore
        name: this.props.navigation.getParam('name'),
        //@ts-ignore
        time_window: this.props.navigation.getParam('time_window'),
        editSuccess: false,
        editFailure: false,
    };

    componentDidMount(): void {

    }

    render() {
        const timeWindowForm =
            <View>
                <Text style={styles.text}>Okno czasowe:</Text>
                <View style={{alignSelf: 'center'}}>
                    <View style={styles.input}>
                        <Picker itemStyle={styles.text}
                                selectedValue={this.state.time_window.toString()}
                                onValueChange={(itemValue, itemIndex) =>
                                    this.setState({time_window: parseInt(itemValue)})
                                }>
                            <Picker.Item label="5 minut" value="5" />
                            <Picker.Item label="10 minut" value="10" />
                            <Picker.Item label="15 minut" value="15" />
                            <Picker.Item label="20 minut" value="20" />
                            <Picker.Item label="25 minut" value="25" />
                            <Picker.Item label="30 minut" value="30" />
                        </Picker>
                    </View>
                </View>
            </View>;
        const postScheduleSuccess =
            <Text style={styles.alertSuccess}>
                Punkt harmonogramu edytowany pomyślnie.
            </Text>;
        const postScheduleFailure =
            <Text style={styles.alertDanger}>
                Nie udało się edytować harmonogramu.
            </Text>;
        return(
            <View>
                {this.state.editSuccess === true ? postScheduleSuccess : null}
                {this.state.editFailure === true ? postScheduleFailure : null}
                <Text style={styles.text}>Edytuj punkt harmonogramu</Text>
                <Text style={styles.text}>Godzina</Text>
                <View style={[styles.inputDisabled]}>
                    <Text style={styles.text}>{this.state.time}</Text>
                </View>
                <Text style={styles.text}>Typ harmonogramu</Text>
                <View style={styles.inputDisabled}>
                    <Text style={styles.text}>{this.state.type==='MEASUREMENT'?'Pomiar':'Inne przypomnienie'}</Text>
                </View>
                <Text style={styles.text}>Nazwa</Text>
                <TextInput
                    defaultValue={this.state.name}
                    onChangeText={(text) => {this.setState({name: text})}}
                    style={styles.input}
                    placeholder={'Opis'}
                />
                {this.state.type==="MEASUREMENT"?timeWindowForm:null}
                <TouchableOpacity
                    style={[styles.buttonBundle, styles.buttonBundleBlue]}
                    //@ts-ignore
                    onPress={() => {
                        this.editSchedule();
                    }}
                >
                    <FontAwesomeIcon name={'save'} size={33}
                                     style=
                                         {[styles.buttonIcon, {paddingTop: 6, paddingLeft: 9, paddingRight: 9, paddingBottom: 6}]}/>
                    <Text style={styles.buttonText}>        Zapisz</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={[styles.buttonBundle, styles.buttonBundleBlue]}
                    onPress={() => {
                        // @ts-ignore
                        this.props.navigation.navigate('PatientSchedule')}}
                >
                    <FontAwesome5Icon
                        name={'sign-out-alt'}
                        size={33}
                        style=
                            {[styles.buttonIcon, {paddingTop: 6, paddingLeft: 8, paddingRight: 8, paddingBottom: 6}]}
                    />
                    <Text style={styles.buttonText}>         Wróć</Text>
                </TouchableOpacity>
            </View>
        );
    }

    editSchedule = async() => {
        try {
            let response = await fetch(SERVER_API_URL+'/api/patient/schedule/edit', {
                method: 'PUT',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer '+ await AsyncStorage.getItem('id_token')
                },
                body: JSON.stringify({
                    id: this.state.id,
                    time: this.state.time,
                    type: this.state.type,
                    time_window: this.state.time_window,
                    name: this.state.name
                })
            });
            if(response.status === 200) {
                this.setState({editSuccess: true});
                setTimeout(() => {
                    //@ts-ignore
                    this.props.navigation.navigate('Dashboard')
                }, 3000);
            }
            else {
                this.setState({
                    editFailure: true
                });
                setTimeout(() => {this.setState({editFailure: false})}, 3000);
            }
        } catch(error) {
            console.log(error);
        }

    }
}
const styles = StyleSheet.create({
    text: {
        fontSize: 28,
        textAlign: 'center'!
    },
    buttonBundle: {
        flexDirection: "row",
        width: 300,
        alignSelf: "center",
        alignContent: "center",
        borderRadius: 50,
        margin: 5
    },
    buttonBundleGreen: {
        backgroundColor: "#4dff5e",
    },
    buttonBundleBlue: {
        backgroundColor: "#4685ff"
    },
    buttonIcon: {
        backgroundColor: "#FFFFFF",
        borderRadius: 25,
    },
    buttonText: {
        fontSize: 32,
        justifyContent: "space-between",
        alignSelf: "stretch"
    },
    alertDanger: {
        backgroundColor: '#ff9e8d',
        fontSize: 18,
        borderRadius: 10,
        textAlign: 'center'
    },
    alertSuccess: {
        backgroundColor: '#71ff92',
        fontSize: 18,
        borderRadius: 10,
        textAlign: 'center'
    },
    input: {
        backgroundColor: '#FFFFFF',
        borderRadius: 10,
        fontSize: 25,
        width: 350,
        padding: 5,

    },
    inputDisabled: {
        backgroundColor: '#c9c9c9',
        borderRadius: 10,
        fontSize: 25,
        width: 350,
        padding: 5,

    },
    rememberMe: {
        flexDirection: 'row',
        paddingBottom: 10,
        paddingTop: 10
    },
    textSmall: {
        fontSize: 18,
    },
    header: {
        fontSize: 35,
        textAlign: 'center'

    },
    stepIndicator: {
        textAlign: 'center',
        fontSize: 20,
        // backgroundColor: '#ff9e8d',
        // borderRadius: 10
    },
    button: {
        width: 350,
        margin: 10,
        borderRadius: 20
    },
    navbuttons: {
        flexDirection: 'row',
        alignContent: 'center',
        width: 350,
        margin: 10
    },
    navbutton: {
        marginLeft: 35,
        marginRight: 35
    }
});
