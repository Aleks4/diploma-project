import {Component} from "react";
import {ActivityIndicator, AsyncStorage, StatusBar, View} from "react-native";
import * as React from "react";
import {Notifications} from "expo";

export class LoadingScreenComponent extends Component {

    componentDidMount() {

        this._bootstrapAsync();
    }

    _bootstrapAsync = async () => {
        await Notifications.deleteChannelAndroidAsync('diabetes_assistant');
        await Notifications.createChannelAndroidAsync('diabetes_assistant', {
            name: 'Diabetes Assistant',
            sound: true,
            vibrate: true
        });
        console.log('Utworzono kanał powiadomień');
        const userToken = await AsyncStorage.getItem('id_token');
        // This will switch to the App screen or Auth screen and this loading
        // screen will be unmounted and thrown away.
        //@ts-ignore
        this.props.navigation.navigate(userToken ? 'Dashboard' : 'Login');
    };

    render() {
        return(
            <View>
                <ActivityIndicator size={"large"}/>
                <StatusBar barStyle="default"/>
            </View>);
    }
}
