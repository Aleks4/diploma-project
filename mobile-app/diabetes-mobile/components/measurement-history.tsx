import {Component} from "react";
import {Button, StyleSheet, View, Text, AsyncStorage, Picker, TouchableOpacity, FlatList} from "react-native";
import * as React from "react";
import {SERVER_API_URL} from "../app.constants";
import Icon from "react-native-vector-icons/AntDesign";
import _ from "lodash";
import FontAwesome5Icon from "react-native-vector-icons/FontAwesome5";

export class MeasurementHistoryComponent extends Component {
    state = {
        measurement_list_dto: null,
        measurement_group: null,

        date_scope: 'WEEK',
        page_number: 0,
        page_size: 7,
        total_page_count: null,
        total_item_count: null,
        includeBloodPressure: false
    };
    constructor(props) {
        super(props);

    }
    componentDidMount(): void {
        this.fetchMeasurements();
    }

    generateKey = (dateString: string): number => {
        let outputKey: number;
        let splitDate: string[] = dateString.split('-');
        outputKey = parseInt(splitDate[0])*10000 + parseInt(splitDate[1])*100+parseInt(splitDate[2]);
        //.forEach(num => {outputKey += parseInt(num)})
            return outputKey;
    };

    renderMeasurementList = (measurementsList: [{date: string, measurements}]) => {
        return(
            <View>
                <View style={styles.measurementRow}>
                    <Text style={styles.textSmall}>Godzina </Text>
                    <Text style={styles.textSmall}>   Glukoza </Text>
                    <Text style={styles.textSmall}>      Ciśnienie krwi </Text>
                </View>
                {
                    measurementsList.map((measurementGroup) => {

                        return(
                            <View key={this.generateKey(measurementGroup.date)}>
                                <Text style={styles.text}>{measurementGroup.date}</Text>
                                {
                                    measurementGroup.measurements.map((measurement) => {
                                        return(
                                            <TouchableOpacity style={styles.measurementRow} key={measurement.id} onPress={(ev) => {
                                                //@ts-ignore
                                                this.props.navigation.navigate('MeasurementDetails', {id: measurement.id})}}>
                                                <Text style={[styles.textSmall, styles.hourColumn]}>{measurement.time}   </Text>
                                                <Text style={[styles.textSmall, styles.glucoseColumn]}>{measurement.glucose_value.toFixed(0)} [{MeasurementHistoryComponent.simpleUnitTranslator(measurement.glucose_unit)}]   </Text>
                                                {
                                                    this.state.includeBloodPressure!== null && this.state.includeBloodPressure === true && (measurement.blood_pressure_upper!==null && measurement.blood_pressure_lower !== null)?<Text style={[styles.textSmall, styles.bloodPressureColumn]}>{measurement.blood_pressure_upper.toFixed(0)}/{measurement.blood_pressure_lower.toFixed(0)} [{measurement.blood_pressure_unit}]</Text>:null
                                                }
                                            </TouchableOpacity>)
                                    })
                                }
                            </View>
                        );
                    })
                }
            </View>
        );
    };

    render() {

        return(
            <View>

                <Text style={styles.text}>Historia pomiarów</Text>
                <View style={styles.input}>
                    <Picker itemStyle={styles.text}
                            selectedValue={this.state.date_scope}
                            onValueChange={(itemValue, itemIndex) =>
                            {
                                this.setState({date_scope: itemValue});
                                this.fetchMeasurements();
                            }
                            }>
                        <Picker.Item label="Ostatni tydzień" value="WEEK" />
                        <Picker.Item label="Ostatni miesiąc" value="MONTH" />
                        <Picker.Item label="Ostatni rok" value="YEAR" />
                    </Picker>
                </View>
                {this.state.measurement_group!== null?this.renderMeasurementList(this.state.measurement_group):null}
                <View style={styles.stickToBottom}>
                    <View style={styles.pageButtonBundle}>
                        <TouchableOpacity onPress={this.previousPage}>
                            <Icon name={'leftcircleo'} size={60}/>
                        </TouchableOpacity>
                        <Text style={styles.textBig}> {this.state.page_number+1} z {this.state.total_page_count} </Text>
                        <TouchableOpacity onPress={this.nextPage}>
                            <Icon name={'rightcircleo'} size={60}/>
                        </TouchableOpacity>
                    </View>
                    <TouchableOpacity
                        style={[styles.buttonBundle, styles.buttonBundleBlue]}
                        onPress={() => {
                            // @ts-ignore
                            this.props.navigation.navigate('Dashboard')}}
                    >
                        <FontAwesome5Icon
                            name={'sign-out-alt'}
                            size={33}
                            style=
                                {[styles.buttonIcon, {paddingTop: 6, paddingLeft: 8, paddingRight: 8, paddingBottom: 6}]}
                        />
                        <Text style={styles.buttonText}>  Wróć do menu</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }

    nextPage = (ev) => {
        if(this.state.total_page_count>this.state.page_number+1) {
            this.setState({page_number: this.state.page_number+1});
            this.fetchMeasurements();
        }
    };

    previousPage = (ev) => {
        if(this.state.page_number>0) {
            this.setState({page_number: this.state.page_number-1});
            this.fetchMeasurements();
        }
    };

    fetchMeasurements = async () => {
        await this.fetchMeasurementsFromStorage();
        fetch(SERVER_API_URL + '/api/measurement/history/range/latest', {
            method: 'POST',
            headers: {
               'Accept': 'application/json',
               'Content-Type': 'application/json',
               'Authorization': 'Bearer '+ await AsyncStorage.getItem('id_token')
            },
            body: JSON.stringify({
                date_scope: this.state.date_scope,
                paging_info: {
                    page_number: this.state.page_number,
                    page_size: this.state.page_size
                }
            })
        }).then((response) => response.json())
            .then(responseJson => {
                    let groupedMeasurements = _(responseJson.measurement_list)
                        .groupBy('date')
                        .map((value, key) => ({date: key, measurements: value}))
                        .toJSON();
                    this.setState({
                        measurement_list_dto: responseJson,
                        measurement_group: groupedMeasurements,
                        total_page_count: responseJson.paging_info.total_page_count,
                        total_item_count: responseJson.paging_info.total_item_count,

                    });
            })
            .catch(this.handleConnectionError);
        // if(response.status===200) {
        //     let responseJson = await response.json();
        //     let groupedMeasurements = _(responseJson.measurement_list)
        //         .groupBy('date')
        //         .map((value, key) => ({date: key, measurements: value}))
        //         .toJSON();
        //     this.setState({
        //         measurement_list_dto: responseJson,
        //         measurement_group: groupedMeasurements,
        //         total_page_count: responseJson.paging_info.total_page_count,
        //         total_item_count: responseJson.paging_info.total_item_count,
        //
        //     });
        // }
        // else {
        //     this.setState({measurement_group: await AsyncStorage.getItem('measurements_cache')})
        // }
        this.setState({includeBloodPressure: await AsyncStorage.getItem('includeBloodPressure') === "true"});
    };

    handleConnectionError = (ev) => {
        this.fetchMeasurementsFromStorage()
    };

    fetchMeasurementsFromStorage = async () => {
        let responseJson = JSON.parse(await AsyncStorage.getItem('measurements_cache'));
        let groupedMeasurements = _(responseJson.measurement_list)
            .groupBy('date')
            .map((value, key) => ({date: key, measurements: value}))
            .toJSON();

        this.setState({
            measurement_group: groupedMeasurements,
            total_page_count: responseJson.paging_info.total_page_count,
            total_item_count: responseJson.paging_info.total_item_count,
        });
    };

    static simpleUnitTranslator(unit: string): string {
        if(unit === 'mgPerDL') return 'mg/dL';
        else if(unit === 'mmolPerL') return 'mmol/L';
        else return '';
    }
}

const styles = StyleSheet.create({
    button: {
        width: 300,
        margin: 10,
        borderRadius: 20
    },
    text: {
        fontSize: 28,
        textAlign: 'center'
    },
    textBig: {
        fontSize: 40,
        textAlign: 'center'
    },
    textSmall: {
        fontSize: 18
    },
    hourColumn: {
        width: 60,
        textAlign: 'center'
    },
    glucoseColumn: {
        width: 110,
        textAlign: 'center'
    },
    bloodPressureColumn: {
        width: 150,
        textAlign: 'center'
    },
    input: {
        backgroundColor: '#FFFFFF',
        borderRadius: 10,
        fontSize: 25,
        width: 310,
        padding: 5,
        textAlign: 'center',
        margin: 5
    },
    measurementRow: {
        backgroundColor: '#FFFFFF',
        borderRadius: 10,
        fontSize: 35,
        width: 320,
        padding: 5,
        margin: 1,
        textAlign: 'center',
        flexDirection: 'row'
    },
    pageButtonBundle: {
        justifyContent: 'center',
        flexDirection: 'row',
        margin: 5
    },
    stickToBottom: {
        justifyContent: 'flex-end',
        marginBottom: 36
    },
    buttonBundle: {
        flexDirection: "row",
        width: 300,
        alignSelf: "center",
        alignContent: "center",
        borderRadius: 50,
        margin: 5
    },
    buttonBundleGreen: {
        backgroundColor: "#4dff5e",
    },
    buttonBundleBlue: {
        backgroundColor: "#4685ff"
    },
    buttonText: {
        fontSize: 32,
        justifyContent: "space-between",
        alignSelf: "stretch"
    },
    buttonIcon: {
        backgroundColor: "#FFFFFF",
        borderRadius: 25,
    }
});
