import {Component} from "react";
import {
    StyleSheet,
    View,
    Text,
    Picker,
    TextInput,
    Keyboard,
    Switch,
    Button,
    AsyncStorage,
    TouchableOpacity
} from "react-native";
import * as React from "react";
import {SERVER_API_URL} from "../app.constants";
import {PatientSettings} from "../dto/patient-settings";
import FontAwesome5Icon from "react-native-vector-icons/FontAwesome5";
import FontAwesomeIcon from "react-native-vector-icons/FontAwesome";
import Icon from "react-native-vector-icons/Icon";
import {DataLoader} from "../services/data-loader";



export class PatientSettingsComponent extends Component {

    state = {
        reportType: null,
        reportPeriod: null,
        includeBloodPressure: null,
        doctorEmail: null,
        timeWindow: null,

        settingsSaveSuccess: false,
        settingsSaveFailure: false,
        dataLoader: new DataLoader()
    };

    componentDidMount(): void {
       this.setState({
           // @ts-ignore
           reportType: this.props.navigation.getParam('reportType'),
           // @ts-ignore
           reportPeriod: this.props.navigation.getParam('reportPeriod'),
           // @ts-ignore
           includeBloodPressure: this.props.navigation.getParam('includeBloodPressure'),
           // @ts-ignore
           doctorEmail: this.props.navigation.getParam('doctorEmail'),
           // @ts-ignore
           timeWindow: this.props.navigation.getParam('timeWindow'),
       });
    }

    render() {
        const alertSuccessMessage =
            <Text style={styles.alertSuccess}>
                Zapisano ustawienia.
            </Text>;
        const alertFailureMessage =
            <Text style={styles.alertDanger}>
                Coś poszło nie tak!
            </Text>;

        const thirdStage_period =
            <View>
                <Text style={styles.text}>Okres raportowania</Text>
                <View style={styles.input}>
                    <Picker itemStyle={styles.text}
                            selectedValue={this.state.reportPeriod}
                            onValueChange={(itemValue, itemIndex) =>
                                this.setState({reportPeriod: itemValue})
                            }>
                        <Picker.Item label="Tygodniowy" value="WEEKLY" />
                        <Picker.Item label="Miesięczny" value="MONTHLY" />
                        <Picker.Item label="Kwartalny" value="QUARTERLY" />
                        <Picker.Item label="Roczny" value="ANNUAL" />
                    </Picker>
                </View>
            </View>;
        return(
            <View>
                {this.state.settingsSaveSuccess===true?alertSuccessMessage:null}
                {this.state.settingsSaveFailure===true?alertFailureMessage:null}
                <Text style={styles.text}>Ustawienia pacjenta</Text>
                <Text style={styles.text}>Typ raportu</Text>
                <View style={styles.input}>
                    <Picker itemStyle={styles.text}
                            selectedValue={this.state.reportType}
                            onValueChange={(itemValue, itemIndex) =>
                                this.setState({reportType: itemValue})
                            }>
                        <Picker.Item label="Ręcznie wyzwalany" value="MANUAL_REPORT" />
                        <Picker.Item label="Okresowy" value="PERIODIC_REPORT" />
                    </Picker>
                </View>
                {this.state.reportType==='PERIODIC_REPORT'?thirdStage_period:null}
                <Text style={styles.text}>Domyślne okno czasowe</Text>
                <View style={styles.input}>
                    <Picker itemStyle={styles.text}
                            selectedValue={this.state.timeWindow}
                            onValueChange={(itemValue, itemIndex) =>
                                this.setState({timeWindow: itemValue})
                            }>
                        <Picker.Item label="5 minut" value="5" />
                        <Picker.Item label="10 minut" value="10" />
                        <Picker.Item label="15 minut" value="15" />
                        <Picker.Item label="20 minut" value="20" />
                        <Picker.Item label="25 minut" value="25" />
                        <Picker.Item label="30 minut" value="30" />
                    </Picker>
                </View>
                <View style={styles.rememberMe}>
                    <Text style={styles.textSmall}>Badane ciśnienie krwi</Text>
                    <Switch
                        value = {this.state.includeBloodPressure}
                        onValueChange={value => {
                            this.setState({includeBloodPressure: value});
                        }}/>
                </View>
                <TouchableOpacity
                    style={[styles.buttonBundle, styles.buttonBundleBlue]}
                    onPress={this.saveButtonHandler}
                >
                    <FontAwesomeIcon name={'save'} size={33}
                                     style=
                                         {[styles.buttonIcon, {paddingTop: 6, paddingLeft: 9, paddingRight: 9, paddingBottom: 6}]}/>
                    <Text style={styles.buttonText}>         Zapisz</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={[styles.buttonBundle, styles.buttonBundleBlue]}
                    onPress={() => {
                        // @ts-ignore
                        this.props.navigation.navigate('Dashboard')}}
                >
                    <FontAwesome5Icon
                        name={'sign-out-alt'}
                        size={33}
                        style=
                            {[styles.buttonIcon, {paddingTop: 6, paddingLeft: 8, paddingRight: 8, paddingBottom: 6}]}
                    />
                    <Text style={styles.buttonText}>  Wróć do menu</Text>
                </TouchableOpacity>
            </View>
        );
    }

    saveButtonHandler = (ev) => {
        let patient_settings: PatientSettings = new PatientSettings();
        patient_settings.reportType = this.state.reportType;
        patient_settings.reportPeriod = this.state.reportPeriod;
        patient_settings.includeBloodPressure = this.state.includeBloodPressure;
        patient_settings.doctorEmail = this.state.doctorEmail;
        patient_settings.timeWindow = parseInt(this.state.timeWindow);
        this.saveSettings(patient_settings);
    };

    saveSettings = async (patient_settings: PatientSettings) => {
        let response = await fetch(SERVER_API_URL+'/api/patient/settings/change', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer '+ await AsyncStorage.getItem('id_token')
            },
            body: JSON.stringify({
                reportType: patient_settings.reportType,
                reportPeriod: patient_settings.reportPeriod,
                includeBloodPressure: patient_settings.includeBloodPressure,
                doctorEmail: patient_settings.doctorEmail,
                timeWindow: patient_settings.timeWindow
            })
        });
        await AsyncStorage.setItem('includeBloodPressure', patient_settings.includeBloodPressure.toString());
        if(response.status === 200) {
            let responseJson = response.json();
            await AsyncStorage.setItem('patient_settings', JSON.stringify(patient_settings));
            this.setState({settingsSaveSuccess: true});
            setTimeout(() => {this.setState({settingsSaveSuccess: false})}, 2000);
        }
        if(response.status%500===1 || response.status%400===1) {
            this.setState({settingsSaveFailure: true});
            setTimeout(() => {this.setState({settingsSaveFailure: false})}, 2000);
        }
    };
}

const styles = StyleSheet.create({
    button: {
        width: 300,
        margin: 10,
        borderRadius: 20
    },
    text: {
        fontSize: 28,
        textAlign: 'center'
    },
    textBig: {
        fontSize: 40,
        textAlign: 'center'
    },
    textSmall: {
        fontSize: 18,
    },
    input: {
        backgroundColor: '#FFFFFF',
        borderRadius: 10,
        fontSize: 25,
        width: 310,
        padding: 5,
        textAlign: 'center',
        margin: 5
    },
    rememberMe: {
        flexDirection: 'row',
        paddingBottom: 10,
        paddingTop: 10
    },
    measurementRow: {
        backgroundColor: '#FFFFFF',
        borderRadius: 10,
        fontSize: 25,
        width: 320,
        padding: 5,
        margin: 1,
        textAlign: 'center'
    },
    alertDanger: {
        backgroundColor: '#ff9e8d',
        fontSize: 18,
        borderRadius: 10,
        textAlign: 'center'
    },
    alertSuccess: {
        backgroundColor: '#84ff92',
        fontSize: 18,
        borderRadius: 10,
        textAlign: 'center'
    },
    buttonBundle: {
        flexDirection: "row",
        width: 300,
        alignSelf: "center",
        alignContent: "center",
        borderRadius: 50,
        margin: 5
    },
    buttonBundleGreen: {
        backgroundColor: "#4dff5e",
    },
    buttonBundleBlue: {
        backgroundColor: "#4685ff"
    },
    buttonText: {
        fontSize: 32,
        justifyContent: "space-between",
        alignSelf: "stretch"
    },
    buttonIcon: {
        backgroundColor: "#FFFFFF",
        borderRadius: 25,
    }
});
