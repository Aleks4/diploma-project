import {Component} from "react";
import {StyleSheet, View, Text, Button, AsyncStorage, TouchableOpacity, Picker} from "react-native";
import * as React from "react";
import {SERVER_API_URL} from "../app.constants";
import {PatientSchedule} from "../dto/patient-schedule";
import _ from "lodash";
import FontAwesome5Icon from "react-native-vector-icons/FontAwesome5";
import Icon from "react-native-vector-icons/Entypo";
import {Notifications} from "expo";
export class PatientScheduleComponent extends Component{

    state = {
        patient_scheduleList: null,
        patient_schedule_measurement: null,
        patient_schedule_other: null,
        schedule_type_display: 'MEASUREMENT'

    };

    componentDidMount(): void {
        this.fetchSchedules();
    }

    renderRow = (patient_schedule: PatientSchedule) => {
        return(
            <TouchableOpacity
                key={patient_schedule.id}
                style={styles.scheduleRow}
                onPress={
                    //@ts-ignore
                    () => {this.props.navigation.navigate('PatientScheduleDetails', {id: patient_schedule.id})}
                }
            >
                <Text style={{ flex: 0, alignSelf: 'stretch', fontSize: 18 }} >{patient_schedule.time} - </Text>
                <Text style={{ flex: 1, alignSelf: 'stretch', fontSize: 18 }} >{patient_schedule.name} </Text>
            </TouchableOpacity>
        );
    };

    renderTable = (data: PatientSchedule[]) => {
        return(
            <View style={{ flex: 0, alignItems: 'center', justifyContent: 'center' }}>
                {
                    data.map((datum) => {
                        return this.renderRow(datum);
                    })
                }
            </View>
        )
    };

    getSelectedSchedules = ():PatientSchedule[] => {
        if(this.state.schedule_type_display==='MEASUREMENT') {
            return this.state.patient_schedule_measurement
        } else return this.state.patient_schedule_other
    };

    render() {
        return(
            <View>
                <Text style={styles.text}>Harmonogram</Text>
                <View style={styles.input}>
                    <Picker itemStyle={styles.text}
                            selectedValue={this.state.schedule_type_display}
                            onValueChange={(itemValue, itemIndex) =>
                            {
                                this.setState({schedule_type_display: itemValue});
                                this.fetchSchedules();
                            }
                            }>
                        <Picker.Item label="Pomiary" value="MEASUREMENT" />
                        <Picker.Item label="Inne, np. lekarstwa" value="MEDICINE" />
                    </Picker>
                </View>
                {this.state.patient_scheduleList!==null?this.renderTable(this.getSelectedSchedules()):null}
                <TouchableOpacity
                    style={[styles.buttonBundle, styles.buttonBundleBlue]}
                    onPress={() => {
                        //@ts-ignore
                        this.props.navigation.navigate('NewPatientSchedule')}}
                >
                    <Icon name={'plus'} size={45} style={[styles.buttonIcon, styles.buttonBundleGreen]}/>
                    <Text style={styles.buttonText}> Dodaj nowy wpis</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={[styles.buttonBundle, styles.buttonBundleBlue]}
                    onPress={() => {
                        // @ts-ignore
                        this.props.navigation.navigate('Dashboard')}}
                >
                    <FontAwesome5Icon
                        name={'sign-out-alt'}
                        size={33}
                        style=
                            {[styles.buttonIcon, {paddingTop: 6, paddingLeft: 8, paddingRight: 8, paddingBottom: 6}]}
                    />
                    <Text style={styles.buttonText}>  Wróć do menu</Text>
                </TouchableOpacity>
            </View>
        );
    }

    fetchSchedules = async () => {
        await this.fetchSchedulesFromStorage();
        try {
            let response = await fetch(SERVER_API_URL+'/api/patient/schedule/list', {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer '+ await AsyncStorage.getItem('id_token')
                }
            });
            let responseJson = await response.json();

            if(response.status===200) {
                this.setState({patient_scheduleList: responseJson,
                    patient_schedule_measurement: _(responseJson).filter(item => {return item.type==='MEASUREMENT'}).reverse().toJSON(),
                    patient_schedule_other: _(responseJson).filter(item => {return item.type==='MEDICINE'}).reverse().toJSON(),
                });
                await AsyncStorage.setItem('patient_schedules', JSON.stringify(responseJson));
            }
        }
        catch(error) {
            console.log(error);
        }
    };

    fetchSchedulesFromStorage = async() => {
        let responseJson = JSON.parse(await AsyncStorage.getItem('patient_schedules'));
        this.setState({patient_scheduleList: responseJson,
            patient_schedule_measurement: _(responseJson).filter(item => {return item.type==='MEASUREMENT'}).reverse().toJSON(),
            patient_schedule_other: _(responseJson).filter(item => {return item.type==='MEDICINE'}).reverse().toJSON(),
        });
    };
}


const styles = StyleSheet.create({
    button: {
        width: 300,
        margin: 10,
        borderRadius: 20
    },
    text: {
        fontSize: 28,
        textAlign: 'center'
    },
    textBig: {
        fontSize: 40,
        textAlign: 'center'
    },
    textSmall: {
        fontSize: 18,
    },
    input: {
        backgroundColor: '#FFFFFF',
        borderRadius: 10,
        fontSize: 25,
        width: 310,
        padding: 5,
        textAlign: 'center',
        margin: 5,
    },
    scheduleRow: {
        backgroundColor: '#FFFFFF',
        borderRadius: 10,
        fontSize: 35,
        width: 320,
        padding: 5,
        margin: 1,
        textAlign: 'center',
        flexDirection: 'row',
        alignSelf: 'stretch'
    },
    pageButtonBundle: {
        justifyContent: 'center',
        flexDirection: 'row',
        margin: 5
    },
    stickToBottom: {
        justifyContent: 'flex-end',
        marginBottom: 36
    },
    buttonBundle: {
        flexDirection: "row",
        width: 300,
        alignSelf: "center",
        alignContent: "center",
        borderRadius: 50,
        margin: 5
    },
    buttonBundleGreen: {
        backgroundColor: "#4dff5e",
    },
    buttonBundleBlue: {
        backgroundColor: "#4685ff"
    },
    buttonText: {
        fontSize: 32,
        justifyContent: "space-between",
        alignSelf: "stretch"
    },
    buttonIcon: {
        backgroundColor: "#FFFFFF",
        borderRadius: 25,
    }
});
