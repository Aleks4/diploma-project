import {Component} from "react";
import {AsyncStorage, StyleSheet, View, Text, Button, Alert, TouchableOpacity} from "react-native";
import * as React from "react";
import {SERVER_API_URL} from "../app.constants";
import {PatientSchedule} from "../dto/patient-schedule";
import FontAwesome5Icon from "react-native-vector-icons/FontAwesome5";
import {DataLoader} from "../services/data-loader";
import Icon from "react-native-vector-icons/FontAwesome";
export class PatientScheduleDetailsComponent extends Component {

    state = {
        scheduleId: null,
        patient_schedule: null,
        dataLoader: new DataLoader()
    };

    componentDidMount(): void {
        // @ts-ignore
        this.setState({scheduleId: this.props.navigation.getParam('id')});
        // @ts-ignore
        this.fetchSchedule(this.props.navigation.getParam('id'));
    }

    displaySchedule = (schedule: PatientSchedule) => {

        return(
            <View>
                <Text style={styles.scheduleRow}>Godzina: {this.state.patient_schedule.time} +/- {this.state.patient_schedule.time_window} minut</Text>
                <Text style={styles.scheduleRow}>Typ: {this.state.patient_schedule.type==='MEASUREMENT'?'Pomiar':'Inne'}</Text>
                <Text style={styles.scheduleRow}>Opis: {this.state.patient_schedule.name}</Text>
                <TouchableOpacity
                    style={[styles.buttonBundle, styles.buttonBundleBlue]}
                    onPress={this.deleteScheduleHandler}
                >
                    <FontAwesome5Icon
                        name={'trash-alt'}
                        size={33}
                        style=
                            {[styles.buttonIcon, {backgroundColor: "#ff645d", paddingTop: 7, paddingLeft: 10, paddingRight: 10, paddingBottom: 7}]}
                    />
                    <Text style={styles.buttonText}>     Usuń wpis</Text>
                </TouchableOpacity>
            </View>
        );
    };

    render() {
        return(
            <View>
                <Text style={styles.text}>Szczegóły harmonogramu</Text>
                {this.state.patient_schedule!==null?this.displaySchedule(this.state.patient_schedule):null}
                <TouchableOpacity
                    style={[styles.buttonBundle, styles.buttonBundleBlue]}
                    onPress={() => {
                        // @ts-ignore
                        this.props.navigation.navigate('PatientScheduleEdit',
                            {
                                id: this.state.patient_schedule.id,
                                time: this.state.patient_schedule.time,
                                name: this.state.patient_schedule.name,
                                type: this.state.patient_schedule.type,
                                time_window: this.state.patient_schedule.time_window
                            })
                    }}
                >
                    <Icon name={'pencil'}
                          size={33}
                          style=
                              {[styles.buttonIcon, {paddingTop: 7, paddingLeft: 10, paddingRight: 10, paddingBottom: 8}]}
                    />
                    <Text style={styles.buttonText}>        Edytuj</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={[styles.buttonBundle, styles.buttonBundleBlue]}
                    onPress={() => {
                        // @ts-ignore
                        this.props.navigation.navigate('PatientSchedule')}}
                >
                    <FontAwesome5Icon
                        name={'sign-out-alt'}
                        size={33}
                        style=
                            {[styles.buttonIcon, {paddingTop: 6, paddingLeft: 8, paddingRight: 8, paddingBottom: 6}]}
                    />
                    <Text style={styles.buttonText}>         Wróć</Text>
                </TouchableOpacity>
            </View>
        );
    }

    fetchSchedule = async (id: number) => {
        let response = await fetch(SERVER_API_URL+'/api/patient/schedule/'+id, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer '+ await AsyncStorage.getItem('id_token')
            },
        });

        let responseJson: PatientSchedule = await response.json();
        this.setState({patient_schedule: responseJson});
    };

    deleteScheduleHandler = (ev) => {
        Alert.alert('Usuwanie', 'Usunąć ten punkt harmonogramu?', [
            {
                text: 'Anuluj',
                style: 'cancel'
            },
            {
                text: 'Usuń',
                //@ts-ignore
                onPress: value => {this.deleteSchedule(this.state.scheduleId)}
            }
        ])
    };

    deleteSchedule = async (id: number) => {
        try {
            let response = await fetch(SERVER_API_URL+'/api/patient/schedule/delete/'+id, {
                method: 'DELETE',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer '+ await AsyncStorage.getItem('id_token')
                }
            });
            if(response.status === 200) {
                this.setState({deleteSuccess: true});
                await this.state.dataLoader.fetchSchedules();
                setTimeout(() => {
                    this.setState({deleteSuccess: false});
                    //@ts-ignore
                    this.props.navigation.navigate('PatientSchedule');
                }, 2000);
            }
        }catch(error) {
            console.log(error);
        }
    };
}
const styles = StyleSheet.create({
    button: {
        width: 300,
        margin: 10,
        borderRadius: 20
    },
    text: {
        fontSize: 28,
        textAlign: 'center'
    },
    textSmall: {
        fontSize: 18,
    },
    input: {
        backgroundColor: '#FFFFFF',
        borderRadius: 10,
        fontSize: 25,
        width: 310,
        padding: 5,
        textAlign: 'center',
        margin: 5
    },
    scheduleRow: {
        backgroundColor: '#FFFFFF',
        borderRadius: 10,
        fontSize: 20,
        width: 320,
        padding: 5,
        margin: 1,
        textAlign: 'center'
    },
    alertDanger: {
        backgroundColor: '#ff9e8d',
        fontSize: 18,
        borderRadius: 10,
        textAlign: 'center'
    },
    buttonBundle: {
        flexDirection: "row",
        width: 300,
        alignSelf: "center",
        alignContent: "center",
        borderRadius: 50,
        margin: 5
    },
    buttonBundleGreen: {
        backgroundColor: "#4dff5e",
    },
    buttonBundleBlue: {
        backgroundColor: "#4685ff"
    },
    buttonText: {
        fontSize: 32,
        justifyContent: "space-between",
        alignSelf: "stretch"
    },
    buttonIcon: {
        backgroundColor: "#FFFFFF",
        borderRadius: 25,
    }
});
