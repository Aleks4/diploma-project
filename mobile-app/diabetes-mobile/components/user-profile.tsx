import {Component} from "react";
import * as React from "react";
import {
    AsyncStorage,
    Button,
    Keyboard,
    Picker,
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    View
} from "react-native";
import FontAwesome5Icon from "react-native-vector-icons/FontAwesome5";
import {UserProfileDto} from "../dto/user-profile-dto";
import {SERVER_API_URL} from "../app.constants";
import FontAwesomeIcon from "react-native-vector-icons/FontAwesome";
import {Formik} from 'formik';
import * as yup from 'yup';
export class UserProfileComponent extends Component {
    state = {
        firstname: '',
        lastname: '',
        email: null,
        phoneNumber: '',
        default_glucose_unit: null,

        profile_page: 1,
        profileSaveSuccess: false,
        profileSaveFailure: false
    };

    validationSchemaProfile = yup.object().shape({
        firstname: yup.string()
            .required('Imię jest wymagane.'),
        lastname: yup.string()
            .required('Nazwisko jest wymagane'),
        phoneNumber: yup.string()
            .label('Numer telefonu')
            .notRequired()
            .matches(/([0-9]{9})/, 'Niepoprawny numer telefonu')
            .max(9, 'Niepoprawny numer telefonu')
            .nullable()
    });

    componentDidMount(): void {
        this.setState({
            //@ts-ignore
            firstname: this.props.navigation.getParam('firstname'),
            //@ts-ignore
            lastname: this.props.navigation.getParam('lastname'),
            //@ts-ignore
            email: this.props.navigation.getParam('email'),
            //@ts-ignore
            phoneNumber: this.props.navigation.getParam('phoneNumber'),
            //@ts-ignore
            default_glucose_unit: this.props.navigation.getParam('default_glucose_unit')
        });
    }

    render() {
        const alertSuccessMessage =
            <Text style={styles.alertSuccess}>
                Zapisano ustawienia.
            </Text>;
        const alertFailureMessage =
            <Text style={styles.alertDanger}>
                Coś poszło nie tak!
            </Text>;

        //@ts-ignore
        const { navigate } = this.props.navigation;
        return(
            <Formik initialValues={{
                //@ts-ignore
                firstname: this.props.navigation.getParam('firstname'),
                //@ts-ignore
                lastname: this.props.navigation.getParam('lastname'),
                //@ts-ignore
                phoneNumber: this.props.navigation.getParam('phoneNumber'),
                //@ts-ignore
                default_glucose_unit: this.props.navigation.getParam('default_glucose_unit'),
            }}
            onSubmit={(values) => {
                this.setState({
                    firstname: values.firstname,
                    lastname: values.lastname,
                    phoneNumber: values.phoneNumber,
                    default_glucose_unit: values.default_glucose_unit
                });
                this.saveButtonHandler();
            }}
                    validationSchema={this.validationSchemaProfile}
            >
                {formikProps => (
                    <View>
                        {this.state.profileSaveSuccess===true?alertSuccessMessage: null}
                        {this.state.profileSaveFailure===true?alertFailureMessage: null}
                        <Text style={styles.text}>Profil</Text>
                        <View>
                            <Text style={styles.text}>Imię</Text>
                            <TextInput
                                style={styles.input}
                                textContentType={'name'}
                                defaultValue={this.state.firstname}
                                editable={true}
                                onChangeText={formikProps.handleChange('firstname')}
                                placeholder={'Twoje imię'}
                            />
                            <Text style={{color: 'red'}}>{formikProps.errors.firstname}</Text>
                            <Text style={styles.text}>Nazwisko</Text>
                            <TextInput
                                style={styles.input}
                                textContentType={'familyName'}
                                onChangeText={formikProps.handleChange('lastname')}
                                defaultValue={this.state.lastname}
                                placeholder={'Twoje nazwisko'}
                            />
                            <Text style={{color: 'red'}}>{formikProps.errors.lastname}</Text>
                            <Text style={styles.text}>Numer telefonu (opcjonalnie)</Text>
                            <TextInput
                                style={styles.input}
                                textContentType={'telephoneNumber'}
                                onChangeText={formikProps.handleChange('phoneNumber')}
                                defaultValue={this.state.phoneNumber}
                                placeholder={'Twój numer telefonu'}
                                keyboardType={'phone-pad'}
                            />
                            <Text style={{color: 'red'}}>{formikProps.errors.phoneNumber}</Text>
                            <Text style={styles.text}>Domyślna jednostka glukozy</Text>
                            <View style={styles.input}>
                                <Picker itemStyle={styles.text}
                                        selectedValue={this.state.default_glucose_unit}
                                        onValueChange={(itemValue, itemIndex) =>{
                                            this.setState({default_glucose_unit: itemValue});
                                            formikProps.setFieldValue('default_glucose_unit', itemValue);
                                        }}>
                                    <Picker.Item label="Miligramy na decylitr [mg/dL]" value="mgPerDL" />
                                    <Picker.Item label="Milimole na litr [mmol/L]" value="mmolPerL" />
                                </Picker>
                            </View>
                        </View>
                        <TouchableOpacity
                            style={[styles.buttonBundle, styles.buttonBundleBlue]}
                            //@ts-ignore
                            onPress={formikProps.handleSubmit}
                        >
                            <FontAwesomeIcon name={'save'} size={33}
                                             style=
                                                 {[styles.buttonIcon, {paddingTop: 6, paddingLeft: 9, paddingRight: 9, paddingBottom: 6}]}/>
                            <Text style={styles.buttonText}>         Zapisz</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={[styles.buttonBundle, styles.buttonBundleBlue]}
                            onPress={() => {
                                // @ts-ignore
                                this.props.navigation.navigate('Dashboard')}}
                        >
                            <FontAwesome5Icon
                                name={'sign-out-alt'}
                                size={33}
                                style=
                                    {[styles.buttonIcon, {paddingTop: 6, paddingLeft: 8, paddingRight: 8, paddingBottom: 6}]}
                            />
                            <Text style={styles.buttonText}>  Wróć do menu</Text>
                        </TouchableOpacity>
                    </View>
                )}
            </Formik>
        );
    }

    saveButtonHandler = (ev?) => {
        let user: UserProfileDto = new UserProfileDto();
        user.default_glucose_unit = this.state.default_glucose_unit;
        user.email = this.state.email;
        user.phoneNumber = this.state.phoneNumber;
        user.firstname = this.state.firstname;
        user.lastname = this.state.lastname;
        this.changeProfile(user);
    };

    changeProfile = async (profile: UserProfileDto) => {
        try {
            let response = await fetch(SERVER_API_URL+'/api/person/profile/change', {
                method: 'PUT',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer '+ await AsyncStorage.getItem('id_token')
                },
                body: JSON.stringify(profile)
            });
            if(response.status === 200) {
                let responseJson = response.json();
                await AsyncStorage.setItem('user_profile', JSON.stringify(profile));
                this.setState({profileSaveSuccess: true});
                setTimeout(() => {this.setState({profileSaveSuccess: false})}, 2000);
            }
            if(response.status%500===1 || response.status%400===1) {
                this.setState({profileSaveFailure: true});
                setTimeout(() => {this.setState({profileSaveFailure: false})}, 2000);
            }
        }catch(error) {
            console.log(error);
            this.setState({profileSaveFailure: true});
            setTimeout(() => {this.setState({profileSaveFailure: false})}, 2000);
        }
    }
}

const styles = StyleSheet.create({
    button: {
        width: 300,
        margin: 10,
        borderRadius: 20
    },
    text: {
        fontSize: 28,
        textAlign: 'center'
    },
    textBig: {
        fontSize: 40,
        textAlign: 'center'
    },
    textSmall: {
        fontSize: 18,
    },
    input: {
        backgroundColor: '#FFFFFF',
        borderRadius: 10,
        fontSize: 25,
        width: 350,
        padding: 5,
        textAlign: 'center',
        margin: 5
    },
    rememberMe: {
        flexDirection: 'row',
        paddingBottom: 10,
        paddingTop: 10
    },
    measurementRow: {
        backgroundColor: '#FFFFFF',
        borderRadius: 10,
        fontSize: 25,
        width: 320,
        padding: 5,
        margin: 1,
        textAlign: 'center'
    },
    alertDanger: {
        backgroundColor: '#ff9e8d',
        fontSize: 18,
        borderRadius: 10,
        textAlign: 'center'
    },
    alertSuccess: {
        backgroundColor: '#84ff92',
        fontSize: 18,
        borderRadius: 10,
        textAlign: 'center'
    },
    buttonBundle: {
        flexDirection: "row",
        width: 300,
        alignSelf: "center",
        alignContent: "center",
        borderRadius: 50,
        margin: 5
    },
    buttonBundleGreen: {
        backgroundColor: "#4dff5e",
    },
    buttonBundleBlue: {
        backgroundColor: "#4685ff"
    },
    buttonText: {
        fontSize: 32,
        justifyContent: "space-between",
        alignSelf: "stretch"
    },
    buttonIcon: {
        backgroundColor: "#FFFFFF",
        borderRadius: 25,
    }
});
