import {Component} from "react";
import {StyleSheet, View, Text, Keyboard, TextInput, Button, Picker, Switch, AsyncStorage} from "react-native";
import * as React from "react";
import {RegistrationDto} from "../dto/registration-dto";
import {SERVER_API_URL} from "../app.constants";
import {PatientSettings} from "../dto/patient-settings";
import {Credentials} from "../dto/credentials";
import {DataLoader} from "../services/data-loader";
import {Formik} from 'formik';
import * as yup from 'yup';
export class RegisterComponent extends Component {
    state = {
        email: '',
        password: '',
        confirm_password: '',
        firstname: '',
        lastname: '',
        phone_number: '',
        default_glucose_unit: 'mgPerDL',

        reportType: 'MANUAL_REPORT',
        reportPeriod: 'WEEKLY',
        doctorEmail: '',
        timeWindow: "10",
        includeBloodPressure: false,

        registrationStep: 1,
        dataLoader: new DataLoader(),
        registerSuccess: false,
        registerFailure: false
    };

    validationSchemaFirstStage = yup.object().shape({
        email: yup.string()
            .label('Email')
            .email('Wprowadź poprawny adres email')
            .required('Wprowadź adres email'),
        password: yup.string()
            .label('Hasło')
            .required('Hasło jest wymagane, musi mieć minimum 8 znaków.')
            .min(8, 'Hasło musi mieć minimum 8 znaków'),
        confirm_password: yup.string()
            .label('Potwierdź hasło')
            .oneOf([yup.ref('password'), null], 'Hasła muszą się zgadzać')
            .required('Wymagane potwierdzenie hasła.')
    });

    validationSchemaSecondStage = yup.object().shape({
        firstname: yup.string()
            .label('Imię')
            .required('Imię jest wymagane'),
        lastname: yup.string()
            .label('Nazwisko')
            .required('Nazwisko jest wymagane'),
        phone_number: yup.string()
            .label('Numer telefonu')
            .notRequired()
            .matches(/([0-9]{9})/, 'Niepoprawny numer telefonu')
            .max(9, 'Niepoprawny numer telefonu'),
        default_glucose_unit: yup.string()
            .oneOf(['mgPerDL', 'mmolPerL'], 'Niepoprawna jednostka.')
            .required('Jednostka jest wymagana')
    });

    validationSchemaThirdStage = yup.object().shape({
        reportType: yup.string()
            .required('Typ raportowania jest wymagany.')
            .oneOf(['MANUAL_REPORT', 'PERIODIC_REPORT'], 'Niepoprawny typ raportu.'),
        reportPeriod: yup.string()
            .required('Wymagany okres raportowania')
            .oneOf(['WEEKLY', 'MONTHLY', 'QUARTERLY', 'ANNUAL']),
        timeWindow: yup.string()
            .required('Okno czasowe jest wymagane')
            .oneOf(['5', '10', '15','20', '25', '30']),
    });

    render() {
        //@ts-ignore
        const { navigate } = this.props.navigation;
        // @ts-ignore
        const firstStage = <Formik initialValues={{
            email: this.state.email,
            password: this.state.password,
            confirm_password: this.state.confirm_password,
        }}
        onSubmit={(values) => {
            this.setState({
               email: values.email,
               password: values.password,
               confirm_password: values.confirm_password,
               registrationStep: 2
            });
            console.log(this.state);
        }}
                                   validationSchema={this.validationSchemaFirstStage}
        >
            {formikProps => (
                <View>
                    <Text style={styles.text}>Twój adres email</Text>
                    <TextInput
                        style={styles.input}
                        placeholder={'Np. user@example.com'}
                        onChangeText={formikProps.handleChange('email')}
                        onBlur={formikProps.handleBlur('email')}
                        defaultValue={this.state.email}
                        onSubmitEditing={Keyboard.dismiss}
                        autoCompleteType={'email'}
                        autoCorrect={false}
                        keyboardType={'email-address'}
                        textContentType={'emailAddress'}
                    />
                    <Text style={{color: 'red'}}>{formikProps.errors.email}</Text>
                    <Text style={styles.text}>Hasło</Text>
                    <TextInput
                        secureTextEntry={true}
                        style={styles.input}
                        placeholder={'Twoje hasło'}
                        onChangeText={formikProps.handleChange('password')}
                        defaultValue={this.state.password}
                        onBlur={formikProps.handleBlur('password')}
                        autoCompleteType={'password'}
                        autoCorrect={false}
                        textContentType={'password'}
                    />
                    <Text style={{color: 'red'}}>{formikProps.errors.password}</Text>
                    <Text style={styles.text}>Powtórz hasło</Text>
                    <TextInput
                        secureTextEntry={true}
                        style={styles.input}
                        placeholder={'Powtórz hasło'}
                        onChangeText={formikProps.handleChange('confirm_password')}
                        onBlur={formikProps.handleBlur('confirm_password')}
                        autoCompleteType={'password'}
                        autoCorrect={false}
                        textContentType={'password'}
                        defaultValue={this.state.confirm_password}
                    />
                    <Text style={{color: 'red'}}>{formikProps.errors.confirm_password}</Text>
                    <View style={styles.button}><Button title={'Powrót do logowania'} onPress={() => {navigate('Login')}}/></View>
                    <View style={styles.button}><Button title={'Następny krok ->'}
                        //@ts-ignore
                                                        onPress={formikProps.handleSubmit}/></View>
                </View>
            )}
        </Formik>;

        const secondStage = <Formik initialValues={{
            firstname: this.state.firstname,
            lastname: this.state.lastname,
            phone_number: this.state.phone_number,
            default_glucose_unit: this.state.default_glucose_unit,
        }} onSubmit={(values) => {
            this.setState({
                firstname: values.firstname,
                lastname: values.lastname,
                phone_number: values.phone_number,
                default_glucose_unit: values.default_glucose_unit,
                registrationStep: 3
            });
        }}
                                    validationSchema={this.validationSchemaSecondStage}
        >
            {formikProps => (<View>
                <Text style={styles.text}>Imię</Text>
                <TextInput
                    style={styles.input}
                    textContentType={'name'}
                    editable={true}
                    onChangeText={formikProps.handleChange('firstname')}
                    defaultValue={this.state.firstname}
                    placeholder={'Twoje imię'}
                />
                <Text style={{color: 'red'}}>{formikProps.errors.firstname}</Text>
                <Text style={styles.text}>Nazwisko</Text>
                <TextInput
                    style={styles.input}
                    textContentType={'familyName'}
                    onChangeText={formikProps.handleChange('lastname')}
                    defaultValue={this.state.lastname}
                    placeholder={'Twoje nazwisko'}
                />
                <Text style={{color: 'red'}}>{formikProps.errors.lastname}</Text>
                <Text style={styles.text}>Numer telefonu (opcjonalnie)</Text>
                <TextInput
                    style={styles.input}
                    textContentType={'telephoneNumber'}
                    onChangeText={formikProps.handleChange('phone_number')}
                    defaultValue={this.state.phone_number}
                    placeholder={'Twój numer telefonu'}
                    keyboardType={'phone-pad'}
                />
                <Text style={{color: 'red'}}>{formikProps.errors.phone_number}</Text>
                <Text style={styles.text}>Domyślna jednostka glukozy</Text>
                <View style={styles.input}>
                    <Picker itemStyle={styles.text}
                            selectedValue={this.state.default_glucose_unit}
                            onValueChange={(itemValue, itemIndex) =>{
                                formikProps.setFieldValue('default_glucose_unit', itemValue);
                                this.setState({default_glucose_unit: itemValue});
                            }}>
                        <Picker.Item label="Miligramy na decylitr [mg/dL]" value="mgPerDL" />
                        <Picker.Item label="Milimole na litr [mmol/L]" value="mmolPerL" />
                    </Picker>
                </View>
                <Text style={{color: 'red'}}>{formikProps.errors.default_glucose_unit}</Text>
                <View>
                    <View style={styles.button}><Button title={'<- Wstecz'} onPress={(ev) => {this.setState({registrationStep: 1})}}/></View>
                    <View style={styles.button}><Button title={'Dalej ->'}
                        //@ts-ignore
                                                        onPress={formikProps.handleSubmit}/></View>
                </View>
            </View>)}
        </Formik>;
        const thirdStage = <Formik initialValues={{
            reportType: this.state.reportType,
            reportPeriod: this.state.reportPeriod,
            timeWindow: this.state.timeWindow,
            includeBloodPressure: this.state.includeBloodPressure,
        }} onSubmit={(values) => {
            this.setState({
                reportType: values.reportType,
                reportPeriod: values.reportPeriod,
                timeWindow: values.timeWindow,
                includeBloodPressure: values.includeBloodPressure,
            });
            this.handleRegister();
        }}
            validationSchema={this.validationSchemaThirdStage}
        >
            {formikProps => (<View>
                <Text style={styles.text}>Typ raportu</Text>
                <View style={styles.input}>
                    <Picker itemStyle={styles.text}
                            selectedValue={this.state.reportType}
                            onValueChange={(itemValue, itemIndex) => {
                                formikProps.setFieldValue('reportType', itemValue);
                                this.setState({reportType: itemValue});
                            }}>
                        <Picker.Item label="Ręcznie wyzwalany" value="MANUAL_REPORT" />
                        <Picker.Item label="Okresowy" value="PERIODIC_REPORT" />
                    </Picker>
                </View>
                <Text style={{color: 'red'}}>{formikProps.errors.reportType}</Text>
                {this.state.reportType==='PERIODIC_REPORT'?(<View>
                    <Text style={styles.text}>Okres raportowania</Text>
                    <View style={styles.input}>
                        <Picker itemStyle={styles.text}
                                selectedValue={this.state.reportPeriod}
                                onValueChange={(itemValue, itemIndex) =>{
                                    formikProps.setFieldValue('reportPeriod', itemValue);
                                    this.setState({reportPeriod: itemValue});
                                }}>
                            <Picker.Item label="Tygodniowy" value="WEEKLY" />
                            <Picker.Item label="Miesięczny" value="MONTHLY" />
                            <Picker.Item label="Kwartalny" value="QUARTERLY" />
                            <Picker.Item label="Roczny" value="ANNUAL" />
                        </Picker>
                    </View>
                    <Text style={{color: 'red'}}>{formikProps.errors.reportPeriod}</Text>
                </View>):null
                }
                <Text style={styles.text}>Domyślne okno czasowe</Text>
                <View style={styles.input}>
                    <Picker itemStyle={styles.text}
                            selectedValue={this.state.timeWindow}
                            onValueChange={(itemValue, itemIndex) =>{
                                formikProps.setFieldValue('timeWindow', itemValue);
                                this.setState({timeWindow: itemValue})
                            }}>
                        <Picker.Item label="5 minut" value="5" />
                        <Picker.Item label="10 minut" value="10" />
                        <Picker.Item label="15 minut" value="15" />
                        <Picker.Item label="20 minut" value="20" />
                        <Picker.Item label="25 minut" value="25" />
                        <Picker.Item label="30 minut" value="30" />
                    </Picker>
                </View>
                <Text style={{color: 'red'}}>{formikProps.errors.timeWindow}</Text>
                <View style={styles.rememberMe}>
                    <Text style={styles.textSmall}>Badane ciśnienie krwi</Text>
                    <Switch
                        value = {this.state.includeBloodPressure}
                        onValueChange={value => {
                            this.setState({includeBloodPressure: value});
                        }}/>
                </View>
                <View style={styles.button}><Button title={'<- Wstecz'} onPress={(ev) => {this.setState({registrationStep: 2})}}/></View>
                <View style={styles.button}><Button color={'#3abf3a'} title={'Rejestruj'}
                                                    //@ts-ignore
                                                    onPress={formikProps.handleSubmit}>Rejestruj</Button></View>
            </View>)}
        </Formik>;

        const registerSuccessMessage = <Text style={styles.alertSuccess}>Zarejestrowano! Zaraz nastąpi przekierowanie...</Text>;
        const registerFailureMessage = <Text style={styles.alertDanger}>Niepowodzenie podczas rejestracji</Text>;

        return (
            <View>
                {this.state.registerSuccess===true?registerSuccessMessage:null}
                {this.state.registerFailure===true?registerFailureMessage:null}
                <Text style={styles.header}>Rejestracja</Text>
                <Text style={styles.stepIndicator}>Krok {this.state.registrationStep} z 3</Text>
                {this.state.registrationStep===1?firstStage:null}
                {this.state.registrationStep===2?secondStage:null}
                {this.state.registrationStep===3?thirdStage:null}
            </View>
        );
    }
    handleRegister = (ev?) => {
        let registerDto = new RegistrationDto();
        registerDto.patient_settings = new PatientSettings();
        registerDto.firstname = this.state.firstname;
        registerDto.lastname = this.state.lastname;
        registerDto.email = this.state.email;
        registerDto.phone_number = this.state.phone_number;
        registerDto.password = this.state.password;
        registerDto.confirm_password = this.state.confirm_password;
        registerDto.patient_settings.reportType = this.state.reportType;
        registerDto.patient_settings.reportPeriod = this.state.reportPeriod;
        registerDto.patient_settings.doctorEmail = this.state.doctorEmail;
        registerDto.patient_settings.timeWindow = parseInt(this.state.timeWindow);
        this.register(registerDto);
    };
    register = async (registrationDto: RegistrationDto) => {
        let response = await fetch(SERVER_API_URL+'/api/register', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(registrationDto)
        });
        if(response.status === 200) {
            this.setState({registerSuccess: true});
            let credentials = new Credentials();
            credentials.rememberMe = true;
            credentials.email = registrationDto.email;
            credentials.password = registrationDto.password;
            await this.loginAfterSuccessfulRegistration(credentials);
        }
        else {
            this.setState({registerFailure: true});
            setTimeout(() => {this.setState({registerFailure: false})}, 3000);
        }
    };

    loginAfterSuccessfulRegistration = async (credentials: Credentials) => {
        try {
            let response = await fetch(SERVER_API_URL+'/api/authenticate',{
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    email: credentials.email,
                    password: credentials.password,
                    rememberMe: credentials.rememberMe
                })
            });
            if(response.status === 200) {
                console.log(response.headers.get('Authorization').substring(7));
                await AsyncStorage.setItem('id_token', response.headers.get('Authorization').substring(7));
                await this.state.dataLoader.resolveRequiredData();
                await this.state.dataLoader.fetchSchedules();
                await this.state.dataLoader.fetchMeasurements();
                await this.state.dataLoader.fetchProfile();

                //@ts-ignore
                this.props.navigation.navigate('Dashboard');
            }
        } catch(error) {
            console.log(error);
        }
    };

    handleReturnToLogin = (ev) => {

    }
}

const styles = StyleSheet.create({
    text: {
        fontSize: 28,
    },
    alertDanger: {
        backgroundColor: '#ff9e8d',
        fontSize: 18,
        borderRadius: 10,
        textAlign: 'center'
    },
    alertSuccess: {
        backgroundColor: '#71ff92',
        fontSize: 18,
        borderRadius: 10,
        textAlign: 'center'
    },
    input: {
        backgroundColor: '#FFFFFF',
        borderRadius: 10,
        fontSize: 25,
        width: 350,
        padding: 5
    },
    rememberMe: {
        flexDirection: 'row',
        paddingBottom: 10,
        paddingTop: 10
    },
    textSmall: {
        fontSize: 18,
    },
    header: {
        fontSize: 35,
        textAlign: 'center'

    },
    stepIndicator: {
        textAlign: 'center',
        fontSize: 20,
        // backgroundColor: '#ff9e8d',
        // borderRadius: 10
    },
    button: {
        width: 350,
        margin: 10,
        borderRadius: 20
    },
    navbuttons: {
        flexDirection: 'row',
        alignContent: 'center',
        width: 350,
        margin: 10
    },
    navbutton: {
       marginLeft: 35,
        marginRight: 35
    }
});
