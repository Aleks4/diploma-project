import {PatientSchedule} from "../dto/patient-schedule";
import {LocalNotificationId} from "expo/build/Notifications/Notifications.types";

export class ScheduleNotification {
    schedule: PatientSchedule;
    notificationId: string;
}
