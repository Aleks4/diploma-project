import React, {Component} from 'react';
import {AsyncStorage, SafeAreaView, StyleSheet, View} from 'react-native';
import {LoginComponent} from "./components/Login";
import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {createBottomTabNavigator, createMaterialTopTabNavigator} from 'react-navigation-tabs';
import {RegisterComponent} from "./components/register";
import {HomeComponent} from "./components/home-component";
import {createStackNavigator} from "react-navigation-stack";
import {LoadingScreenComponent} from "./components/loading-screen";
import {NewMeasurementComponent} from "./components/new-measurement-component";
import {MeasurementHistoryComponent} from "./components/measurement-history";
import {MeasurementDetailsComponent} from "./components/measurement-details";
import {PatientSettingsComponent} from "./components/patient-settings";
import {PatientScheduleComponent} from "./components/patient-schedule";
import {PatientScheduleDetailsComponent} from "./components/patient-schedule-details";
import {NewPatientScheduleComponent} from "./components/new-patient-schedule";
import {UserProfileComponent} from "./components/user-profile";
import {DataLoader} from "./services/data-loader";
import {PatientService} from "./services/patient-service";
import NetInfo, {NetInfoSubscription} from "@react-native-community/netinfo";
import {Measurement} from "./dto/measurement";
import AwaitLock from 'await-lock';
import {MeasurementEditComponent} from "./components/measurement-edit";
import {PatientScheduleEditComponent} from "./components/patient-schedule-edit";

const PatientDashboardNavigator = createStackNavigator(
    {
        Home: {screen: HomeComponent}
    },
    {
        initialRouteName: 'Home'
    }
);
const AuthNavigator = createSwitchNavigator({
        Loading: {screen: LoadingScreenComponent},
        Login: {screen: LoginComponent},
        Register: {screen: RegisterComponent},

        Dashboard: HomeComponent,
        NewMeasurement: NewMeasurementComponent,
        MeasurementHistory: MeasurementHistoryComponent,
        MeasurementDetails: MeasurementDetailsComponent,
        MeasurementEdit: MeasurementEditComponent,
        PatientSettings: PatientSettingsComponent,
        PatientSchedule: PatientScheduleComponent,
        PatientScheduleEdit: PatientScheduleEditComponent,
        PatientScheduleDetails: PatientScheduleDetailsComponent,
        NewPatientSchedule: NewPatientScheduleComponent,
        UserProfile: UserProfileComponent
    },
    {
        initialRouteName: 'Loading'
    });


const AppContainer = createAppContainer(AuthNavigator);

export default class App extends Component {

    private net_info: NetInfoSubscription = null;
    private asyncLock: AwaitLock = new AwaitLock();

    state = {
        dataLoader: new DataLoader(),
        patientService: new PatientService(),
    };

    componentDidMount(): void {
        this.net_info = NetInfo.addEventListener(state => {
            if(state.isInternetReachable) {
                console.log('Wykryto podłączenie do internetu. Sprawdzanie cache...');
                this.netEventHandler();
            }
        });
    }

    componentWillUnmount(): void {
        this.net_info();
    }

    netEventHandler = async() => {
        await this.asyncLock.acquireAsync();
        try {
            if(!(await this.state.patientService.isLoggedIn())) {
                console.log('Użytkownik niezalogowany. Odstąpiono od sprawdzania pamięci podręcznej');
                return;
            }
            await this.state.dataLoader.initPendingCache();
            let pendingMeasurements: Measurement[] = JSON.parse(await AsyncStorage.getItem('measurements_pending'));
            if(pendingMeasurements.length!==0) {
                await this.state.dataLoader.bulkInsertMeasurement(pendingMeasurements);
                await AsyncStorage.setItem('measurements_pending', JSON.stringify([]));
            }
            else {
                console.log('Brak zaleglych pomiarow do wstawienia.')
            }
        } finally {
            this.asyncLock.release();
        }

    };

    render() {
        return (
            <View style={styles.container}>
                <SafeAreaView>
                    <View style={styles.safeArea}>
                        <AppContainer/>
                    </View>
                </SafeAreaView>
            </View>);
    }

}

// without the style you will see a blank screen
// export default () => (
//     <View style={styles.container}>
//         <SafeAreaView>
//             <View style={styles.safeArea}>
//                 <AppContainer/>
//             </View>
//         </SafeAreaView>
//
//     </View>
// );


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#a6cfff',
        alignItems: 'center',
        justifyContent: 'center'
    },
    safeArea: {
        paddingTop: 36
    }
});


